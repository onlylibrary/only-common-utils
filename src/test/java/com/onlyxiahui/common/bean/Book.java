package com.onlyxiahui.common.bean;

import java.math.BigDecimal;
import java.util.Date;

import com.onlyxiahui.common.bean.base.PrimaryStringBean;

/**
 * Description <br>
 * Date 2020-11-10 09:04:09<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public class Book extends PrimaryStringBean {

	private String name;
	private Double weight;
	private int pageCount;
	private Date sellDate;
	private BigDecimal price;

	/**
	 * 创建时间
	 */
	private String createdDateTime = "2020-02-02";
	/**
	 * 修改时间
	 */
	private String updatedDateTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
}
