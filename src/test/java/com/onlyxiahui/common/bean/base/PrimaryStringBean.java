package com.onlyxiahui.common.bean.base;

/**
 * Description <br>
 * Date 2020-11-10 15:29:39<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class PrimaryStringBean extends BaseBean {

	/**
	 * id
	 */
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
