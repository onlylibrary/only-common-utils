package com.onlyxiahui.common.bean.base;

/**
 * Description <br>
 * Date 2020-11-10 15:29:39<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class BaseBean {
	/**
	 * 创建时间戳
	 */
	private Long createdTimestamp;

	/**
	 * 修改时间戳
	 */
	private Long updatedTimestamp;

	/**
	 * 创建时间
	 */
	private String createdDateTime;

	/**
	 * 修改时间
	 */
	private String updatedDateTime;

	/**
	 * 是否逻辑删除：0：否、1：是
	 */
	private Integer isDeleted;

	public Long getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Long createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public Long getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	public void setUpdatedTimestamp(Long updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
}
