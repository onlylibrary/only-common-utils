package com.onlyxiahui.common.utils.base.util.time;

import java.util.Date;
import java.util.List;

import org.junit.Test;

/**
 * Date 2019-01-11 14:38:08<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class DateUtilTest {

	@Test
	public void getBetweenDays() {
		int days = DateUtil.getBetweenDays("2020-10-01 23:59:59", "2020-10-08");
		System.out.println(days);
	}

	@Test
	public void getWeekByDate() {
		int week = DateUtil.getWeekByDate("2020-10-08");
		System.out.println(week);
		week = DateUtil.getWeekByDate("2020-10-01");
		System.out.println(week);
		week = DateUtil.getWeekByDate("2020-10-22");
		System.out.println(week);
	}

	@Test
	public void getWeekList() {
		Date beginDate = DateUtil.stringDateToDate("2020-10-08");
		Date endDate = DateUtil.stringDateToDate("2020-10-01");
		List<?> list = DateUtil.getWeekList(beginDate, endDate);

		for (Object o : list) {
			System.out.println(o);
		}
	}

	@Test
	public void getBetweenScopeDateList() {
		Date beginDate = DateUtil.stringDateToDate("2020-10-01");
		Date endDate = DateUtil.stringDateToDate("2020-10-11");

		List<Date> list = DateUtil.getBetweenExcludeScopeDateList(beginDate, endDate);
		for (Date o : list) {
			System.out.println(DateUtil.dateToDateTime(o));
		}

		System.out.println("------------------------");

		list = DateUtil.getBetweenIncludeScopeDateList(beginDate, endDate);
		for (Date o : list) {
			System.out.println(DateUtil.dateToDate(o));
		}
	}

	@Test
	public void getNowDifferenceTomorrowZeroMillis() {

		Long time = DateUtil.getNowDifferenceTomorrowZeroMillis();
		System.out.println(time);
		System.out.println(DateUtil.millisecondToTime(time));
		System.out.println(DateUtil.millisecondTimestampToDateTimeString(System.currentTimeMillis()));
	}
}
