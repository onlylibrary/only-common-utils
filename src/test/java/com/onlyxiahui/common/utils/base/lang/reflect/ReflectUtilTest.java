package com.onlyxiahui.common.utils.base.lang.reflect;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;

import com.onlyxiahui.common.bean.Book;
import com.onlyxiahui.common.utils.base.util.time.DateUtil;

/**
 * Description <br>
 * Date 2020-11-10 09:03:56<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ReflectUtilTest {

	@Test
	public void getField() {
		Book bookOld = new Book();

		bookOld.setId("1000");
		bookOld.setName("Mark中国 ");

		bookOld.setPageCount(55);
		bookOld.setPrice(new BigDecimal("20.33"));
		bookOld.setSellDate(DateUtil.stringDateToDate("2020-10-11"));
		bookOld.setWeight(33.22D);

		System.out.println(ReflectUtil.getField(Book.class, "name"));
	}

	@Test
	public void getFieldList() {
		Book bookOld = new Book();

		bookOld.setId("1000");
		bookOld.setName("Mark中国 ");

		bookOld.setPageCount(55);
		bookOld.setPrice(new BigDecimal("20.33"));
		bookOld.setSellDate(DateUtil.stringDateToDate("2020-10-11"));
		bookOld.setWeight(33.22D);

		bookOld.setCreatedDateTime("2020-10-10");

		System.out.println(bookOld.getCreatedDateTime());
		System.out.println(ReflectUtil.getFieldValue(Book.class, "createdDateTime"));
		System.out.println(ReflectUtil.getFieldValue(bookOld, "createdDateTime"));

		List<Field> list = ReflectUtil.getFieldList(Book.class, null);
		for (Field f : list) {
			System.out.println(f + ":" + f.isAccessible());
		}
	}
}
