package com.onlyxiahui.common.utils.base.compare;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.Test;

/**
 * Description <br>
 * Date 2020-11-10 21:10:17<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class EqualsUtilTest {

	public static void main(String[] args) {
		BigInteger bt = new BigInteger("1111111");
		BigDecimal bd1 = new BigDecimal("1111111");
		BigDecimal bd2 = new BigDecimal("1111111");
		boolean isEquals = EqualsUtil.isEquals(bt, bd1);
		System.out.println(isEquals);
		isEquals = EqualsUtil.isEquals(bd1, bd2);
		System.out.println(isEquals);
		bd2 = new BigDecimal("1111111.00");
		isEquals = EqualsUtil.isEquals(bd1, bd2);
		System.out.println(isEquals);
	}

	@Test
	public void isNull() {
		boolean isEquals = EqualsUtil.isEquals(null, null);
		System.out.println(isEquals);
		isEquals = EqualsUtil.isEquals(null, "");
		System.out.println(isEquals);
		isEquals = EqualsUtil.isEquals(null, null, false);
		System.out.println(isEquals);
	}
}
