package com.onlyxiahui.common.utils.base.lang.string;

/**
 * Description <br>
 * Date 2020-12-24 15:54:21<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class StringValueUtilTest {

	public static void main(String[] args) {
		String text = "6632jsajais0032.3232";
		System.out.println(StringValueUtil.getStartStringNumber(text));
		System.out.println(StringValueUtil.getEndStringNumber(text));
	}
}
