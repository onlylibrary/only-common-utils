package com.onlyxiahui.common.utils.base.lang.string;

/**
 * Description <br>
 * Date 2020-12-24 15:54:21<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class StringReplaceUtilTest {

	public static void main(String[] args) {
		System.out.println(StringReplaceUtil.replacePartString("13222222222", "****", 3, 6));
		System.out.println(StringReplaceUtil.replaceEachChar("13222222222", "****", 3, 6));
		System.out.println(StringReplaceUtil.replaceBeforeEachChar("13222222222", "****", 3, 7));
		System.out.println(StringReplaceUtil.replaceAfterEachChar("13222222222", "****", 3, 7));
		System.out.println(StringReplaceUtil.replaceMiddleEachChar("13222222222", "****", 3, 7));
	}
}
