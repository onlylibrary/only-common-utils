package com.onlyxiahui.common.utils.base.math;

import java.math.BigDecimal;

/**
 * Description <br>
 * Date 2020-12-07 15:17:22<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class BigDecimalTest {

	public static void main(String[] args) {
		BigDecimal zero = new BigDecimal("0");
		BigDecimal none = new BigDecimal("1");
		BigDecimal lone = new BigDecimal("-1");

		System.out.println(zero.compareTo(none));
		System.out.println(zero.compareTo(zero));
		System.out.println(lone.compareTo(zero));

		System.out.println("-------------");

		System.out.println(BigDecimalCompareUtil.isEqual(none, zero));
		System.out.println(BigDecimalCompareUtil.isEqual(zero, zero));
		System.out.println("-------------");

		System.out.println(BigDecimalCompareUtil.isGreater(none, zero));
		System.out.println(BigDecimalCompareUtil.isGreater(zero, none));
		System.out.println(BigDecimalCompareUtil.isGreater(none, none));
		System.out.println("-------------");

		System.out.println(BigDecimalCompareUtil.isGreaterEqual(none, zero));
		System.out.println(BigDecimalCompareUtil.isGreaterEqual(zero, none));
		System.out.println(BigDecimalCompareUtil.isGreaterEqual(none, none));
		System.out.println("-------------");

		System.out.println(BigDecimalCompareUtil.isLess(none, zero));
		System.out.println(BigDecimalCompareUtil.isLess(zero, none));
		System.out.println(BigDecimalCompareUtil.isLess(none, none));
		System.out.println("-------------");

		System.out.println(BigDecimalCompareUtil.isLessEqual(none, zero));
		System.out.println(BigDecimalCompareUtil.isLessEqual(zero, none));
		System.out.println(BigDecimalCompareUtil.isLessEqual(none, none));
	}
}
