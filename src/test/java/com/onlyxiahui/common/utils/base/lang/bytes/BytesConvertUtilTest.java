package com.onlyxiahui.common.utils.base.lang.bytes;

import java.util.Arrays;

import org.junit.Test;

/**
 * Date 2019-01-11 14:28:35<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class BytesConvertUtilTest {

	@Test
	public void bytesToInt() {
		int value = 80;
		byte[] bs = BytesConvertUtil.intToBytes(value);
		System.out.println(Arrays.toString(bs));
		System.out.println(BytesConvertUtil.bytesToInt(bs));
	}

	@Test
	public void intToBytes() {
		int value = 4;
		byte[] bs = BytesConvertUtil.intToBytes(value);
		System.out.println(Arrays.toString(bs));
	}

	@Test
	public void bytesMerge() {
		int value1 = 80;
		byte[] bytes1 = BytesConvertUtil.intToBytes(value1);
		int value2 = 60;
		byte[] bytes2 = BytesConvertUtil.intToBytes(value2);
		byte[] bytes = BytesConvertUtil.bytesMerge(bytes1, bytes2);
		System.out.println(Arrays.toString(bytes1));
		System.out.println(Arrays.toString(bytes2));
		System.out.println(Arrays.toString(bytes));
	}

	@Test
	public void byteToInt() {
		byte b = 2;
		int i = BytesConvertUtil.byteToInt(b);
		System.out.println(i);
	}

	@Test
	public void bytesToHexText() {
		int value1 = 80;
		byte[] bytes1 = BytesConvertUtil.intToBytes(value1);
		String text = BytesConvertUtil.bytesToHexText(bytes1);
		System.out.println(text);
	}

	@Test
	public void hexTextToBytes() {
		int value1 = 80;
		byte[] bytes1 = BytesConvertUtil.intToBytes(value1);
		String text = BytesConvertUtil.bytesToHexText(bytes1);

		byte[] bytes2 = BytesConvertUtil.hexTextToBytes(text);
		System.out.println(Arrays.toString(bytes1));
		System.out.println(Arrays.toString(bytes2));
	}
}
