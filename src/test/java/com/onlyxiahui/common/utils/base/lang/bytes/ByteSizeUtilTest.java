package com.onlyxiahui.common.utils.base.lang.bytes;

import org.junit.Test;

/**
 * Description <br>
 * Date 2020-11-11 10:53:51<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ByteSizeUtilTest {

	public static void main(String[] args) {

		System.out.println(BytesSizeUtil.getSizeUnit(100L));
		System.out.println(BytesSizeUtil.getSizeUnit(1924L));
		System.out.println(BytesSizeUtil.getSizeUnit(1124L));
		System.out.println(BytesSizeUtil.getSizeUnit(99L * 1024L * 1024L));
		System.out.println(BytesSizeUtil.getSizeUnit(99L * 1024L * 1024L + 384L));
		System.out.println(BytesSizeUtil.getSizeUnit(Long.MAX_VALUE - 330932L));
		System.out.println(BytesSizeUtil.getSizeUnit(Long.MAX_VALUE));
		// --------------------------------------------------
		System.out.println("--------------------------------------------------");
		System.out.println(BytesSizeUtil.getSizeUnit(100L, 9));
		System.out.println(BytesSizeUtil.getSizeUnit(1924L, 10));
		System.out.println(BytesSizeUtil.getSizeUnit(1124L, 2));
		System.out.println(BytesSizeUtil.getSizeUnit(99L * 1024L * 1024L, 2));
		System.out.println(BytesSizeUtil.getSizeUnit(99L * 1024L * 1024L + 384L, 4));
		System.out.println(BytesSizeUtil.getSizeUnit(Long.MAX_VALUE - 330932L, 27));
		System.out.println(BytesSizeUtil.getSizeUnit(Long.MAX_VALUE, 6));
	}

	@Test
	public void test() {

	}
}
