package com.onlyxiahui.common.utils.base.security;

import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.Test;

import com.onlyxiahui.common.utils.base.security.asymmetric.SecurityRsaUtil;

/**
 * Description <br>
 * Date 2020-11-09 15:12:41<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class SecurityRsaUtilTest {
	@Test
	public void testRsa() {
		String privateKeyClassPath = "/data/security/rsa/rsa_private_key_pkcs8.pem";
		PrivateKey privateKey = CertificateUtil.getPemRsaPrivateKeyByClassPath(privateKeyClassPath);

		String publicKeyClassPath = "/data/security/rsa/rsa_public_key.pem";
		PublicKey publicKey = CertificateUtil.getPemRsaPublicKeyByClassPath(publicKeyClassPath);
		StringBuilder sb = new StringBuilder();
		sb.append("MIME和PEM关于长度的限制都是用于SMTP的。");
		sb.append("该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		sb.append("--------------------- ");
		sb.append("jdk8的Base64类是基于rfc2045和rfc4648实现的，根据上文列出的协议内容可以确定，该类的编码结果不会包含换行符，而且在类的注释");
		sb.append("中明确说明了不会添加换行符。");
		sb.append("MIME协议通常作为base64协议的引用。但是MIME协议并没有定义'base64'，而是定义了'base64 内容传输编码'。因此MIME将base64编码的数据的长度限制为76个字符。");
		sb.append("MIME is often used as a reference for base 64 encoding.  However,");
		sb.append("MIME does not define \"base 64\" per se, but rather a \"base 64 Content-");
		sb.append("Transfer-Encoding\" for use within MIME.  As such, MIME enforces a");
		sb.append("limit on line length of base 64-encoded data to 76 characters.  MIME");
		sb.append("inherits the encoding from Privacy Enhanced Mail (PEM) [3], stating");
		sb.append("that it is \"virtually identical\"; however, PEM uses a line length of");
		sb.append("64 characters.  The MIME and PEM limits are both due to limits within");
		sb.append("SMTP.");
		sb.append(" Implementations MUST NOT add line feeds to base-encoded data unless");
		sb.append("the specification referring to this document explicitly directs base");
		sb.append("encoders to add line feeds after a specific number of characters.");
		sb.append("今晚打老虎,哈哈啥哈哈啥啊哈哈！该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		String content = sb.toString();
		// content = "今晚打老虎,哈哈啥哈哈啥啊哈哈！";
		try {
			String value = SecurityRsaUtil.encryptToBase64String(content, publicKey);
			System.out.println(value);
			String back = SecurityRsaUtil.decryptByBase64String(value, privateKey);
			System.out.println(back);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testRsa0() {
		String privateKeyClassPath = "/data/security/rsa/rsa_private_key_512_pkcs8.pem";
		PrivateKey privateKey = CertificateUtil.getPemRsaPrivateKeyByClassPath(privateKeyClassPath);

		String publicKeyClassPath = "/data/security/rsa/rsa_public_key_512.pem";
		PublicKey publicKey = CertificateUtil.getPemRsaPublicKeyByClassPath(publicKeyClassPath);
		StringBuilder sb = new StringBuilder();
		sb.append("MIME和PEM关于长度的限制都是用于SMTP的。");
		sb.append("该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		sb.append("--------------------- ");
		sb.append("jdk8的Base64类是基于rfc2045和rfc4648实现的，根据上文列出的协议内容可以确定，该类的编码结果不会包含换行符，而且在类的注释");
		sb.append("中明确说明了不会添加换行符。");
		sb.append("MIME协议通常作为base64协议的引用。但是MIME协议并没有定义'base64'，而是定义了'base64 内容传输编码'。因此MIME将base64编码的数据的长度限制为76个字符。");
		sb.append("MIME is often used as a reference for base 64 encoding.  However,");
		sb.append("MIME does not define \"base 64\" per se, but rather a \"base 64 Content-");
		sb.append("Transfer-Encoding\" for use within MIME.  As such, MIME enforces a");
		sb.append("limit on line length of base 64-encoded data to 76 characters.  MIME");
		sb.append("inherits the encoding from Privacy Enhanced Mail (PEM) [3], stating");
		sb.append("that it is \"virtually identical\"; however, PEM uses a line length of");
		sb.append("64 characters.  The MIME and PEM limits are both due to limits within");
		sb.append("SMTP.");
		sb.append(" Implementations MUST NOT add line feeds to base-encoded data unless");
		sb.append("the specification referring to this document explicitly directs base");
		sb.append("encoders to add line feeds after a specific number of characters.");
		sb.append("今晚打老虎,哈哈啥哈哈啥啊哈哈！该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		String content = sb.toString();
		// content = "今晚打老虎,哈哈啥哈哈啥啊哈哈！";
		try {
			String value = SecurityRsaUtil.encryptToBase64String(content, publicKey);
			System.out.println(value);
			String back = SecurityRsaUtil.decryptByBase64String(value, privateKey);
			System.out.println(back);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testRsa1() {
		String privateKeyClassPath = "/data/security/rsa/rsa_private_key_1024_pkcs8.pem";
		PrivateKey privateKey = CertificateUtil.getPemRsaPrivateKeyByClassPath(privateKeyClassPath);

		String publicKeyClassPath = "/data/security/rsa/rsa_public_key_1024.pem";
		PublicKey publicKey = CertificateUtil.getPemRsaPublicKeyByClassPath(publicKeyClassPath);
		StringBuilder sb = new StringBuilder();
		sb.append("MIME和PEM关于长度的限制都是用于SMTP的。");
		sb.append("该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		sb.append("--------------------- ");
		sb.append("jdk8的Base64类是基于rfc2045和rfc4648实现的，根据上文列出的协议内容可以确定，该类的编码结果不会包含换行符，而且在类的注释");
		sb.append("中明确说明了不会添加换行符。");
		sb.append("MIME协议通常作为base64协议的引用。但是MIME协议并没有定义'base64'，而是定义了'base64 内容传输编码'。因此MIME将base64编码的数据的长度限制为76个字符。");
		sb.append("MIME is often used as a reference for base 64 encoding.  However,");
		sb.append("MIME does not define \"base 64\" per se, but rather a \"base 64 Content-");
		sb.append("Transfer-Encoding\" for use within MIME.  As such, MIME enforces a");
		sb.append("limit on line length of base 64-encoded data to 76 characters.  MIME");
		sb.append("inherits the encoding from Privacy Enhanced Mail (PEM) [3], stating");
		sb.append("that it is \"virtually identical\"; however, PEM uses a line length of");
		sb.append("64 characters.  The MIME and PEM limits are both due to limits within");
		sb.append("SMTP.");
		sb.append(" Implementations MUST NOT add line feeds to base-encoded data unless");
		sb.append("the specification referring to this document explicitly directs base");
		sb.append("encoders to add line feeds after a specific number of characters.");
		sb.append("今晚打老虎,哈哈啥哈哈啥啊哈哈！该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		String content = sb.toString();
		// content = "今晚打老虎,哈哈啥哈哈啥啊哈哈！";
		try {
			String value = SecurityRsaUtil.encryptToBase64String(content, publicKey);
			System.out.println(value);
			String back = SecurityRsaUtil.decryptByBase64String(value, privateKey);
			System.out.println(back);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testRsa2() {
		String privateKeyClassPath = "/data/security/rsa/rsa_private_key_2048_pkcs8.pem";
		PrivateKey privateKey = CertificateUtil.getPemRsaPrivateKeyByClassPath(privateKeyClassPath);

		String publicKeyClassPath = "/data/security/rsa/rsa_public_key_2048.pem";
		PublicKey publicKey = CertificateUtil.getPemRsaPublicKeyByClassPath(publicKeyClassPath);
		StringBuilder sb = new StringBuilder();
		sb.append("MIME和PEM关于长度的限制都是用于SMTP的。");
		sb.append("该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		sb.append("--------------------- ");
		sb.append("jdk8的Base64类是基于rfc2045和rfc4648实现的，根据上文列出的协议内容可以确定，该类的编码结果不会包含换行符，而且在类的注释");
		sb.append("中明确说明了不会添加换行符。");
		sb.append("MIME协议通常作为base64协议的引用。但是MIME协议并没有定义'base64'，而是定义了'base64 内容传输编码'。因此MIME将base64编码的数据的长度限制为76个字符。");
		sb.append("MIME is often used as a reference for base 64 encoding.  However,");
		sb.append("MIME does not define \"base 64\" per se, but rather a \"base 64 Content-");
		sb.append("Transfer-Encoding\" for use within MIME.  As such, MIME enforces a");
		sb.append("limit on line length of base 64-encoded data to 76 characters.  MIME");
		sb.append("inherits the encoding from Privacy Enhanced Mail (PEM) [3], stating");
		sb.append("that it is \"virtually identical\"; however, PEM uses a line length of");
		sb.append("64 characters.  The MIME and PEM limits are both due to limits within");
		sb.append("SMTP.");
		sb.append(" Implementations MUST NOT add line feeds to base-encoded data unless");
		sb.append("the specification referring to this document explicitly directs base");
		sb.append("encoders to add line feeds after a specific number of characters.");
		sb.append("今晚打老虎,哈哈啥哈哈啥啊哈哈！该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		String content = sb.toString();
		// content = "今晚打老虎,哈哈啥哈哈啥啊哈哈！";
		try {
			String value = SecurityRsaUtil.encryptToBase64String(content, publicKey);
			System.out.println(value);
			String back = SecurityRsaUtil.decryptByBase64String(value, privateKey);
			System.out.println(back);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testRsa3() {
		String privateKeyClassPath = "/data/security/rsa/rsa_private_key_4096_pkcs8.pem";
		PrivateKey privateKey = CertificateUtil.getPemRsaPrivateKeyByClassPath(privateKeyClassPath);

		String publicKeyClassPath = "/data/security/rsa/rsa_public_key_4096.pem";
		PublicKey publicKey = CertificateUtil.getPemRsaPublicKeyByClassPath(publicKeyClassPath);
		StringBuilder sb = new StringBuilder();
		sb.append("MIME和PEM关于长度的限制都是用于SMTP的。");
		sb.append("该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		sb.append("--------------------- ");
		sb.append("jdk8的Base64类是基于rfc2045和rfc4648实现的，根据上文列出的协议内容可以确定，该类的编码结果不会包含换行符，而且在类的注释");
		sb.append("中明确说明了不会添加换行符。");
		sb.append("MIME协议通常作为base64协议的引用。但是MIME协议并没有定义'base64'，而是定义了'base64 内容传输编码'。因此MIME将base64编码的数据的长度限制为76个字符。");
		sb.append("MIME is often used as a reference for base 64 encoding.  However,");
		sb.append("MIME does not define \"base 64\" per se, but rather a \"base 64 Content-");
		sb.append("Transfer-Encoding\" for use within MIME.  As such, MIME enforces a");
		sb.append("limit on line length of base 64-encoded data to 76 characters.  MIME");
		sb.append("inherits the encoding from Privacy Enhanced Mail (PEM) [3], stating");
		sb.append("that it is \"virtually identical\"; however, PEM uses a line length of");
		sb.append("64 characters.  The MIME and PEM limits are both due to limits within");
		sb.append("SMTP.");
		sb.append(" Implementations MUST NOT add line feeds to base-encoded data unless");
		sb.append("the specification referring to this document explicitly directs base");
		sb.append("encoders to add line feeds after a specific number of characters.");
		sb.append("今晚打老虎,哈哈啥哈哈啥啊哈哈！该协议的实现在编码结果中不能添加换行符，除非引用了该文档的实现中，明确说明在特定长度之后添加换行符。");
		String content = sb.toString();
		// content = "今晚打老虎,哈哈啥哈哈啥啊哈哈！";
		try {
			String value = SecurityRsaUtil.encryptToBase64String(content, publicKey);
			System.out.println(value);
			String back = SecurityRsaUtil.decryptByBase64String(value, privateKey);
			System.out.println(back);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
