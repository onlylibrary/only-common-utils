package com.onlyxiahui.common.utils.base.io.file;

/**
 * Description <br>
 * Date 2021-05-13 16:03:47<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class FileTextUtilTest {

	public static void main(String[] args) {
		System.out.println(FileTextUtil.getTextByClassPath("/file/text.txt"));
		System.out.println("-----");
		System.out.println(FileTextUtil.getTextByFilePath("file/text/text.txt"));
		System.out.println("-----");
	}
}
