package com.onlyxiahui.common.utils.base.util.time;

import org.junit.Test;

/**
 * 
 * Description <br>
 * Date 2019-03-30 17:47:42<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class LocalDateUtilTest {

	@Test
	public void getCurrentDateTimeMillisecond() {
		System.out.println("getCurrentDateTimeMillisecond:" + LocalDateUtil.getCurrentDateTimeMillisecond());
	}

	/**
	 * 
	 * Description 获取当前日期+时间<br>
	 * format:yyyy-MM-dd HH:mm:ss<br>
	 * Date 2019-03-30 17:48:17<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	@Test
	public void getCurrentDateTime() {
		System.out.println("getCurrentDateTime:" + LocalDateUtil.getCurrentDateTime());
	}

	/**
	 * 
	 * Description 获取当前时间<br>
	 * format:HH:mm:ss<br>
	 * Date 2019-03-30 17:48:50<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	@Test
	public void getCurrentDate() {
		System.out.println("getCurrentDate:" + LocalDateUtil.getCurrentDate());
	}

	/**
	 * 
	 * Description 获取当前日期<br>
	 * format:yyyy-MM-dd<br>
	 * Date 2019-03-30 17:49:38<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	@Test
	public void getCurrentTime() {
		System.out.println("getCurrentTime:" + LocalDateUtil.getCurrentTime());
	}

	/**
	 * 
	 * Description 获取当前自定义格式时间 <br>
	 * Date 2019-03-30 17:49:54<br>
	 * 
	 * @param format :自定义时间格式
	 * @return
	 * @since 1.0.0
	 */
	@Test
	public void getCurrent() {
		System.out.println("getCurrent:" + LocalDateUtil.getCurrent(LocalDateUtil.FORMAT_DATE));
	}

	/**
	 * 
	 * Description 将毫秒转为（HH:mm:ss）多少小时：多少分钟：多少秒 <br>
	 * Date 2019-03-30 17:50:10<br>
	 * 
	 * @param time
	 * @return
	 * @since 1.0.0
	 */
	@Test
	public void millisecondToTime() {
		System.out.println("millisecondToTime-1:" + LocalDateUtil.millisecondToTime(System.currentTimeMillis()));
		System.out.println("millisecondToTime-2:" + LocalDateUtil.millisecondToTime(30L));
		System.out.println("millisecondToTime-3:" + LocalDateUtil.millisecondToTime(1000L));
		System.out.println("millisecondToTime-4:" + LocalDateUtil.millisecondToTime(1000L * 60L));
		System.out.println("millisecondToTime-5:" + LocalDateUtil.millisecondToTime(1000L * 60L * 60L));
		System.out.println("millisecondToTime-6:" + LocalDateUtil.millisecondToTime(1000L * 60L * 60L * 24));
		System.out.println("millisecondToTime-7:" + LocalDateUtil.millisecondToTime(1000L * 60L * 60L * 24 + 1000L));
	}

	/**
	 * 
	 * Description 获取指定天数后的时间 <br>
	 * Date 2019-03-30 17:51:18<br>
	 * 
	 * @param days
	 * @return
	 * @since 1.0.0
	 */
	@Test
	public void getAddDays() {
		System.out.println("getAddDays:" + LocalDateUtil.getAddDays(6));
	}

	@Test
	public void millisecondTimestampToLocalDateTime() {
		System.out.println("timestampToLocalDateTime:" + LocalDateUtil.millisecondTimestampToLocalDateTime(System.currentTimeMillis()));
	}

	/**
	 * 
	 * Description 大部分时间格式转yyyy-MM-dd HH:mm:ss <br>
	 * Date 2019-03-30 17:58:19<br>
	 * 
	 * @param text
	 * @return
	 * @since 1.0.0
	 */
	@Test
	public void allToDateTime() {
		System.out.println("allToDateTime-1:" + LocalDateUtil.allToDateTime("2018/02/1"));
		System.out.println("allToDateTime-1:" + LocalDateUtil.allToDateTime("2018.02.1"));
		System.out.println("allToDateTime-1:" + LocalDateUtil.allToDateTime("20180201"));
	}

	@Test
	public void allToDateTimeMillisecond() {
		System.out.println("allToDateTimeMillisecond:" + LocalDateUtil.allToDateTimeMillisecond("2018/02/01 10:10:21"));
	}
}
