package com.onlyxiahui.common.utils.base.net;

import java.math.BigInteger;

import com.onlyxiahui.common.utils.base.lang.bytes.BytesSizeUtil;

/**
 * <br>
 *
 * @date 2022-05-24 16:38:51<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class BytesSizeUtilTest {

	public static void main(String[] args) {
		System.out.println(BytesSizeUtil.getSizeUnit(new BigInteger("1000000000000"), 2));
		System.out.println(BytesSizeUtil.getSizeUnit(new BigInteger("100000000000000000"), 2));
		System.out.println(BytesSizeUtil.getSizeUnit(new BigInteger("10000000000000000000000"), 2));
		System.out.println(BytesSizeUtil.getSizeUnit(new BigInteger("1000000000000000000000000000"), 2));
		System.out.println(BytesSizeUtil.getSizeUnit(new BigInteger("10000000000000000000000000000000000000"), 2));
		System.out.println(BytesSizeUtil.getSizeUnit(new BigInteger("100000000000000000000000000000000000000000000000"), 2));
		System.out.println(BytesSizeUtil.getSizeUnit(new BigInteger("1000000000000000000000000000000000000000000000000000000000"), 2));
	}

}
