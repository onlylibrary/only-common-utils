package com.onlyxiahui.common.utils.base.lang.number;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

/**
 * <br>
 *
 * @date 2022-05-24 12:06:20<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class NumberJudgeUtilTest {

	public static void main(String[] args) {

		isNumber("");
		System.out.println();
		isNumber("1");
		isNumber("1l");
		isNumber("1L");
		isNumber("1f");
		isNumber("1F");
		isNumber("1d");
		isNumber("1D");
		isNumber("1E");
		isNumber("1a");
		System.out.println();
		isNumber("-1");
		isNumber("-1l");
		isNumber("-1L");
		isNumber("-1f");
		isNumber("-1F");
		isNumber("-1d");
		isNumber("-1D");
		isNumber("-1E");
		isNumber("-1a");
		System.out.println();
		isNumber("+1");
		isNumber("+1l");
		isNumber("+1L");
		isNumber("+1f");
		isNumber("+1F");
		isNumber("+1d");
		isNumber("+1D");
		isNumber("+1E");
		isNumber("+1a");
		System.out.println();
		isNumber("000");
		isNumber("001");
		System.out.println();
		isNumber("0");
		isNumber("0.0");
		isNumber("0.0l");
		isNumber("0.0L");
		isNumber("0.0f");
		isNumber("0.0F");
		isNumber("0.0d");
		isNumber("0.0D");
		isNumber("0.0E");
		isNumber("0.0a");
		System.out.println();
		isNumber("-0");
		isNumber("-0.0");
		isNumber("-0.0l");
		isNumber("-0.0L");
		isNumber("-0.0f");
		isNumber("-0.0F");
		isNumber("-0.0d");
		isNumber("-0.0D");
		isNumber("-0.0E");
		isNumber("-0.0a");
		System.out.println();
		isNumber("+0");
		isNumber("+0.0");
		isNumber("+0.0l");
		isNumber("+0.0L");
		isNumber("+0.0f");
		isNumber("+0.0F");
		isNumber("+0.0d");
		isNumber("+0.0D");
		isNumber("+0.0E");
		isNumber("+0.0a");
		System.out.println();
		isNumber("0.12921");
		isNumber("0.12921l");
		isNumber("0.12921L");
		isNumber("0.12921f");
		isNumber("0.12921F");
		isNumber("0.12921d");
		isNumber("0.12921D");
		isNumber("0.12921E");
		isNumber("0.12921a");
		System.out.println();
		isNumber("-0.12921");
		isNumber("-0.12921l");
		isNumber("-0.12921L");
		isNumber("-0.12921f");
		isNumber("-0.12921F");
		isNumber("-0.12921d");
		isNumber("-0.12921D");
		isNumber("-0.12921E");
		isNumber("-0.12921a");
		System.out.println();
		isNumber("+0.12921");
		isNumber("+0.12921l");
		isNumber("+0.12921L");
		isNumber("+0.12921f");
		isNumber("+0.12921F");
		isNumber("+0.12921d");
		isNumber("+0.12921D");
		isNumber("+0.12921E");
		isNumber("+0.12921a");
		System.out.println();
		isNumber("F");

		System.out.println();
		isNumber("1,100,000");
		isNumber("1,100,000l");
		isNumber("1,100,000L");
		isNumber("1,100,000f");
		isNumber("1.7976931348623157e+308");
		isNumber("999999999999999999999999999999999999999999999999999999999999999999999999");

		isNumber("-2147483648");
		isNumber("2147483647");
		isNumber("-2147483659");
		isNumber("2147483648");
		isNumber("-2147483647");
		isNumber("2147483646");

		isNumber("-9223372036854775808");
		isNumber("9223372036854775807");

		isNumber("-9223372036854775809");
		isNumber("9223372036854775808");

//		System.out.println();
//
//		System.out.println(Long.MIN_VALUE);
//		System.out.println(Long.MAX_VALUE);

	}

	@Test
	public void n() {
		String v = "-9223372036854775808";
		System.out.println("isScopeJavaLong(\"" + v + "\")=" + NumberJudgeUtil.isScopeJavaLong(v) + "<br>");
	}

	public static void isNumber(String v) {
//		System.out.println("isJavaNumber(\"" + v + "\")=" + NumberJudgeUtil.isJavaNumber(v) + "<br>");
		System.out.println("isScopeJavaBigInteger(\"" + v + "\")=" + NumberJudgeUtil.isScopeJavaBigInteger(v) + "<br>");
//		System.out.println("isDouble(\"" + v + "\")=" + NumberJudgeUtil.isDouble(v) + "<br>");
//		System.out.println("isNumber(\"" + v + "\")=" + isPositiveInteger(v) + "<br>");
	}

	@Test
	public void isLong() {
		System.out.println(NumberJudgeUtil.isJustJavaInteger("-111111111111116666666666666666666666666666666611111"));
	}

	private static boolean match(String regex, String text) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		return matcher.matches();
	}

	/**
	 * 是否为数字
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isNumbers(String text) {
		String regex = "^(\\-|\\+)?\\d+(\\.\\d+)?(f|F|d|D)?$";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(text).matches();
	}

	/**
	 * 验证非零的正整数
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isPositiveInteger(String text) {
		String regex = "^\\+?[1-9][0-9]*$";
		return match(regex, text);
	}

}
