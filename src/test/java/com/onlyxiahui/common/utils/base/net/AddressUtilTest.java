package com.onlyxiahui.common.utils.base.net;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.List;

public class AddressUtilTest {

	public static void main(String[] args) {

		String getIpV4 = AddressUtil.getIpV4ByName("www.baidu.com");
		System.out.println("getIpV4ByUrl：" + getIpV4);

		String getIpV6 = AddressUtil.getIpV6ByName("Only-PC");
		System.out.println("getIpV6ByUrl：" + getIpV6);

		String getLocalIpV4 = AddressUtil.getLocalIpV4();
		System.out.println("getLocalIpV4：" + getLocalIpV4);

		String getLocalIpV6 = AddressUtil.getLocalIpV6();
		System.out.println("getLocalIpV6：" + getLocalIpV6);

		List<String> getLocalIpV4List = AddressUtil.getLocalIpV4List();
		System.out.println("getLocalIpV4List：----------------------------------");
		for (String ip : getLocalIpV4List) {
			System.out.println("getLocalIpV4List：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<String> getLocalIpV6List = AddressUtil.getLocalIpV6List();
		System.out.println("getLocalIpV6List：----------------------------------");
		for (String ip : getLocalIpV6List) {
			System.out.println("getLocalIpV6List：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<String> getWanIpV4List = AddressUtil.getWanIpV4List();
		System.out.println("getWanIpV4List：----------------------------------");
		for (String ip : getWanIpV4List) {
			System.out.println("getWanIpV4List：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<String> getWanIpV6List = AddressUtil.getWanIpV6List();
		System.out.println("getWanIpV6List：----------------------------------");
		for (String ip : getWanIpV6List) {
			System.out.println("getWanIpV6List：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<String> getLanIpV4List = AddressUtil.getLanIpV4List();
		System.out.println("getLanIpV4List：----------------------------------");
		for (String ip : getLanIpV4List) {
			System.out.println("getLanIpV4List：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<String> getLanIpV6List = AddressUtil.getLanIpV6List();
		System.out.println("getLanIpV6List：----------------------------------");
		for (String ip : getLanIpV6List) {
			System.out.println("getLanIpV6List：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		String getLocalHostName = AddressUtil.getLocalHostName();
		System.out.println("getLocalHostName：" + getLocalHostName);

		String getMacAddress = AddressUtil.getMacAddress();
		System.out.println("getMacAddress：" + getMacAddress);

		List<String> getMacAddressList = AddressUtil.getMacAddressList();
		System.out.println("getMacAddressList：----------------------------------");
		for (String ip : getMacAddressList) {
			System.out.println("getMacAddressList：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<String> getCardMacAddressList = AddressUtil.getCardMacAddressList();
		System.out.println("getCardMacAddressList：----------------------------------");
		for (String ip : getCardMacAddressList) {
			System.out.println("getCardMacAddressList：" + ip);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<InetAddress> getCardInetAddressList = AddressUtil.getCardInetAddressList();
		System.out.println("getCardInetAddressList：----------------------------------");

		for (InetAddress ia : getCardInetAddressList) {
			// IPV6
			String type = null;
			if (ia instanceof Inet6Address) {
				type = "IpV6";
			}
			if (ia instanceof Inet4Address) {
				type = "IpV4";
			}

			System.out.println("getCardInetAddressList：type=" + type + " ip=" +
					ia.getHostAddress() + " isSiteLocalAddress=" +
					ia.isSiteLocalAddress() + " isLinkLocalAddress=" + ia.isLinkLocalAddress() + " isLoopbackAddress=" + ia.isLoopbackAddress());
		}
		System.out.println();
		System.out.println();
		System.out.println();

		List<InetAddress> getInetAddressList = AddressUtil.getInetAddressList();
		System.out.println("getInetAddressList：----------------------------------");

		for (InetAddress ia : getInetAddressList) {
			// IPV6
			String type = null;
			if (ia instanceof Inet6Address) {
				type = "IpV6";
			}
			if (ia instanceof Inet4Address) {
				type = "IpV4";
			}

			System.out.println("getInetAddressList：type=" + type + " ip=" +
					ia.getHostAddress() + " isSiteLocalAddress=" +
					ia.isSiteLocalAddress() + " isLinkLocalAddress=" + ia.isLinkLocalAddress() + " isLoopbackAddress=" + ia.isLoopbackAddress());
		}
		System.out.println();
		System.out.println();
		System.out.println();
	}
}
