package com.onlyxiahui.common.utils.base.math;

import org.junit.Test;

/**
 * Description <br>
 * Date 2020-11-11 11:49:11<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class RateUtilTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(RateUtil.getPercentageIntegerRate(100L, 30L));
		System.out.println(RateUtil.getPercentageDecimalsRate(100L, 30L));
		System.out.println(RateUtil.getLessDecimalsRate(100L, 30L));

		System.out.println("-----------------------");
		System.out.println(RateUtil.getPercentageIntegerRate(30L, 100L));
		System.out.println(RateUtil.getPercentageDecimalsRate(30L, 100L));
		System.out.println(RateUtil.getLessDecimalsRate(30L, 100L));

		System.out.println("-----------------------");
		System.out.println(RateUtil.getPercentageIntegerRate(10L, 3L));
		System.out.println(RateUtil.getPercentageDecimalsRate(10L, 3L));
		System.out.println(RateUtil.getLessDecimalsRate(10L, 3L));

		System.out.println("-----------------------");
		System.out.println(RateUtil.getPercentageIntegerRate(100L, 42L));
		System.out.println(RateUtil.getPercentageDecimalsRate(100L, 42L));
		System.out.println(RateUtil.getLessDecimalsRate(100L, 42L));
	}

	@Test
	public void getPercentageDecimalsRate() {
		System.out.println(RateUtil.getPercentageDecimalsRate(100L, 30L));
	}

	@Test
	public void getLessDecimalsRate() {
		System.out.println(RateUtil.getLessDecimalsRate(100L, 30L));
	}
}
