package com.onlyxiahui.common.utils.base.lang.util.collection;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * <br>
 *
 * @date 2022-05-24 14:06:31<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ListConvertUtilTest {

	@Test
	public void convertList() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);
		List<String> stringList = ListConvertUtil.convertList(intList, (n) -> {
			return n + "";
		});

		stringList.forEach((v) -> {
			System.out.println(v);
		});
	}

	@Test
	public void convertListByKey() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);
		List<String> stringList = ListConvertUtil.convertListByKey(intList, (v) -> {
			return v + "";
		}, (k) -> {
			return k + "";
		});

		stringList.forEach((v) -> {
			System.out.println(v);
		});
	}

	@Test
	public void convertListByKeys() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);
		List<String> stringList = ListConvertUtil.convertListByKeys(intList, (v) -> {
			return v + "";
		}, (keys) -> {
			List<String> vList = ListConvertUtil.convertList(intList, (n) -> {
				return n + "";
			});
			return vList;
		});

		stringList.forEach((v) -> {
			System.out.println(v);
		});
	}

	@Test
	public void convertListBackByKeys() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);
		ListConvertUtil.convertListBackByKeys(intList, (n) -> {
			return n + "";
		}, (items) -> {
			List<String> stringList = ListConvertUtil.convertList(intList, (n) -> {
				return n + "";
			});
			return stringList;
		}, (stringList) -> {
			stringList.forEach((v) -> {
				System.out.println(v);
			});
		});
	}
}
