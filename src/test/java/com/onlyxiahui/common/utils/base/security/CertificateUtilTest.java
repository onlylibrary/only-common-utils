package com.onlyxiahui.common.utils.base.security;

import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.Test;

/**
 * Description <br>
 * Date 2020-11-09 14:21:10<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class CertificateUtilTest {

	@Test
	public void testX509ClassPath() {
		String classPath = "/data/security/rsa/client.crt";
		PublicKey key = CertificateUtil.getX509PublicKeyByClassPath(classPath);
		System.out.println(key);
	}

	@Test
	public void testX509FilePath() {
		String filePath = "file/security/rsa/client.crt";
		PublicKey key = CertificateUtil.getX509PublicKeyByFilePath(filePath);
		System.out.println(key);
	}

	/*******************************************/

	@Test
	public void testPemRsaPublicKeyClassPath() {
		String classPath = "/data/security/rsa/rsa_public_key.pem";
		PublicKey key = CertificateUtil.getPemRsaPublicKeyByClassPath(classPath);
		System.out.println(key);
		System.out.println(key.getAlgorithm());
		System.out.println(key.getEncoded().length);
	}

	@Test
	public void testPemRsaPublicKeyFilePath() {
		String filePath = "file/security/rsa/rsa_public_key.pem";
		PublicKey key = CertificateUtil.getPemRsaPublicKeyByFilePath(filePath);
		System.out.println(key);
	}

	/*******************************************/

	@Test
	public void testPemRsaPrivateKeyClassPath() {
		String classPath = "/data/security/rsa/rsa_private_key_pkcs8.pem";
		PrivateKey key = CertificateUtil.getPemRsaPrivateKeyByClassPath(classPath);
		System.out.println(key);
		System.out.println(key.getAlgorithm());
		System.out.println(key.getEncoded().length);
	}

	@Test
	public void testPemRsaPrivateKeyFilePath() {
		String filePath = "file/security/rsa/rsa_private_key_pkcs8.pem";
		PrivateKey key = CertificateUtil.getPemRsaPrivateKeyByFilePath(filePath);
		System.out.println(key);
	}

	/*******************************************/

	@Test
	public void testPemRsaPrivateTextKeyClassPath() {
		String classPath = "/data/security/rsa/rsa_private_key.pem";
		String key = CertificateUtil.getTextKeyByClassPath(classPath);
		System.out.println(key);
	}

	@Test
	public void testPemRsaPrivateTextKeyFilePath() {
		String filePath = "file/security/rsa/rsa_private_key.pem";
		String key = CertificateUtil.getTextKeyByFilePath(filePath);
		System.out.println(key);
	}

	@Test
	public void testPemRsaPublicTextKeyClassPath() {
		String classPath = "/data/security/rsa/rsa_public_key.pem";
		String key = CertificateUtil.getTextKeyByClassPath(classPath);
		System.out.println(key);
	}

	@Test
	public void testPemRsaPublicTextKeyFilePath() {
		String filePath = "file/security/rsa/rsa_public_key.pem";
		String key = CertificateUtil.getTextKeyByFilePath(filePath);
		System.out.println(key);
	}
}
