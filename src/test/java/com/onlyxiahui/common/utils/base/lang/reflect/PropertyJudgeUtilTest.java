package com.onlyxiahui.common.utils.base.lang.reflect;

import org.junit.Test;

/**
 * <br>
 *
 * @date 2022-05-24 17:41:35<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class PropertyJudgeUtilTest {

	@Test
	public void testP() {
		long i = 9;
		Object v = i;
		Class<?> classType = v.getClass();
		long t = System.currentTimeMillis();
		System.out.println(Long.class == classType);
		System.out.println(System.currentTimeMillis() - t);

		t = System.currentTimeMillis();
		System.out.println(long.class == classType);
		System.out.println(System.currentTimeMillis() - t);

		t = System.currentTimeMillis();
		System.out.println(long.class.isAssignableFrom(classType));
		System.out.println(System.currentTimeMillis() - t);

		t = System.currentTimeMillis();
		System.out.println(Long.class.isAssignableFrom(classType));
		System.out.println(System.currentTimeMillis() - t);
	}

	@Test
	public void testInteger() {
		int v1 = 1;
		Integer v2 = 1;
		show(v1);
		show(v2);
	}

	public static void show(Object o) {

		System.out.println("Value-isInteger:" + PropertyJudgeUtil.isInteger(o));
		System.out.println("Value-isLong:" + PropertyJudgeUtil.isLong(o));
		System.out.println("Value-isFloat:" + PropertyJudgeUtil.isFloat(o));
		System.out.println("Value-isDouble:" + PropertyJudgeUtil.isDouble(o));
		System.out.println("Value-isByte:" + PropertyJudgeUtil.isByte(o));
		System.out.println("Value-isCharacter:" + PropertyJudgeUtil.isCharacter(o));
		System.out.println("Value-isShort:" + PropertyJudgeUtil.isShort(o));
		System.out.println("Value-isBoolean:" + PropertyJudgeUtil.isBoolean(o));
		System.out.println("Value-isPrimitive:" + PropertyJudgeUtil.isPrimitive(o));
		System.out.println("Value-isString:" + PropertyJudgeUtil.isString(o));

		// System.out.println(PropertyJudgeUtil.isArray(o.getClass()));
		System.out.println("--------------------------------------------------");
	}
}
