package com.onlyxiahui.common.utils.base.lang.util.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**
 * <br>
 *
 * @date 2022-05-24 14:15:01<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ListMapUtilTest {

	@Test
	public void listToMapByKey() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);

		Map<String, Integer> map = ListMapUtil.listToMapByKey(intList, (v) -> {
			return v + "";
		});

		for (Map.Entry<String, Integer> e : map.entrySet()) {
			System.out.println("key:" + e.getKey() + "=" + e.getValue());
		}
	}

	@Test
	public void listToMapListByKey() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);

		Map<String, List<Integer>> map = ListMapUtil.listToMapListByKey(intList, (v) -> {
			return v + "";
		});
		for (Map.Entry<String, List<Integer>> e : map.entrySet()) {
			String key = e.getKey();
			List<Integer> values = e.getValue();
			System.out.println("key:" + key);
			if (null != values) {
				System.out.println("   value:" + values);
			}
		}
	}

	@Test
	public void listToMapConvertByKey() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);
		Map<String, String> map = ListMapUtil.listToMapConvertByKey(intList, (v) -> {
			return v + "";
		}, (k) -> {
			return k + "";
		});
		for (Map.Entry<String, String> e : map.entrySet()) {
			System.out.println("key:" + e.getKey() + "=" + e.getValue());
		}
	}

	@Test
	public void listToMapListConvertByKey() {
		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);

		Map<String, List<String>> map = ListMapUtil.listToMapListConvertByKey(intList, (v) -> {
			return v + "";
		}, (k) -> {
			return k + "";
		});
		for (Map.Entry<String, List<String>> e : map.entrySet()) {
			String key = e.getKey();
			List<String> values = e.getValue();
			System.out.println("key:" + key);
			if (null != values) {
				System.out.println("   value:" + values);
			}
		}
	}

	@Test
	public void listToMapConvertByKeys() {

		List<Integer> intList = new ArrayList<>();
		intList.add(3);
		intList.add(1);
		intList.add(4);
		intList.add(1);
		intList.add(5);

		Map<String, String> map = ListMapUtil.listToMapConvertByKeys(intList, (v) -> {
			return v + "";
		}, (keys) -> {
			List<String> stringList = ListConvertUtil.convertList(keys, (n) -> {
				return n + "";
			});
			return stringList;
		}, (v) -> {
			return v + "";
		});
		for (Map.Entry<String, String> e : map.entrySet()) {
			System.out.println("key:" + e.getKey() + "=" + e.getValue());
		}
	}

	/**********************************************/

	@Test
	public void listToMapBackByKey() {
	}

	@Test
	public void listToMapListBackByKey() {
	}

	@Test
	public void listToMapConvertBackByKey() {
	}

	@Test
	public void listToMapListConvertBackByKey() {
	}

	@Test
	public void listToMapConvertBackByKeys() {
	}
}
