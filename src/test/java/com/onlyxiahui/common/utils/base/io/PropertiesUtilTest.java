package com.onlyxiahui.common.utils.base.io;

import com.onlyxiahui.common.utils.base.io.file.PropertiesUtil;

public class PropertiesUtilTest {

	public static void main(String[] args) {
		System.out.println(PropertiesUtil.getPropertyByClassPath("/config/config.properties", "only.property"));
		System.out.println(PropertiesUtil.getPropertyByFilePath("file/config/config.properties", "only.property"));
	}
}
