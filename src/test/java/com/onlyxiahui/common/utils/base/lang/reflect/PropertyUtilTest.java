package com.onlyxiahui.common.utils.base.lang.reflect;

import java.math.BigDecimal;

import com.onlyxiahui.common.bean.Book;
import com.onlyxiahui.common.utils.base.util.time.DateUtil;

/**
 * Description <br>
 * Date 2020-11-10 09:03:56<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class PropertyUtilTest {

	public static void main(String[] args) {
		Book bookOld = new Book();

		bookOld.setId("1000");
		bookOld.setName("Mark中国 ");

		bookOld.setPageCount(55);
		bookOld.setPrice(new BigDecimal("20.33"));
		bookOld.setSellDate(DateUtil.stringDateToDate("2020-10-11"));
		bookOld.setWeight(33.22D);

		System.out.println(PropertyJudgeUtil.isPrimitive(bookOld.getPageCount()));

		System.out.println(PropertyJudgeUtil.isInteger(bookOld.getPageCount()));
		System.out.println(PropertyJudgeUtil.isLong(bookOld.getPageCount()));
	}
}
