package com.onlyxiahui.common.utils.base.security;

import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.Test;

/**
 * Description <br>
 * Date 2020-11-09 15:07:59<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class SecuritySignatureUtilTest {

	@Test
	public void testSignature() {
		String privateKeyClassPath = "/data/security/rsa/rsa_private_key_pkcs8.pem";
		PrivateKey privateKey = CertificateUtil.getPemRsaPrivateKeyByClassPath(privateKeyClassPath);

		String publicKeyClassPath = "/data/security/rsa/rsa_public_key.pem";
		PublicKey publicKey = CertificateUtil.getPemRsaPublicKeyByClassPath(publicKeyClassPath);

		String content = "今晚打老虎,哈哈啥哈哈啥啊哈哈！";

		String sign = SecuritySignatureUtil.signatureSha1WithRsa(content, privateKey);

		boolean mark = SecuritySignatureUtil.verifySha1WithRsa(content, sign, publicKey);
		System.out.println(mark);
	}
}
