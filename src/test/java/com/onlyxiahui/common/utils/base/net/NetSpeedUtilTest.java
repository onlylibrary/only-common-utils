package com.onlyxiahui.common.utils.base.net;

import org.junit.Test;

/**
 * Description <br>
 * Date 2020-11-11 11:45:01<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class NetSpeedUtilTest {

	@Test
	public void getSpeedTextBySecond() {
		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L, 1000L));
		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L + 380L, 1000L));
		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L * 1024L * 1024L, 1000L));
		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L * 1024L * 1024L + 380L, 1000L));

		System.out.println("------------------------");

		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L, 60L));
		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L + 380L, 60L));
		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L * 1024L * 1024L, 60L));
		System.out.println(NetSpeedUtil.getSpeedTextOfSecondByMillisecond(1024L * 1024L * 1024L + 380L, 60L));
	}

}
