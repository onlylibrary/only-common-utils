package com.onlyxiahui.common.utils.base.lang;

import org.junit.Test;

/**
 * <br>
 *
 * @date 2022-05-24 10:32:00<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class CompareUtilTest {

	public static void main(String[] args) {

	}

	@Test
	public void isEqual() {
		System.out.println(CompareUtil.isEqual(1, 1));
		System.out.println(CompareUtil.isEqual(1, 2));
		System.out.println(CompareUtil.isEqual(1L, 1L));
		System.out.println(CompareUtil.isEqual(1L, 2L));
		System.out.println(CompareUtil.isEqual(1.1, 1.10));
		System.out.println(CompareUtil.isEqual(1.1, 1.20));
		System.out.println(CompareUtil.isEqual(1.1D, 1.1D));
		System.out.println(CompareUtil.isEqual(1.1D, 1.2D));
	}

	@Test
	public void isGreater() {
		System.out.println(CompareUtil.isGreater(1, 1));
		System.out.println(CompareUtil.isGreater(1, 2));
		System.out.println(CompareUtil.isGreater(2, 1));

		System.out.println(CompareUtil.isGreater(1L, 1L));
		System.out.println(CompareUtil.isGreater(1L, 2L));
		System.out.println(CompareUtil.isGreater(2L, 1L));

		System.out.println(CompareUtil.isGreater(1.1, 1.10));
		System.out.println(CompareUtil.isGreater(1.1, 1.20));
		System.out.println(CompareUtil.isGreater(1.2, 1.10));

		System.out.println(CompareUtil.isGreater(1.1D, 1.1D));
		System.out.println(CompareUtil.isGreater(1.1D, 1.2D));
		System.out.println(CompareUtil.isGreater(1.2D, 1.1D));
	}

	@Test
	public void isGreaterEqual() {
		System.out.println(CompareUtil.isGreaterEqual(1, 1));
		System.out.println(CompareUtil.isGreaterEqual(1, 2));
		System.out.println(CompareUtil.isGreaterEqual(2, 1));

		System.out.println(CompareUtil.isGreaterEqual(1L, 1L));
		System.out.println(CompareUtil.isGreaterEqual(1L, 2L));
		System.out.println(CompareUtil.isGreaterEqual(2L, 1L));

		System.out.println(CompareUtil.isGreaterEqual(1.1, 1.10));
		System.out.println(CompareUtil.isGreaterEqual(1.1, 1.20));
		System.out.println(CompareUtil.isGreaterEqual(1.2, 1.10));

		System.out.println(CompareUtil.isGreaterEqual(1.1D, 1.1D));
		System.out.println(CompareUtil.isGreaterEqual(1.1D, 1.2D));
		System.out.println(CompareUtil.isGreaterEqual(1.2D, 1.1D));
	}

	@Test
	public void isLess() {
		System.out.println(CompareUtil.isLess(1, 1));
		System.out.println(CompareUtil.isLess(1, 2));
		System.out.println(CompareUtil.isLess(2, 1));

		System.out.println(CompareUtil.isLess(1L, 1L));
		System.out.println(CompareUtil.isLess(1L, 2L));
		System.out.println(CompareUtil.isLess(2L, 1L));

		System.out.println(CompareUtil.isLess(1.1, 1.10));
		System.out.println(CompareUtil.isLess(1.1, 1.20));
		System.out.println(CompareUtil.isLess(1.2, 1.10));

		System.out.println(CompareUtil.isLess(1.1D, 1.1D));
		System.out.println(CompareUtil.isLess(1.1D, 1.2D));
		System.out.println(CompareUtil.isLess(1.2D, 1.1D));
	}

	@Test
	public void isLessEqual() {
		System.out.println(CompareUtil.isLessEqual(1, 1));
		System.out.println(CompareUtil.isLessEqual(1, 2));
		System.out.println(CompareUtil.isLessEqual(2, 1));

		System.out.println(CompareUtil.isLessEqual(1L, 1L));
		System.out.println(CompareUtil.isLessEqual(1L, 2L));
		System.out.println(CompareUtil.isLessEqual(2L, 1L));

		System.out.println(CompareUtil.isLessEqual(1.1, 1.10));
		System.out.println(CompareUtil.isLessEqual(1.1, 1.20));
		System.out.println(CompareUtil.isLessEqual(1.2, 1.10));

		System.out.println(CompareUtil.isLessEqual(1.1D, 1.1D));
		System.out.println(CompareUtil.isLessEqual(1.1D, 1.2D));
		System.out.println(CompareUtil.isLessEqual(1.2D, 1.1D));
	}
}
