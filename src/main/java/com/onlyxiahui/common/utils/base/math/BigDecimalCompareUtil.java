package com.onlyxiahui.common.utils.base.math;

import java.math.BigDecimal;

/**
 * Description <br>
 * Date 2020-12-24 15:12:28<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class BigDecimalCompareUtil {

	static BigDecimal zero = new BigDecimal("0");

	/**
	 * 
	 * 等于0 <br>
	 * Date 2020-12-24 15:17:27<br>
	 * 
	 * @param data
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isEqualZero(BigDecimal data) {
		boolean is = false;
		if (null != data) {
			is = data.compareTo(zero) == 0;
		}
		return is;
	}

	/**
	 * 
	 * 大于0 <br>
	 * Date 2020-12-24 15:17:38<br>
	 * 
	 * @param data
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isGreaterZero(BigDecimal data) {
		boolean is = false;
		if (null != data) {
			is = data.compareTo(zero) > 0;
		}
		return is;
	}

	/**
	 * 
	 * 大于等于0 <br>
	 * Date 2020-12-24 15:17:58<br>
	 * 
	 * @param data
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isGreaterEqualZero(BigDecimal data) {
		return isGreaterZero(data) || isEqualZero(data);
	}

	/**
	 * 
	 * 小于0 <br>
	 * Date 2020-12-24 15:18:34<br>
	 * 
	 * @param data
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isLessZero(BigDecimal data) {
		boolean is = false;
		if (null != data) {
			is = data.compareTo(zero) < 0;
		}
		return is;
	}

	/**
	 * 
	 * 小于等于0 <br>
	 * Date 2020-12-24 15:18:34<br>
	 * 
	 * @param data
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isLessEqualZero(BigDecimal data) {
		return isLessZero(data) || isEqualZero(data);
	}

	/********************************************/

	public static boolean isEqual(BigDecimal b1, BigDecimal b2) {
		boolean is = false;
		if (null != b1 && null != b2) {
			is = b1.compareTo(b2) == 0;
		}
		return is;
	}

	/**
	 * 
	 * 大于<br>
	 * Date 2020-12-31 11:13:26<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isGreater(BigDecimal b1, BigDecimal b2) {
		boolean is = false;
		if (null != b1 && null != b2) {
			is = b1.compareTo(b2) > 0;
		}
		return is;
	}

	/**
	 * 
	 * 大于等于<br>
	 * Date 2020-12-31 11:14:02<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isGreaterEqual(BigDecimal b1, BigDecimal b2) {
		return isGreater(b1, b2) || isEqual(b1, b2);
	}

	/**
	 * 
	 * 小于 <br>
	 * Date 2020-12-31 11:15:01<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isLess(BigDecimal b1, BigDecimal b2) {
		boolean is = false;
		if (null != b1 && null != b2) {
			is = b1.compareTo(b2) < 0;
		}
		return is;
	}

	/**
	 * 
	 * 小于等于<br>
	 * Date 2020-12-31 11:14:52<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isLessEqual(BigDecimal b1, BigDecimal b2) {
		return isLess(b1, b2) || isEqual(b1, b2);
	}
}
