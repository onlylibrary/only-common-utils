package com.onlyxiahui.common.utils.base.beans;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * Description 对象转Map工具 <br>
 * Date 2020-11-09 18:53:55<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class BeanToMapUtil {

	private static Map<Class<?>, List<PropertyDescriptor>> listMap = new ConcurrentHashMap<Class<?>, List<PropertyDescriptor>>();
	private static Map<Class<?>, Map<String, PropertyDescriptor>> keyMap = new ConcurrentHashMap<Class<?>, Map<String, PropertyDescriptor>>();

	/**
	 * 
	 * Description 对象转map <br>
	 * Date 2020-11-09 18:57:08<br>
	 * 
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static Map<String, Object> toMap(Object value) {
		return toMap(value, null, null);
	}

	/**
	 * 
	 * Description 对象值put进map <br>
	 * Date 2020-11-09 18:57:22<br>
	 * 
	 * @param value
	 * @param map
	 * @return
	 * @since 1.0.0
	 */
	public static Map<String, Object> toMap(Object value, Map<String, Object> map) {
		return toMap(value, map, null);
	}

	/**
	 * 
	 * Description 按nameList转map <br>
	 * Date 2020-11-09 18:57:43<br>
	 * 
	 * @param value
	 * @param nameList
	 * @return
	 * @since 1.0.0
	 */

	public static Map<String, Object> toMap(Object value, List<String> nameList) {
		return toMap(value, null, nameList);
	}

	/**
	 * 
	 * Description 安指定字段put进map<br>
	 * Date 2020-11-09 18:55:55<br>
	 * 
	 * @param value
	 * @param dataMap
	 * @param nameList
	 * @return
	 * @since 1.0.0
	 */
	public static Map<String, Object> toMap(Object value, Map<String, Object> dataMap, List<String> nameList) {
		if (null == dataMap) {
			int size = null == nameList ? 20 : nameList.size();
			dataMap = new HashMap<String, Object>(size);
		}

		try {
			if (null != value) {

				if (null == nameList || nameList.isEmpty()) {
					if (value instanceof Map) {
						Map<?, ?> map = (Map<?, ?>) value;
						Set<?> keySet = map.keySet();
						for (Object key : keySet) {
							Object data = map.get(key);
							String name = null == key ? "" : key.toString();

							dataMap.put(name, data);
						}
					} else {
						List<PropertyDescriptor> propertyDescriptorList = getReadMethodList(value.getClass());
						for (PropertyDescriptor pd : propertyDescriptorList) {
							String propertyName = pd.getName();
							Method readMethod = pd.getReadMethod();
							Object data = readMethod.invoke(value);

							dataMap.put(propertyName, data);
						}
					}
				} else {
					if (value instanceof Map) {
						Map<?, ?> map = (Map<?, ?>) value;
						for (String name : nameList) {
							Object data = map.get(name);

							dataMap.put(name, data);
						}
					} else {
						Map<String, PropertyDescriptor> map = getReadMethodMap(value.getClass());
						for (String name : nameList) {
							PropertyDescriptor pd = map.get(name);
							if (null != pd) {
								String propertyName = pd.getName();
								Method readMethod = pd.getReadMethod();
								Object data = readMethod.invoke(value);

								dataMap.put(propertyName, data);
							}
						}
					}
				}
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return dataMap;
	}

	public static List<PropertyDescriptor> getReadMethodList(Class<?> classType) {
		List<PropertyDescriptor> list = listMap.get(classType);
		try {
			if (null == list) {
				list = new ArrayList<PropertyDescriptor>();
				BeanInfo bi = Introspector.getBeanInfo(classType, Object.class);
				PropertyDescriptor[] pds = bi.getPropertyDescriptors();
				for (PropertyDescriptor pd : pds) {
					if (null != pd.getReadMethod()) {
						list.add(pd);
					}
				}
				listMap.put(classType, list);
			}
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static Map<String, PropertyDescriptor> getReadMethodMap(Class<?> classType) {
		Map<String, PropertyDescriptor> map = keyMap.get(classType);
		try {
			if (null == map) {

				BeanInfo bi = Introspector.getBeanInfo(classType, Object.class);
				PropertyDescriptor[] pds = bi.getPropertyDescriptors();
				map = new HashMap<String, PropertyDescriptor>(pds.length);
				for (PropertyDescriptor pd : pds) {
					if (null != pd.getReadMethod()) {
						map.put(pd.getName(), pd);
					}
				}
				keyMap.put(classType, map);
			}
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return map;
	}
}
