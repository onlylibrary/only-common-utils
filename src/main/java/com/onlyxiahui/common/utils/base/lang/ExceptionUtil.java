package com.onlyxiahui.common.utils.base.lang;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * Description 封装对异常的常用工具<br>
 * Date 2020-11-09 19:14:00<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class ExceptionUtil {

	/**
	 * 
	 * Description 将调用栈信息转换成字符串形式，以方便记录在在日记。<br>
	 * Date 2020-11-09 19:15:24<br>
	 * 
	 * @param exception
	 * @return String
	 * @since 1.0.0
	 */
	public static String getStackTraceAsString(Throwable exception) {
		if (null == exception) {
			return "";
		}
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}
}
