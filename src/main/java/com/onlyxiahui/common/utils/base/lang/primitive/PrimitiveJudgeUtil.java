package com.onlyxiahui.common.utils.base.lang.primitive;

/**
 * Date 2019-01-11 14:14:40<br>
 * Description
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class PrimitiveJudgeUtil {

	public static boolean isInteger(Object o) {
		return (o instanceof Integer);
	}

	public static boolean isLong(Object o) {
		return (o instanceof Long);
	}

	public static boolean isFloat(Object o) {
		return (o instanceof Float);
	}

	public static boolean isDouble(Object o) {
		return (o instanceof Double);
	}

	public static boolean isByte(Object o) {
		return (o instanceof Byte);
	}

	public static boolean isCharacter(Object o) {
		return (o instanceof Character);
	}

	public static boolean isShort(Object o) {
		return (o instanceof Short);
	}

	public static boolean isBoolean(Object o) {
		return (o instanceof Boolean);
	}
}
