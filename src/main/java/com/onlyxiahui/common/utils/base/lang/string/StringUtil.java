package com.onlyxiahui.common.utils.base.lang.string;

/**
 * Date 2019-03-30 17:05:53<br>
 * Description
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class StringUtil {

	public static boolean isNotEmpty(final CharSequence cs) {
		return !StringJudgeUtil.isEmpty(cs);
	}

	public static boolean isEmpty(final CharSequence cs) {
		return StringJudgeUtil.isEmpty(cs);
	}

	public static boolean isNotBlank(final CharSequence cs) {
		return !isBlank(cs);
	}

	public static boolean isBlank(final CharSequence cs) {
		return StringJudgeUtil.isBlank(cs);
	}
}
