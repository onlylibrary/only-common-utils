package com.onlyxiahui.common.utils.base.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * 
 * Description 
 * <br>
 * Date 2020-11-10 13:38:48<br>
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class Md5Util {

	/**
	 * --对文本进行32位小写MD5加密
	 * 
	 * @param text ：要进行加密的文本
	 * @return String ：加密后的内容
	 */
	public static String lower32(String text) {
		return lower32(text, null);
	}

	/**
	 * --对文本进行32位小写MD5加密
	 * 
	 * @param text
	 * @param charset ：字符集
	 * @return String
	 */
	public static String lower32(String text, String charset) {
		String result = null;
		// 首先判断是否为空
		if (null != text && !"".equals(text)) {
			try {
				// 首先进行实例化和初始化
				MessageDigest md = MessageDigest.getInstance("MD5");
				// 得到一个操作系统默认的字节编码格式的字节数组
				byte[] bytes = (null == charset) ? text.getBytes() : text.getBytes(charset);
				// 对得到的字节数组进行处理
				md.update(bytes);
				// 进行哈希计算并返回结果
				byte[] btResult = md.digest();
				// 进行哈希计算后得到的数据的长度
				StringBuffer sb = new StringBuffer();
				for (byte b : btResult) {
					int bt = b & 0xff;
					if (bt < 16) {
						sb.append(0);
					}
					sb.append(Integer.toHexString(bt));
				}
				result = sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * --对文本进行32位大写MD5加密
	 * 
	 * @param text
	 * @return String
	 */
	public static String upper32(String text) {
		String result = lower32(text);
		if (null != result && !"".equals(result)) {
			result = result.toUpperCase();
		}
		return result;
	}

	/**
	 * -- 对文本进行32位大写MD5加密
	 * 
	 * @param text
	 * @param charset
	 * @return String
	 */
	public static String upper32(String text, String charset) {
		String result = lower32(text, charset);
		if (null != result && !"".equals(result)) {
			result = result.toUpperCase();
		}
		return result;
	}

	/**
	 * --对文本进行16位小写MD5加密
	 * 
	 * @param text
	 * @return String
	 */
	public static String lower16(String text) {
		String value = lower32(text);
		return null == value ? "" : value.substring(8, 24);
	}

	/**
	 * -- 对文本进行16位大写MD5加密
	 * 
	 * @param text
	 * @return String
	 */
	public static String upper16(String text) {
		String value = upper32(text);
		return null == value ? "" : value.substring(8, 24);
	}
}