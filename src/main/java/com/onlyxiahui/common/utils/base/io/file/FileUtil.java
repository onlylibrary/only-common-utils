package com.onlyxiahui.common.utils.base.io.file;

import java.io.File;
import java.io.IOException;

/**
 * Date 2019-01-11 14:13:04<br>
 * Description
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class FileUtil {

	/**
	 * Description 文件夹否存在
	 *
	 * @param path
	 * @return boolean
	 */
	public static boolean isFolderExists(String path) {
		File file = new File(path);
		return (file.exists() && file.isDirectory());
	}

	/**
	 * Description 创建文件夹（全路径都为文件夹，最后的目录不是文件）
	 *
	 * @param folderPath
	 */
	public static void createFullFolder(String folderPath) {
		File folder = new File(folderPath);
		File parent = folder.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		if (!folder.exists()) {
			folder.mkdir();
		}
	}

	/**
	 * Description 创建文件夹（只创建父文件夹）
	 *
	 * @param path
	 */
	public static void createParentFolder(String path) {
		File file = new File(path);
		File parent = file.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
	}

	/**
	 * Description 检查并创建，创建文件夹（全路径都为文件夹）
	 *
	 * @param folderPath
	 */
	public static void checkOrCreateFolder(String folderPath) {
		File folder = new File(folderPath);
		File parent = folder.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		if (!folder.exists()) {
			folder.mkdir();
		}
	}

	/**
	 * Description 文件是否存在
	 *
	 * @param path
	 * @return boolean
	 */
	public static boolean isFileExists(String path) {
		File file = new File(path);
		return (file.exists() && !file.isDirectory());
	}

	/**
	 * Description 创建文件，自动创建父文件夹
	 *
	 * @param path
	 */
	public static void createFile(String path) {
		File file = new File(path);
		File parent = file.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Description 检查并创建文件
	 *
	 * @param path
	 */
	public static void checkOrCreateFile(String path) {
		File file = new File(path);
		File parent = file.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
