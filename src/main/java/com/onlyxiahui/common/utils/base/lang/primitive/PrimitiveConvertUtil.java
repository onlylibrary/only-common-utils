package com.onlyxiahui.common.utils.base.lang.primitive;

/**
 * Date 2019-01-11 14:14:40<br>
 * Description
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class PrimitiveConvertUtil {

	/**
	 * Date 2019-03-30 17:17:16<br>
	 * Description int to long
	 *
	 * @param i
	 * @return long
	 * @since 1.0.0
	 */
	public static long intToLong(int i) {
		long l = i & 0x7fffffffL;
		if (i < 0) {
			l |= 0x080000000L;
		}
		return l;
	}
}
