package com.onlyxiahui.common.utils.base.lang.number;

import java.math.BigDecimal;

/**
 * 
 * Description <br>
 * Date 2019-04-01 10:18:21<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class NumberArithmeticUtil {

	public static final int DEFAULT_SCALE = 10;

	/**
	 * 两个Number数相加
	 *
	 * @param v1
	 * @param v2
	 * @return BigDecimal
	 */
	public static <N extends Number> BigDecimal add(N v1, N v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.add(b2);
	}

	/**
	 * 两个Number数相减
	 *
	 * @param v1
	 * @param v2
	 * @return BigDecimal
	 */
	public static <N extends Number> BigDecimal subtract(N v1, N v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.subtract(b2);
	}

	/**
	 * 两个Number数相乘
	 *
	 * @param v1
	 * @param v2
	 * @return Number
	 */
	public static <N extends Number> BigDecimal multiply(N v1, N v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.multiply(b2);
	}

	/**
	 * 两个Number数相除
	 *
	 * @param v1
	 * @param v2
	 * @return Number
	 */
	public static <N extends Number> BigDecimal divide(N v1, N v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.divide(b2, DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);

	}

	/**
	 * 两个Number数相除，并保留scale位小数
	 *
	 * @param v1
	 * @param v2
	 * @param scale
	 * @return BigDecimal
	 */
	public static <N extends Number> BigDecimal divide(N v1, N v2, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException("The scale must be a positive integer or zero");
		}
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 返回两个数里比较大的值
	 *
	 * @param v1
	 * @param v2
	 * @return BigDecimal
	 */
	public static <N extends Number> BigDecimal max(N v1, N v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.max(b2);
	}

	/**
	 * 返回两个数里比较小的值
	 *
	 * @param v1
	 * @param v2
	 * @return BigDecimal
	 */
	public static <N extends Number> BigDecimal min(N v1, N v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.min(b2);
	}
}
