package com.onlyxiahui.common.utils.base.util;

import java.util.UUID;

/**
 * 
 * Description UUID <br>
 * Date 2019-01-11 14:06:03<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class UuidUtil {

	/**
	 * 
	 * Description 获取去掉“-”的uuid <br>
	 * Date 2019-03-30 17:28:53<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getUuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
