package com.onlyxiahui.common.utils.base.lang.bytes;

/**
 * 
 * Date 2019-01-11 14:24:10<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class BytesConvertUtil {

	/**
	 * 
	 * Description 字节转整型 <br>
	 * Date 2019-04-01 09:51:17<br>
	 * 
	 * @param bytes
	 * @return int
	 * @since 1.0.0
	 */
	public static int bytesToInt(byte[] bytes) {
		int value = 0;
		for (int i = 0; i < bytes.length; i++) {
			value += (bytes[i] & 0XFF) << (8 * (3 - i));
		}
		return value;
	}

	public static byte[] intToBytes(int value) {
		int length = 4;
		byte[] bytes = new byte[4];
		for (int i = 0; i < length; i++) {
			bytes[i] = (byte) (value >> (24 - i * 8));
		}
		return bytes;
	}

	public static byte[] bytesMerge(byte[] bytes1, byte[] bytes2) {
		byte[] bytes3 = new byte[bytes1.length + bytes2.length];
		System.arraycopy(bytes1, 0, bytes3, 0, bytes1.length);
		System.arraycopy(bytes2, 0, bytes3, bytes1.length, bytes2.length);
		return bytes3;
	}

	/**
	 * 
	 * Description byte to int <br>
	 * Date 2019-04-01 09:51:34<br>
	 * 
	 * @param b
	 * @return int
	 * @since 1.0.0
	 */
	public static int byteToInt(byte b) {
		int l = b & 0x07f;
		if (b < 0) {
			l |= 0x80;
		}
		return l;
	}

	/**
	 * 将byte数组转换为表示16进制值的字符串， 如：byte[]{8,18}转换为：0813，<br>
	 * 和public static byte[]hexTextToBytes(String hexText) 互为可逆的转换过程
	 * 
	 * @param bytes ：需要转换的byte数组
	 * @return String ：转换后的字符串
	 * @throws Exception:本方法不处理任何异常，所有异常全部抛出
	 */
	public static String bytesToHexText(byte[] bytes) {
		int length = bytes.length;
		// 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍
		StringBuffer sb = new StringBuffer(length * 2);
		for (int i = 0; i < length; i++) {
			int intTemp = bytes[i];
			// 把负数转换为正数
			while (intTemp < 0) {
				intTemp = intTemp + 256;
			}
			// 小于0F的数需要在前面补0
			if (intTemp < 16) {
				sb.append("0");
			}
			sb.append(Integer.toString(intTemp, 16));
		}
		return sb.toString();
	}

	/**
	 * 将表示16进制值的字符串转换为byte数组，<br>
	 * 和public static String bytesToHexText(byte[] bytes) 互为可逆的转换过程
	 * 
	 * @param hexText ：需要转换的字符串
	 * @return byte[]：转换后的byte数组
	 * @throws Exception ：本方法不处理任何异常，所有异常全部抛出
	 */
	public static byte[] hexTextToBytes(String hexText) {
		byte[] bytes = hexText.getBytes();
		int length = bytes.length;
		int v = 2;
		// 两个字符表示一个字节，所以字节数组长度是字符串长度除以2
		byte[] array = new byte[length / 2];
		for (int i = 0; i < length; i = i + v) {
			String temp = new String(bytes, i, 2);
			array[i / 2] = (byte) Integer.parseInt(temp, 16);
		}
		return array;
	}
}
