package com.onlyxiahui.common.utils.base.lang.string;

/**
 * Date 2019-03-30 17:05:53<br>
 * Description
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class StringReplaceUtil {

	/**
	 * 替换字符串中的一节，从beginIndex开始到endIndex结尾，原字符串中的这一部分被替换成replace<br>
	 * Date 2019-03-30 17:05:41<br>
	 * 
	 * @param text
	 * @param replace
	 * @param beginIndex
	 * @param endIndex
	 * @return String
	 * @since 1.0.0
	 */
	public static String replacePartString(String text, String replace, int beginIndex, int endIndex) {
		String value = null;
		if (null != text) {
			StringBuilder sb = new StringBuilder();
			char[] chars = text.toCharArray();
			int length = chars.length;
			for (int i = 0; i < length; i++) {
				if (beginIndex > i) {
					sb.append(chars[i]);
				}
			}
			sb.append(replace);
			for (int i = 0; i < length; i++) {
				if (i > endIndex) {
					sb.append(chars[i]);
				}
			}
			value = sb.toString();
		}
		return value;
	}

	/**
	 * 
	 * 每个字符替换为目标字符串 <br>
	 * Date 2020-12-24 16:05:13<br>
	 * 
	 * @param text       原字符串
	 * @param replace    目标字符串
	 * @param beginIndex 替换开始
	 * @param endIndex   替换结束
	 * @return
	 * @since 1.0.0
	 */
	public static String replaceEachChar(String text, String replace, int beginIndex, int endIndex) {
		String value = null;
		if (null != text) {
			StringBuilder sb = new StringBuilder();
			char[] chars = text.toCharArray();
			int length = chars.length;
			for (int i = 0; i < length; i++) {
				if (beginIndex <= i && i <= endIndex) {
					sb.append(replace);
				} else {
					sb.append(chars[i]);
				}
			}
			value = sb.toString();
		}
		return value;
	}

	/**
	 * 
	 * 每个字符替换为目标字符 <br>
	 * Date 2020-12-24 16:05:13<br>
	 * 
	 * @param text       原字符串
	 * @param replace    目标字符
	 * @param beginIndex 替换开始
	 * @param endIndex   替换结束
	 * @return
	 * @since 1.0.0
	 */
	public static String replaceEachChar(String text, char replace, int beginIndex, int endIndex) {
		String value = null;
		if (null != text) {
			StringBuilder sb = new StringBuilder();
			char[] chars = text.toCharArray();
			int length = chars.length;
			for (int i = 0; i < length; i++) {
				if (beginIndex <= i && i <= endIndex) {
					sb.append(replace);
				} else {
					sb.append(chars[i]);
				}
			}
			value = sb.toString();
		}
		return value;
	}

	/**
	 * 
	 * 保持后部分长（endKeepLength）度后，向前部替换指定长度（replaceLength）的每个字符为目标字符串（replace） <br>
	 * Date 2020-12-24 16:14:22<br>
	 * 
	 * @param text
	 * @param replace
	 * @param replaceLength
	 * @param endKeepLength
	 * @return
	 * @since 1.0.0
	 */
	public static String replaceBeforeEachChar(String text, String replace, int replaceLength, int endKeepLength) {
		String value = null;
		if (null != text) {
			StringBuilder sb = new StringBuilder();
			char[] chars = text.toCharArray();
			int length = chars.length;
			int endIndex = (length - endKeepLength);
			int beginIndex = (endIndex - replaceLength);
			beginIndex = (beginIndex < 0) ? 0 : beginIndex;
			for (int i = 0; i < length; i++) {
				if (beginIndex <= i && i < endIndex) {
					sb.append(replace);
				} else {
					sb.append(chars[i]);
				}
			}
			value = sb.toString();
		}
		return value;
	}

	/**
	 * 
	 * 保持前部分长（beginKeepLength）度后，向后部替换指定长度（replaceLength）的每个字符为目标字符串（replace） <br>
	 * Date 2020-12-24 16:22:31<br>
	 * 
	 * @param text
	 * @param replace
	 * @param replaceLength
	 * @param beginKeepLength
	 * @return
	 * @since 1.0.0
	 */
	public static String replaceAfterEachChar(String text, String replace, int replaceLength, int beginKeepLength) {
		String value = null;
		if (null != text) {
			StringBuilder sb = new StringBuilder();
			char[] chars = text.toCharArray();
			int length = chars.length;
			int endIndex = (beginKeepLength + replaceLength);
			int beginIndex = (beginKeepLength);
			endIndex = (endIndex >= length) ? length : endIndex;
			for (int i = 0; i < length; i++) {
				if (beginIndex <= i && i < endIndex) {
					sb.append(replace);
				} else {
					sb.append(chars[i]);
				}
			}
			value = sb.toString();
		}
		return value;
	}

	/**
	 * 
	 * 替换中间部分的字符串；保留前部分长度beginLength和后部分长度endLength；<br>
	 * 中间的每一个字符都被替换成replace<br>
	 * Date 2019-03-30 17:07:30<br>
	 * 
	 * @param text
	 * @param replace
	 * @param beginLength
	 * @param endLength
	 * @return String
	 * @since 1.0.0
	 */
	public static String replaceMiddleEachChar(String text, String replace, int beginLength, int endLength) {
		String value = null;
		if (null != text) {
			StringBuilder sb = new StringBuilder();
			char[] chars = text.toCharArray();
			int length = chars.length;
			int endIndex = (length - endLength);
			for (int i = 0; i < length; i++) {
				if (beginLength <= i && i < endIndex) {
					sb.append(replace);
				} else {
					sb.append(chars[i]);
				}
			}
			value = sb.toString();
		}
		return value;
	}
}
