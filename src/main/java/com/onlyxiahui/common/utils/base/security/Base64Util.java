package com.onlyxiahui.common.utils.base.security;

import java.util.Base64;

/**
 * Description <br>
 * Date 2020-11-09 15:45:43<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class Base64Util {

	/**
	 * 
	 * Description 编码 <br>
	 * Date 2020-11-09 15:46:32<br>
	 * 
	 * @param bytes
	 * @return
	 * @since 1.0.0
	 */
	public static String encrypt(byte[] bytes) {
		return new String(Base64.getEncoder().encode(bytes));
	}

	/**
	 * 
	 * Description 解码 <br>
	 * Date 2020-11-09 15:46:39<br>
	 * 
	 * @param text
	 * @return
	 * @since 1.0.0
	 */
	public static byte[] decrypt(String text) {
		return Base64.getDecoder().decode(text);
	}
}
