package com.onlyxiahui.common.utils.base.lang.primitive;

import com.onlyxiahui.common.utils.base.lang.string.StringUtil;
import com.onlyxiahui.common.utils.base.lang.string.StringValueUtil;

/**
 * 
 * Description <br>
 * Date 2019-04-01 10:18:21<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class PrimitiveParseUtil {
	/**
	 * 字符串转换为int,如果字符串为空则返回默认值
	 *
	 * @param text
	 * @param defaultValue
	 * @return int
	 */
	public static int parseIntger(String text, int defaultValue) {
		if (StringUtil.isBlank(text)) {
			return defaultValue;
		}
		text = StringValueUtil.trim(text);
		return Integer.parseInt(text);
	}

	/**
	 * 字符串转换为long,如果字符串为空则返回默认值
	 *
	 * @param text
	 * @param defaultValue
	 * @return long
	 */
	public static long parseLong(String text, long defaultValue) {
		if (StringUtil.isBlank(text)) {
			return defaultValue;
		}
		text = StringValueUtil.trim(text);
		return Long.parseLong(text);
	}

	/**
	 * 字符串转换为float,如果字符串为空则返回默认值
	 *
	 * @param text
	 * @param defaultValue
	 * @return float
	 */
	public static float parseFloat(String text, float defaultValue) {
		if (StringUtil.isBlank(text)) {
			return defaultValue;
		}
		text = StringValueUtil.trim(text);
		return Float.parseFloat(text);
	}

	/**
	 * 字符串转换为double,如果字符串为空则返回默认值
	 *
	 * @param text
	 * @param defaultValue
	 * @return double
	 */
	public static double parseDouble(String text, double defaultValue) {
		if (StringUtil.isBlank(text)) {
			return defaultValue;
		}
		text = StringValueUtil.trim(text);
		return Double.parseDouble(text);
	}

	/**
	 * 
	 * 字符串转换为short,如果字符串为空则返回默认值
	 *
	 * @param text
	 * @param defaultValue
	 * @return short
	 */
	public static short parseShort(String text, short defaultValue) {
		if (StringUtil.isBlank(text)) {
			return defaultValue;
		}
		text = StringValueUtil.trim(text);
		return Short.parseShort(text);
	}

	/**
	 * 
	 * 字符串转换为byte,如果字符串为空则返回默认值
	 *
	 * @param text
	 * @param defaultValue
	 * @return byte
	 */
	public static byte parseByte(String text, byte defaultValue) {
		if (StringUtil.isBlank(text)) {
			return defaultValue;
		}
		text = StringValueUtil.trim(text);
		return Byte.parseByte(text);
	}

	/**
	 * 
	 * 字符串转换为boolean,如果字符串为空则返回默认值
	 *
	 * @param text
	 * @param defaultValue
	 * @return boolean
	 */
	public static boolean parseBoolean(String text, boolean defaultValue) {
		if (StringUtil.isBlank(text)) {
			return defaultValue;
		}
		text = StringValueUtil.trim(text);
		return Boolean.parseBoolean(text);
	}
}
