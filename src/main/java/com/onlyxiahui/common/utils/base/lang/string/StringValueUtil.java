package com.onlyxiahui.common.utils.base.lang.string;

/**
 * 
 * Description <br>
 * Date 2021-05-12 12:23:05<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class StringValueUtil {

	/**
	 * A String for a space character.
	 *
	 */
	public static final String SPACE = " ";

	/**
	 * The empty String {@code ""}.
	 * 
	 */
	public static final String EMPTY = "";

	/**
	 * A String for linefeed LF ("\n").
	 */
	public static final String LF = "\n";

	/**
	 * A String for carriage return CR ("\r").
	 *
	 */
	public static final String CR = "\r";

	/**
	 * Represents a failed index search.
	 * 
	 * @since 2.1
	 */
	public static final int INDEX_NOT_FOUND = -1;

	public static String valueOrEmpty(String value) {
		return (value == null) ? EMPTY : value;
	}

	/**
	 * 
	 * 获取字符串前面的数字 <br>
	 * Date 2020-12-24 16:21:29<br>
	 * 
	 * @param text
	 * @return
	 * @since 1.0.0
	 */
	public static String getStartStringNumber(String text) {
		String no = null;
		if (StringJudgeUtil.isNotBlank(text)) {
			for (int i = 0; i < text.length(); i++) {
				char temp = text.charAt(i);
				if (temp >= '9' || temp <= '0') {
					no = text.substring(0, i);
					break;
				}
			}
		}
		return no;
	}

	public static String getEndStringNumber(String text) {
		String no = null;
		if (StringJudgeUtil.isNotBlank(text)) {
			int length = text.length();
			int end = length - 1;
			for (int i = end; i >= 0; i--) {
				char temp = text.charAt(i);
				if (temp > '9' || temp < '0') {
					no = text.substring(i + 1, length);
					break;
				}
			}
		}
		return no;
	}

	public static String trim(final String text) {
		return text == null ? null : text.trim();
	}
}
