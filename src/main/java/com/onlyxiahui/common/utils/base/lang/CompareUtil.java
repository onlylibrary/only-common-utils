package com.onlyxiahui.common.utils.base.lang;

/**
 * <br>
 *
 * @date 2022-05-24 10:26:08<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class CompareUtil {

	public static <T extends Comparable<T>> boolean isEqual(T b1, T b2) {
		boolean is = false;
		if (null != b1 && null != b2) {
			is = b1.compareTo(b2) == 0;
		}
		return is;
	}

	/**
	 * 
	 * 大于<br>
	 * Date 2020-12-31 11:13:26<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static <T extends Comparable<T>> boolean isGreater(T b1, T b2) {
		boolean is = false;
		if (null != b1 && null != b2) {
			is = b1.compareTo(b2) > 0;
		}
		return is;
	}

	/**
	 * 
	 * 大于等于<br>
	 * Date 2020-12-31 11:14:02<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static <T extends Comparable<T>> boolean isGreaterEqual(T b1, T b2) {
		return isGreater(b1, b2) || isEqual(b1, b2);
	}

	/**
	 * 
	 * 小于 <br>
	 * Date 2020-12-31 11:15:01<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static <T extends Comparable<T>> boolean isLess(T b1, T b2) {
		boolean is = false;
		if (null != b1 && null != b2) {
			is = b1.compareTo(b2) < 0;
		}
		return is;
	}

	/**
	 * 
	 * 小于等于<br>
	 * Date 2020-12-31 11:14:52<br>
	 * 
	 * @param b1
	 * @param b2
	 * @return
	 * @since 1.0.0
	 */
	public static <T extends Comparable<T>> boolean isLessEqual(T b1, T b2) {
		return isLess(b1, b2) || isEqual(b1, b2);
	}
}
