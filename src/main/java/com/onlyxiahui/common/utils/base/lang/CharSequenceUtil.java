package com.onlyxiahui.common.utils.base.lang;

/**
 * <br>
 *
 * @date 2022-05-24 12:02:25<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class CharSequenceUtil {

	private static final int NOT_FOUND = -1;

	public static int indexOf(final CharSequence cs, final int searchChar, int start) {
		if (cs instanceof String) {
			return ((String) cs).indexOf(searchChar, start);
		}
		final int sz = cs.length();
		if (start < 0) {
			start = 0;
		}
		if (searchChar < Character.MIN_SUPPLEMENTARY_CODE_POINT) {
			for (int i = start; i < sz; i++) {
				if (cs.charAt(i) == searchChar) {
					return i;
				}
			}
			return NOT_FOUND;
		}
		// supplementary characters (LANG1300)
		if (searchChar <= Character.MAX_CODE_POINT) {
			final char[] chars = Character.toChars(searchChar);
			for (int i = start; i < sz - 1; i++) {
				final char high = cs.charAt(i);
				final char low = cs.charAt(i + 1);
				if (high == chars[0] && low == chars[1]) {
					return i;
				}
			}
		}
		return NOT_FOUND;
	}
}
