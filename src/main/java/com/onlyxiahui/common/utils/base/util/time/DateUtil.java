package com.onlyxiahui.common.utils.base.util.time;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.onlyxiahui.common.utils.base.lang.string.StringUtil;

/**
 * 
 * Description 日期工具类<br>
 * Date 2019-01-11 14:31:40<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class DateUtil {

	public static final String YYYYMMDD = "yyyyMMdd";
	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";

	public static final long SECONDS_IN_DAY = 1000L * 3600L * 24L;

	public static final String NUMBER_FORMAT_XX = "00";

	/**
	 * 
	 * Description 获取当前日期+时间+毫秒<br>
	 * Format yyyy-MM-dd HH:mm:ss.SSS<br>
	 * Sample 2020-10-10 10:10:10.999<br>
	 * Date 2019-03-30 17:47:57<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDateTimeMillisecond() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return dateFormat.format(new Date()).toString();
	}

	/**
	 * 
	 * Description 获取当前日期+时间<br>
	 * Format yyyy-MM-dd HH:mm:ss<br>
	 * Sample 2020-10-10 10:10:10<br>
	 * Date 2019-03-30 17:48:17<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前日期（年-月-日）<br>
	 * Format yyyy-MM-dd<br>
	 * Sample 2020-10-10<br>
	 * Date 2019-03-30 17:48:50<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前时间（时:分:秒）<br>
	 * Format HH:mm:ss<br>
	 * Sample 17:49:38<br>
	 * Date 2019-03-30 17:49:38<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前年份<br>
	 * Format yyyy<br>
	 * Sample 2020<br>
	 * 
	 * Date 2020-11-10 10:02:25<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public static String getCurrentYear() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前月<br>
	 * Format MM<br>
	 * Sample 08<br>
	 * 
	 * Date 2020-11-10 10:02:36<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public static String getCurrentMonth() {
		SimpleDateFormat dFormat = new SimpleDateFormat("MM");
		return dFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前天<br>
	 * Format dd<br>
	 * Sample 28<br>
	 * <br>
	 * Date 2020-11-10 10:02:43<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public static String getCurrentDay() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前日期+小时+分钟<br>
	 * Format yyyy-MM-dd HH:mm<br>
	 * Sample 2020-11-10 10:02<br>
	 * <br>
	 * Date 2020-11-10 10:02:50<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDateHourMinute() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前日期+小时<br>
	 * Format yyyy-MM-dd HH<br>
	 * Sample 2020-11-10 10<br>
	 * <br>
	 * Date 2020-11-10 10:02:50<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDateHour() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获得当前年月（年-月）<br>
	 * Format yyyy-MM<br>
	 * Sample 2020-10<br>
	 * 
	 * Date 2020-11-10 10:02:25<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public static String getCurrentYearMonth() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
		return dateFormat.format(new Date());
	}

	/**
	 * 
	 * Description 获取当前自定义格式时间<br>
	 * Date 2019-03-30 17:49:54<br>
	 * 
	 * @param format :自定义时间格式
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrent(String format) {
		Calendar day = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String date = sdf.format(day.getTime());
		return date;
	}

	/**
	 * Description 获取当前日期时间精确到毫秒<br>
	 * Format 20130401114422555
	 *
	 * @return String
	 */
	public static String getCurrentDateTimeMillis() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYYMMDDHHMMSSSSS);
		return dateFormat.format(new Date()).toString();
	}

	/**
	 * Description 将毫秒转为（HH:mm:ss）多少小时：多少分钟：多少秒
	 *
	 * @param time
	 * @return String
	 */
	public static String millisecondToTime(long millisecond) {
		StringBuilder sb = new StringBuilder();
		millisecond = millisecond / 1000L;

		long hour = millisecond / 3600L;
		long minute = (millisecond - hour * 3600L) / 60L;
		long second = millisecond - hour * 3600L - minute * 60L;

		sb.append((hour > 9 ? hour : ("0" + hour)));
		sb.append(":");
		sb.append((minute > 9 ? minute : ("0" + minute)));
		sb.append(":");
		sb.append((second > 9 ? second : ("0" + second)));
		return sb.toString();
	}

	/**
	 * Description 时间转字符日期时间精确到秒<br>
	 * Format yyyy-MM-dd HH:mm:ss<br>
	 * Sample 2013-04-01 11:44:22
	 *
	 * @return String
	 */
	public static String dateToDateTime(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_DATE_TIME);
		return dateFormat.format(date);
	}

	/**
	 * Description 时间转字符日期<br>
	 * Format yyyy-MM-dd<br>
	 * Sample 2013-04-01
	 *
	 * @return String
	 */
	public static String dateToDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_DATE);
		return dateFormat.format(date);
	}

	/**
	 * Description 时间转字符时间<br>
	 * Format HH:mm:ss<br>
	 * Sample 11:44:22
	 *
	 * @return String
	 */
	public static String dateToTime(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_TIME);
		return dateFormat.format(date);
	}

	/**
	 * Description 时间转字符年<br>
	 * Format yyyy<br>
	 * Sample 2013
	 * 
	 * @return String
	 */
	public static String dateToYear(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_YEAR);
		return dateFormat.format(date);
	}

	/**
	 * Description 时间转字符月<br>
	 * Format MM<br>
	 * Sample 04
	 *
	 * @return String
	 */
	public static String dateToMonth(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_MONTH);
		return dateFormat.format(date);
	}

	/**
	 * Description 时间转字符天<br>
	 * Format dd<br>
	 * Sample 01
	 *
	 * @return String
	 */
	public static String dateToDay(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_DAY);
		return dateFormat.format(date);
	}

	/**
	 * Description 格式化日期时间<br>
	 *
	 * @param date
	 * @param format
	 * @return String
	 */
	public static String format(Date date, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}

	/**
	 * Description 日期+时间（yyyy-MM-dd HH:mm:ss）转Date<br>
	 *
	 * @param dateTime
	 * @return Date
	 */
	public static Date stringDateTimeToDate(String dateTime) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = dateFormat.parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * Description 日期（yyyy-MM-dd）转Date<br>
	 *
	 * @param time
	 * @return Date
	 */
	public static Date stringDateToDate(String time) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = dateFormat.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 
	 * Description 指定格式转Date<br>
	 * Date 2020-11-10 10:40:51<br>
	 * 
	 * @param dateTime
	 * @param format
	 * @return Date
	 * @since 1.0.0
	 */
	public static Date stringDateToDate(String dateTime, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = dateFormat.parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * Description String(格式 2011-06-30 12:20:22)转换成时间戳
	 *
	 * @param dateTime
	 * @return long
	 */
	public static long stringDateTimeToMillisecondTimestamp(String dateTime) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long date = 0L;
		try {
			date = dateFormat.parse(dateTime).getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * Description String(格式 2011-06-30)转换成时间戳
	 *
	 * @param dateTime
	 * @return long
	 */
	public static long stringDateToMillisecondTimestamp(String dateTime) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		long date = 0;
		try {
			date = df.parse(dateTime).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * Description 毫秒时间戳转换成String
	 *
	 * @param time
	 * @return String
	 */
	public static String millisecondTimestampToString(long time, String format) {
		String date = new SimpleDateFormat(format).format(time);
		return date;
	}

	/**
	 * Description 时间戳转换成String<br>
	 * 格式 2011-06-30 12:20:22
	 *
	 * @param time
	 * @return String
	 */
	public static String millisecondTimestampToDateTimeString(long time) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(time);
	}

	/**
	 * Description 时间戳转换成String 格式 2011-06-30
	 *
	 * @param time
	 * @return String
	 */
	public static String millisecondTimestampToDateString(long time) {
		return new SimpleDateFormat("yyyy-MM-dd").format(time);

	}

	/**
	 * Description 取当月的第一天日期<br>
	 * Format yyyy-MM-dd<br>
	 * Sample 2020-10-01<br>
	 * 
	 * @return String
	 */
	public static String getCurrentMonthFirstDayDate() {
		return format(new Date(), "yyyy-MM-01");
	}

	/**
	 * Description 取当月的最后一天日期<br>
	 * Format yyyy-MM-dd<br>
	 * Sample 2020-10-31<br>
	 * 
	 * @return String
	 */
	public static String getCurrentMonthLastDayDate() {
		Calendar cal = Calendar.getInstance();
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return format(new Date(), "yyyy-MM-" + maxDay);
	}

	/**
	 * Description 获取某年某月最后一天<br>
	 * Format d<br>
	 * Sample 31<br>
	 *
	 * @param year
	 * @param month
	 * @return int
	 */
	public static int getLastDayOfYearMonth(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, 1);
		return calendar.getActualMaximum(Calendar.DATE);
	}

	/**
	 * Description 获取某年某月最后一天<br>
	 * Format d<br>
	 * Sample 31<br>
	 *
	 * @param year
	 * @param month
	 * @return int
	 */
	public static int getLastDayOfYearMonth(String year, String month) {
		return getLastDayOfYearMonth(Integer.parseInt(year), Integer.parseInt(month));
	}

	/**
	 * Description 获取某年某月最后一天<br>
	 * Format d<br>
	 * Sample 31<br>
	 *
	 * @param year
	 * @param month
	 * @return int
	 */
	public static int getLastDay(int year, int month) {
		return getLastDayOfYearMonth(year, month);
	}

	/**
	 * Description 获取某年某月最后一天<br>
	 * Format d<br>
	 * Sample 31<br>
	 *
	 * @param year
	 * @param month
	 * @return String
	 */
	public static String getLastDay(String year, String month) {
		NumberFormat numberFormat = new DecimalFormat(NUMBER_FORMAT_XX);
		int last = getLastDayOfYearMonth(Integer.parseInt(year), Integer.parseInt(month));
		return numberFormat.format(last);
	}

	/**
	 * @param monthOrDay
	 * @return String
	 */
	public static String monthOrDayFormat(int monthOrDay) {
		NumberFormat numberFormat = new DecimalFormat(NUMBER_FORMAT_XX);
		return numberFormat.format(monthOrDay);
	}

	/**
	 * Description 将字符串转换成Date 格式可以是以下几种：<br>
	 * yyyy-MM-dd HH:mm:ss<br>
	 * yyyy-MM-dd<br>
	 * yyyyMMdd<br>
	 * HH:mm:ss<br>
	 *
	 * @param dateText ：字符串
	 * @return date
	 */
	public static Date parse(String dateText) {
		Date date = null;
		if (StringUtil.isNotBlank(dateText)) {
			dateText = LocalDateUtil.allToDateTimeMillisecond(dateText);
			SimpleDateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_DATE_TIME);
			try {
				date = dateFormat.parse(dateText);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	/**
	 * Description 将字符串转换成Timestamp
	 *
	 * @param dateText ：格式化的字符串
	 * @return Timestamp
	 */
	public static Timestamp parseTimestamp(String dateText) {
		Date date = parse(dateText);
		if (date == null) {
			return null;
		} else {
			return new Timestamp(date.getTime());
		}
	}

	/**
	 * Description 时间转字符日期时间精确到秒<br>
	 * Format yyyy-MM-dd HH:mm:ss<br>
	 * Sample 2013-04-01 11:44:22
	 *
	 * @param date ：需要格式化的日期
	 * @return String ：字符串，格式yyyy-MM-dd HH:mm:ss
	 */
	public static String formatDateTime(Date date) {
		return format(date, LocalDateUtil.FORMAT_DATE_TIME);
	}

	/**
	 * Description 时间转字符日期<br>
	 * Format yyyy-MM-dd<br>
	 * Sample 2013-04-01
	 *
	 * @param date ：需要格式化的日期
	 * @return String ：字符串，格式yyyy-MM-dd
	 */
	public static String formatDate(Date date) {
		return format(date, LocalDateUtil.FORMAT_DATE);
	}

	/**
	 * 
	 * Description 取得指定日期N天后的日期<br>
	 * Date 2020-11-10 12:26:57<br>
	 * 
	 * @param dateText
	 * @param days     ：正数加，负数减
	 * @return
	 * @since 1.0.0
	 */
	public static Date addDays(String dateText, int days) {
		Date date = DateUtil.parse(dateText);
		return null != date ? addDays(date, days) : null;
	}

	/**
	 * Description 取得指定日期N天后的日期
	 *
	 * @param date ：指定日期
	 * @param days ：增加的天数，负数表示减
	 * @return date
	 */
	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, days);
		return cal.getTime();
	}

	/**
	 * Description 获取指定N天数后日期<br>
	 * Format yyyy-MM-dd<br>
	 * Sample 2013-04-01
	 *
	 * @return String
	 */
	public static String getDateByAddDays(int days) {
		return getDateByAddDays(days, "yyyy-MM-dd");
	}

	/**
	 * Description 获取指定N天数后日期<br>
	 *
	 * @return String
	 */
	public static String getDateByAddDays(int days, String format) {
		Date nowDate = new Date();
		// long nowTime = (nowDate.getTime()) + (long) (1000L * 60L * 60L * 24L * days);
		// nowDate.setTime(nowTime);
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(addDays(nowDate, days));
	}

	/**
	 * Description 获取当前星期几<br>
	 * :day of the week(星期几) 和 weekday(工作日（周一到周五）)<br>
	 * 
	 * @return int
	 */
	public static int getDayOfTheWeekOfCurrent() {
		Calendar calendar = Calendar.getInstance();
		// calendar.setFirstDayOfWeek(Calendar.MONDAY);// 以周一为每周第一天
		calendar.setTime(new Date());
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * Description 获取当前日期是当年的第几周
	 *
	 * @return int
	 * @throws ParseException
	 */
	public static int getCurrentWeek() {
		Calendar calendar = Calendar.getInstance();
		// calendar.setFirstDayOfWeek(Calendar.MONDAY);// 以周一为每周第一天
		calendar.setTime(new Date());
		return calendar.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 获取指定日期是当年的第几周
	 *
	 * @param startDate
	 * @return int
	 * @throws ParseException
	 */
	public static int getWeekByDate(String dateText) {
		Date date = DateUtil.parse(dateText);
		return null == date ? 0 : getWeekByDate(date);
	}

	/**
	 * 
	 * Description 获取指定日期是当年的第几周<br>
	 * Date 2020-11-10 11:09:52<br>
	 * 
	 * @param date
	 * @return
	 * @since 1.0.0
	 */
	public static int getWeekByDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		// calendar.setFirstDayOfWeek(Calendar.MONDAY);// 以周一为每周第一天
		calendar.setTime(date);
		return calendar.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 
	 * Description 获取两个日期之间的间隔天数（按24小时算）<br>
	 * Date 2019-04-01 09:56:30<br>
	 * 
	 * @param start
	 * @param end
	 * @return int
	 * @since 1.0.0
	 */
	public static int getBetweenDays(String start, String end) {
		Date startDate = DateUtil.parse(start);
		Date endDate = DateUtil.parse(end);
		return getBetweenDays(startDate, endDate);
	}

	/**
	 * 
	 * Description 获取2个时间之间的天数（按24小时算）<br>
	 * Date 2019-04-01 10:07:34<br>
	 * 
	 * @param startDate
	 * @param endDate
	 * @return int
	 * @since 1.0.0
	 */
	public static int getBetweenDays(Date startDate, Date endDate) {
		long days = (endDate.getTime() - startDate.getTime()) / (24L * 60L * 60L * 1000L);
		int betweenDays = Integer.parseInt(String.valueOf(days));
		return betweenDays;
	}

	/**
	 * 
	 * Description 获取时间范围内的所有日期（排除取值范围）<br>
	 * Date 2019-04-01 10:08:09<br>
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return List<Date>
	 * @since 1.0.0
	 */
	public static List<Date> getBetweenExcludeScopeDateList(Date beginDate, Date endDate) {
		List<Date> dateList = new ArrayList<Date>();

		Calendar beginCalendar = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		beginCalendar.setTime(beginDate);
		beginCalendar.set(Calendar.HOUR_OF_DAY, 0);
		beginCalendar.set(Calendar.MINUTE, 0);
		beginCalendar.set(Calendar.SECOND, 0);
		beginCalendar.set(Calendar.MILLISECOND, 0);
		beginCalendar.add(Calendar.DAY_OF_MONTH, 1);

		Calendar endCalendar = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		endCalendar.setTime(endDate);
		endCalendar.set(Calendar.HOUR_OF_DAY, 0);
		endCalendar.set(Calendar.MINUTE, 0);
		endCalendar.set(Calendar.SECOND, 0);
		endCalendar.set(Calendar.MILLISECOND, 0);

		Date end = endCalendar.getTime();

		// 测试此日期是否在指定日期之后
		while (end.after(beginCalendar.getTime())) {
			// 根据日历的规则，为给定的日历字段添加或减去指定的时间量
			Date date = beginCalendar.getTime();
			dateList.add(date);
			beginCalendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		return dateList;
	}

	/**
	 * 
	 * Description 获取时间范围内的所有日期（包含取值范围）<br>
	 * Date 2019-04-01 10:08:23<br>
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return List<Date>
	 * @since 1.0.0
	 */
	public static List<Date> getBetweenIncludeScopeDateList(Date beginDate, Date endDate) {
		List<Date> dateList = new ArrayList<Date>();

		Calendar beginCalendar = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		beginCalendar.setTime(beginDate);
		beginCalendar.set(Calendar.HOUR_OF_DAY, 0);
		beginCalendar.set(Calendar.MINUTE, 0);
		beginCalendar.set(Calendar.SECOND, 0);
		beginCalendar.set(Calendar.MILLISECOND, 0);

		Calendar endCalendar = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		endCalendar.setTime(endDate);
		endCalendar.set(Calendar.HOUR_OF_DAY, 0);
		endCalendar.set(Calendar.MINUTE, 0);
		endCalendar.set(Calendar.SECOND, 0);
		endCalendar.set(Calendar.MILLISECOND, 0);

		Date end = endCalendar.getTime();
		// 测试此日期是否在指定日期之后
		while (end.after(beginCalendar.getTime())) {
			// 根据日历的规则，为给定的日历字段添加或减去指定的时间量
			Date date = beginCalendar.getTime();
			dateList.add(date);
			beginCalendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		dateList.add(end);
		return dateList;
	}

	/**
	 * 
	 * Description 获取日期范围内在当年第几周到第几周（包含条件）<br>
	 * Date 2019-04-01 10:08:38<br>
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return List<Integer>
	 * @since 1.0.0
	 */
	public static List<Integer> getWeekList(Date beginDate, Date endDate) {

		Calendar beginCalendar = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		beginCalendar.setTime(beginDate);

		Calendar endCalendar = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		endCalendar.setTime(endDate);

		int beginWeek = beginCalendar.get(Calendar.WEEK_OF_YEAR);
		int endWeek = endCalendar.get(Calendar.WEEK_OF_YEAR);
		// endCalendar.get(Calendar.)
		// endCalendar.get(field);
		List<Integer> weekList = new ArrayList<Integer>();

		weekList.add(beginWeek);
		for (int i = beginWeek; i < endWeek; i++) {
			weekList.add(i);
		}
		if (endWeek > beginWeek) {
			weekList.add(endWeek);
		}
		return weekList;
	}

	/**
	 * 
	 * Description 获取当月第一天的日期（Date）<br>
	 * Date 2019-04-01 10:08:52<br>
	 * 
	 * @return Date
	 * @since 1.0.0
	 */
	public static Date getFirstDayDateOfCurrentMonth() {
		Calendar calendar = Calendar.getInstance();
		// 设置时间,当前时间不用设置
		// calendar.setTime(new Date());
		// 设置日期为本月最小日期
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
		return calendar.getTime();
	}

	/**
	 * 
	 * Description 获取当月最后一天<br>
	 * Date 2019-04-01 10:09:04<br>
	 * 
	 * @return Date
	 * @since 1.0.0
	 */
	public static Date getLastDayDateOfCurrentMonth() {
		Calendar calendar = Calendar.getInstance();
		// 设置时间,当前时间不用设置
		// calendar.setTime(new Date());
		// 设置日期为本月最大日期
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		return calendar.getTime();
	}

	/**
	 * 
	 * Description 判断字符串是否符合时间格式<br>
	 * Date 2019-04-01 10:09:14<br>
	 * 
	 * @param dateText ：字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isDate(String dateText) {
		if (StringUtil.isBlank(dateText)) {
			return false;
		} else if (parse(dateText) != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Description 获取当前时间戳
	 *
	 * @return Timestamp
	 */
	public static Timestamp getNowTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	/**
	 * Description 获取当前日期
	 *
	 * @return Date
	 */
	public static Date getNowDate() {
		return new Date();
	}

	/**
	 * 
	 * Description 获取当前小时<br>
	 * Date 2019-04-01 10:10:00<br>
	 * 
	 * @return int
	 * @since 1.0.0
	 */
	public static int getNowHour() {
		Calendar cl = Calendar.getInstance();
		return cl.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 
	 * Description 获取当前到隔天零点相差的毫秒数<br>
	 * Date 2019-04-01 10:10:12<br>
	 * 
	 * @return Long
	 * @since 1.0.0
	 */
	public static Long getNowDifferenceTomorrowZeroMillis() {
		Calendar cl = Calendar.getInstance();
		cl.add(Calendar.DAY_OF_MONTH, 1);
		cl.set(Calendar.HOUR_OF_DAY, 0);
		cl.set(Calendar.MINUTE, 0);
		cl.set(Calendar.SECOND, 0);
		return cl.getTimeInMillis() - System.currentTimeMillis();
	}

	/**
	 * 
	 * Description 时间1（dateText1）是否在时间2（dateText2）之前<br>
	 * Date 2020-11-10 12:40:32<br>
	 * 
	 * @param dateText1
	 * @param dateText2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean before(String dateText1, String dateText2) {
		Date date1 = DateUtil.parse(dateText1);
		Date date2 = DateUtil.parse(dateText2);
		return before(date1, date2);
	}

	/**
	 * 
	 * Description 时间1（date1）是否在时间2（date2）之前<br>
	 * Date 2020-11-10 12:41:20<br>
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean before(Date date1, Date date2) {
		boolean flag = date1.before(date2);
		return flag;
	}

	/**
	 * 
	 * Description 时间1（dateText1）是否在时间2（dateText2）之后<br>
	 * Date 2020-11-10 12:41:35<br>
	 * 
	 * @param dateText1
	 * @param dateText2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean after(String dateText1, String dateText2) {
		Date date1 = DateUtil.parse(dateText1);
		Date date2 = DateUtil.parse(dateText2);
		return after(date1, date2);
	}

	/**
	 * 
	 * Description 时间1（date1）是否在时间2（date2）之后<br>
	 * Date 2020-11-10 12:41:54<br>
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * @since 1.0.0
	 */
	public static boolean after(Date date1, Date date2) {
		boolean flag = date1.after(date2);
		return flag;
	}
}
