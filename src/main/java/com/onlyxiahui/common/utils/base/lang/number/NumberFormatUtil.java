package com.onlyxiahui.common.utils.base.lang.number;

import java.text.DecimalFormat;

/**
 * 
 * Description <br>
 * Date 2019-04-01 10:18:21<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class NumberFormatUtil {

	/**
	 * 保留2位小数，当没小数时取整数
	 *
	 * @param number
	 * @return String
	 */
	public static String format2Digit(Number number) {
		return new DecimalFormat("#.##").format(number);
	}

	public static String format(Number number, String format) {
		return new DecimalFormat(format).format(number);
	}
}
