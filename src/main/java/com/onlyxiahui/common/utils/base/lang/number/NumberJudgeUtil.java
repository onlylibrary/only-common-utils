package com.onlyxiahui.common.utils.base.lang.number;

import java.math.BigInteger;
import java.util.regex.Pattern;

import com.onlyxiahui.common.utils.base.lang.string.StringJudgeUtil;
import com.onlyxiahui.common.utils.base.lang.string.StringUtil;

/**
 * 
 * Description <br>
 * Date 2019-04-01 10:18:21<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class NumberJudgeUtil {

	public static final BigInteger MAX_INTEGER_BIG = new BigInteger(Integer.toString(Integer.MAX_VALUE));
	public static final BigInteger MIN_INTEGER_BIG = new BigInteger(Integer.toString(Integer.MIN_VALUE));

	public static final BigInteger MAX_LONG_BIG = new BigInteger(Long.toString(Long.MAX_VALUE));
	public static final BigInteger MIN_LONG_BIG = new BigInteger(Long.toString(Long.MIN_VALUE));

	/**
	 * 
	 * 是否自然数字类型<br>
	 * isNumber("")=false<br>
	 * 
	 * isNumber("1")=true<br>
	 * isNumber("1l")=false<br>
	 * isNumber("1L")=false<br>
	 * isNumber("1f")=false<br>
	 * isNumber("1F")=false<br>
	 * isNumber("1d")=false<br>
	 * isNumber("1D")=false<br>
	 * isNumber("1E")=false<br>
	 * isNumber("1a")=false<br>
	 * 
	 * isNumber("-1")=true<br>
	 * isNumber("-1l")=false<br>
	 * isNumber("-1L")=false<br>
	 * isNumber("-1f")=false<br>
	 * isNumber("-1F")=false<br>
	 * isNumber("-1d")=false<br>
	 * isNumber("-1D")=false<br>
	 * isNumber("-1E")=false<br>
	 * isNumber("-1a")=false<br>
	 * 
	 * isNumber("+1")=true<br>
	 * isNumber("+1l")=false<br>
	 * isNumber("+1L")=false<br>
	 * isNumber("+1f")=false<br>
	 * isNumber("+1F")=false<br>
	 * isNumber("+1d")=false<br>
	 * isNumber("+1D")=false<br>
	 * isNumber("+1E")=false<br>
	 * isNumber("+1a")=false<br>
	 * 
	 * isNumber("000")=true<br>
	 * isNumber("001")=true<br>
	 * 
	 * isNumber("0")=true<br>
	 * isNumber("0.0")=true<br>
	 * isNumber("0.0l")=false<br>
	 * isNumber("0.0L")=false<br>
	 * isNumber("0.0f")=false<br>
	 * isNumber("0.0F")=false<br>
	 * isNumber("0.0d")=false<br>
	 * isNumber("0.0D")=false<br>
	 * isNumber("0.0E")=false<br>
	 * isNumber("0.0a")=false<br>
	 * 
	 * isNumber("-0")=true<br>
	 * isNumber("-0.0")=true<br>
	 * isNumber("-0.0l")=false<br>
	 * isNumber("-0.0L")=false<br>
	 * isNumber("-0.0f")=false<br>
	 * isNumber("-0.0F")=false<br>
	 * isNumber("-0.0d")=false<br>
	 * isNumber("-0.0D")=false<br>
	 * isNumber("-0.0E")=false<br>
	 * isNumber("-0.0a")=false<br>
	 * 
	 * isNumber("+0")=true<br>
	 * isNumber("+0.0")=true<br>
	 * isNumber("+0.0l")=false<br>
	 * isNumber("+0.0L")=false<br>
	 * isNumber("+0.0f")=false<br>
	 * isNumber("+0.0F")=false<br>
	 * isNumber("+0.0d")=false<br>
	 * isNumber("+0.0D")=false<br>
	 * isNumber("+0.0E")=false<br>
	 * isNumber("+0.0a")=false<br>
	 * 
	 * isNumber("0.12921")=true<br>
	 * isNumber("0.12921l")=false<br>
	 * isNumber("0.12921L")=false<br>
	 * isNumber("0.12921f")=false<br>
	 * isNumber("0.12921F")=false<br>
	 * isNumber("0.12921d")=false<br>
	 * isNumber("0.12921D")=false<br>
	 * isNumber("0.12921E")=false<br>
	 * isNumber("0.12921a")=false<br>
	 * 
	 * isNumber("-0.12921")=true<br>
	 * isNumber("-0.12921l")=false<br>
	 * isNumber("-0.12921L")=false<br>
	 * isNumber("-0.12921f")=false<br>
	 * isNumber("-0.12921F")=false<br>
	 * isNumber("-0.12921d")=false<br>
	 * isNumber("-0.12921D")=false<br>
	 * isNumber("-0.12921E")=false<br>
	 * isNumber("-0.12921a")=false<br>
	 * 
	 * isNumber("+0.12921")=true<br>
	 * isNumber("+0.12921l")=false<br>
	 * isNumber("+0.12921L")=false<br>
	 * isNumber("+0.12921f")=false<br>
	 * isNumber("+0.12921F")=false<br>
	 * isNumber("+0.12921d")=false<br>
	 * isNumber("+0.12921D")=false<br>
	 * isNumber("+0.12921E")=false<br>
	 * isNumber("+0.12921a")=false<br>
	 * 
	 * isNumber("F")=false<br>
	 * 
	 * isNumber("1,100,000")=false<br>
	 * isNumber("1,100,000l")=false<br>
	 * isNumber("1,100,000L")=false<br>
	 * isNumber("1,100,000f")=false<br>
	 * isNumber("1.7976931348623157e+308")=false<br>
	 * isNumber("999999999999999999999999999999999999999999999999999999999999999999999999")=true<br>
	 * isNumber("-2147483648")=true<br>
	 * isNumber("2147483647")=true<br>
	 * isNumber("-2147483659")=true<br>
	 * isNumber("2147483648")=true<br>
	 * isNumber("-2147483647")=true<br>
	 * isNumber("2147483646")=true<br>
	 * isNumber("-9223372036854775808")=true<br>
	 * isNumber("9223372036854775807")=true<br>
	 * isNumber("-9223372036854775809")=true<br>
	 * isNumber("9223372036854775808")=true<br>
	 * 
	 * @date 2022-06-02 11:18:56<br>
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isNumber(String value) {
		if (StringJudgeUtil.isBlank(value)) {
			return false;
		}
		// Pattern.compile("^([+-]?(\\d*))(.)?(\\d+)?$");
		String regex = "^(\\-|\\+)?\\d+(\\.\\d+)?$";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(value).matches();
	}

	/**
	 * 
	 * 是否自然整数类型<br>
	 * isIntegral("")=false<br>
	 * 
	 * isIntegral("1")=true<br>
	 * isIntegral("1l")=false<br>
	 * isIntegral("1L")=false<br>
	 * isIntegral("1f")=false<br>
	 * isIntegral("1F")=false<br>
	 * isIntegral("1d")=false<br>
	 * isIntegral("1D")=false<br>
	 * isIntegral("1E")=false<br>
	 * isIntegral("1a")=false<br>
	 * 
	 * isIntegral("-1")=true<br>
	 * isIntegral("-1l")=false<br>
	 * isIntegral("-1L")=false<br>
	 * isIntegral("-1f")=false<br>
	 * isIntegral("-1F")=false<br>
	 * isIntegral("-1d")=false<br>
	 * isIntegral("-1D")=false<br>
	 * isIntegral("-1E")=false<br>
	 * isIntegral("-1a")=false<br>
	 * 
	 * isIntegral("+1")=true<br>
	 * isIntegral("+1l")=false<br>
	 * isIntegral("+1L")=false<br>
	 * isIntegral("+1f")=false<br>
	 * isIntegral("+1F")=false<br>
	 * isIntegral("+1d")=false<br>
	 * isIntegral("+1D")=false<br>
	 * isIntegral("+1E")=false<br>
	 * isIntegral("+1a")=false<br>
	 * 
	 * isIntegral("000")=true<br>
	 * isIntegral("001")=true<br>
	 * 
	 * isIntegral("0")=true<br>
	 * isIntegral("0.0")=false<br>
	 * isIntegral("0.0l")=false<br>
	 * isIntegral("0.0L")=false<br>
	 * isIntegral("0.0f")=false<br>
	 * isIntegral("0.0F")=false<br>
	 * isIntegral("0.0d")=false<br>
	 * isIntegral("0.0D")=false<br>
	 * isIntegral("0.0E")=false<br>
	 * isIntegral("0.0a")=false<br>
	 * 
	 * isIntegral("-0")=true<br>
	 * isIntegral("-0.0")=false<br>
	 * isIntegral("-0.0l")=false<br>
	 * isIntegral("-0.0L")=false<br>
	 * isIntegral("-0.0f")=false<br>
	 * isIntegral("-0.0F")=false<br>
	 * isIntegral("-0.0d")=false<br>
	 * isIntegral("-0.0D")=false<br>
	 * isIntegral("-0.0E")=false<br>
	 * isIntegral("-0.0a")=false<br>
	 * 
	 * isIntegral("+0")=true<br>
	 * isIntegral("+0.0")=false<br>
	 * isIntegral("+0.0l")=false<br>
	 * isIntegral("+0.0L")=false<br>
	 * isIntegral("+0.0f")=false<br>
	 * isIntegral("+0.0F")=false<br>
	 * isIntegral("+0.0d")=false<br>
	 * isIntegral("+0.0D")=false<br>
	 * isIntegral("+0.0E")=false<br>
	 * isIntegral("+0.0a")=false<br>
	 * 
	 * isIntegral("0.12921")=false<br>
	 * isIntegral("0.12921l")=false<br>
	 * isIntegral("0.12921L")=false<br>
	 * isIntegral("0.12921f")=false<br>
	 * isIntegral("0.12921F")=false<br>
	 * isIntegral("0.12921d")=false<br>
	 * isIntegral("0.12921D")=false<br>
	 * isIntegral("0.12921E")=false<br>
	 * isIntegral("0.12921a")=false<br>
	 * 
	 * isIntegral("-0.12921")=false<br>
	 * isIntegral("-0.12921l")=false<br>
	 * isIntegral("-0.12921L")=false<br>
	 * isIntegral("-0.12921f")=false<br>
	 * isIntegral("-0.12921F")=false<br>
	 * isIntegral("-0.12921d")=false<br>
	 * isIntegral("-0.12921D")=false<br>
	 * isIntegral("-0.12921E")=false<br>
	 * isIntegral("-0.12921a")=false<br>
	 * 
	 * isIntegral("+0.12921")=false<br>
	 * isIntegral("+0.12921l")=false<br>
	 * isIntegral("+0.12921L")=false<br>
	 * isIntegral("+0.12921f")=false<br>
	 * isIntegral("+0.12921F")=false<br>
	 * isIntegral("+0.12921d")=false<br>
	 * isIntegral("+0.12921D")=false<br>
	 * isIntegral("+0.12921E")=false<br>
	 * isIntegral("+0.12921a")=false<br>
	 * 
	 * isIntegral("F")=false<br>
	 * 
	 * isIntegral("1,100,000")=false<br>
	 * isIntegral("1,100,000l")=false<br>
	 * isIntegral("1,100,000L")=false<br>
	 * isIntegral("1,100,000f")=false<br>
	 * isIntegral("1.7976931348623157e+308")=false<br>
	 * isIntegral("999999999999999999999999999999999999999999999999999999999999999999999999")=true<br>
	 * isIntegral("-2147483648")=true<br>
	 * isIntegral("2147483647")=true<br>
	 * isIntegral("-2147483659")=true<br>
	 * isIntegral("2147483648")=true<br>
	 * isIntegral("-2147483647")=true<br>
	 * isIntegral("2147483646")=true<br>
	 * isIntegral("-9223372036854775808")=true<br>
	 * isIntegral("9223372036854775807")=true<br>
	 * isIntegral("-9223372036854775809")=true<br>
	 * isIntegral("9223372036854775808")=true<br>
	 * 
	 * @date 2022-06-02 11:12:23<br>
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isIntegral(String value) {
		if (StringUtil.isBlank(value)) {
			return false;
		}
		final char[] chars = value.toCharArray();
		int length = chars.length;
		final int start = chars[0] == '-' || chars[0] == '+' ? 1 : 0;
		for (int index = start; index < length; index++) {
			if (!Character.isDigit(chars[index])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 是否代码中可表示数字<br>
	 * isJavaNumber("")=false<br>
	 * 
	 * isJavaNumber("1")=true<br>
	 * isJavaNumber("1l")=true<br>
	 * isJavaNumber("1L")=true<br>
	 * isJavaNumber("1f")=true<br>
	 * isJavaNumber("1F")=true<br>
	 * isJavaNumber("1d")=true<br>
	 * isJavaNumber("1D")=true<br>
	 * isJavaNumber("1E")=false<br>
	 * isJavaNumber("1a")=false<br>
	 * 
	 * isJavaNumber("-1")=true<br>
	 * isJavaNumber("-1l")=true<br>
	 * isJavaNumber("-1L")=true<br>
	 * isJavaNumber("-1f")=true<br>
	 * isJavaNumber("-1F")=true<br>
	 * isJavaNumber("-1d")=true<br>
	 * isJavaNumber("-1D")=true<br>
	 * isJavaNumber("-1E")=false<br>
	 * isJavaNumber("-1a")=false<br>
	 * 
	 * isJavaNumber("+1")=true<br>
	 * isJavaNumber("+1l")=true<br>
	 * isJavaNumber("+1L")=true<br>
	 * isJavaNumber("+1f")=true<br>
	 * isJavaNumber("+1F")=true<br>
	 * isJavaNumber("+1d")=true<br>
	 * isJavaNumber("+1D")=true<br>
	 * isJavaNumber("+1E")=false<br>
	 * isJavaNumber("+1a")=false<br>
	 * 
	 * isJavaNumber("000")=true<br>
	 * isJavaNumber("001")=true<br>
	 * 
	 * isJavaNumber("0")=true<br>
	 * isJavaNumber("0.0")=true<br>
	 * isJavaNumber("0.0l")=false<br>
	 * isJavaNumber("0.0L")=false<br>
	 * isJavaNumber("0.0f")=true<br>
	 * isJavaNumber("0.0F")=true<br>
	 * isJavaNumber("0.0d")=true<br>
	 * isJavaNumber("0.0D")=true<br>
	 * isJavaNumber("0.0E")=false<br>
	 * isJavaNumber("0.0a")=false<br>
	 * 
	 * isJavaNumber("-0")=true<br>
	 * isJavaNumber("-0.0")=true<br>
	 * isJavaNumber("-0.0l")=false<br>
	 * isJavaNumber("-0.0L")=false<br>
	 * isJavaNumber("-0.0f")=true<br>
	 * isJavaNumber("-0.0F")=true<br>
	 * isJavaNumber("-0.0d")=true<br>
	 * isJavaNumber("-0.0D")=true<br>
	 * isJavaNumber("-0.0E")=false<br>
	 * isJavaNumber("-0.0a")=false<br>
	 * 
	 * isJavaNumber("+0")=true<br>
	 * isJavaNumber("+0.0")=true<br>
	 * isJavaNumber("+0.0l")=false<br>
	 * isJavaNumber("+0.0L")=false<br>
	 * isJavaNumber("+0.0f")=true<br>
	 * isJavaNumber("+0.0F")=true<br>
	 * isJavaNumber("+0.0d")=true<br>
	 * isJavaNumber("+0.0D")=true<br>
	 * isJavaNumber("+0.0E")=false<br>
	 * isJavaNumber("+0.0a")=false<br>
	 * 
	 * isJavaNumber("0.12921")=true<br>
	 * isJavaNumber("0.12921l")=false<br>
	 * isJavaNumber("0.12921L")=false<br>
	 * isJavaNumber("0.12921f")=true<br>
	 * isJavaNumber("0.12921F")=true<br>
	 * isJavaNumber("0.12921d")=true<br>
	 * isJavaNumber("0.12921D")=true<br>
	 * isJavaNumber("0.12921E")=false<br>
	 * isJavaNumber("0.12921a")=false<br>
	 * 
	 * isJavaNumber("-0.12921")=true<br>
	 * isJavaNumber("-0.12921l")=false<br>
	 * isJavaNumber("-0.12921L")=false<br>
	 * isJavaNumber("-0.12921f")=true<br>
	 * isJavaNumber("-0.12921F")=true<br>
	 * isJavaNumber("-0.12921d")=true<br>
	 * isJavaNumber("-0.12921D")=true<br>
	 * isJavaNumber("-0.12921E")=false<br>
	 * isJavaNumber("-0.12921a")=false<br>
	 * 
	 * isJavaNumber("+0.12921")=true<br>
	 * isJavaNumber("+0.12921l")=false<br>
	 * isJavaNumber("+0.12921L")=false<br>
	 * isJavaNumber("+0.12921f")=true<br>
	 * isJavaNumber("+0.12921F")=true<br>
	 * isJavaNumber("+0.12921d")=true<br>
	 * isJavaNumber("+0.12921D")=true<br>
	 * isJavaNumber("+0.12921E")=false<br>
	 * isJavaNumber("+0.12921a")=false<br>
	 * 
	 * isJavaNumber("F")=false<br>
	 * 
	 * isJavaNumber("1,100,000")=false<br>
	 * isJavaNumber("1,100,000l")=false<br>
	 * isJavaNumber("1,100,000L")=false<br>
	 * isJavaNumber("1,100,000f")=false<br>
	 * isJavaNumber("1.7976931348623157e+308")=true<br>
	 * isJavaNumber("999999999999999999999999999999999999999999999999999999999999999999999999")=true<br>
	 * 
	 * @param value
	 * @return boolean
	 */
	public static boolean isJavaNumber(String value) {
		if (StringUtil.isBlank(value)) {
			return false;
		}
		final char[] chars = value.toCharArray();
		int sz = chars.length;
		boolean hasExp = false;
		boolean hasDecPoint = false;
		boolean allowSigns = false;
		boolean foundDigit = false;
		// deal with any possible sign up front
		final int start = chars[0] == '-' || chars[0] == '+' ? 1 : 0;
		// leading 0, skip if is a decimal number
		if (sz > start + 1 && chars[start] == '0' && !StringJudgeUtil.contains(value, '.')) {
			// leading 0x/0X
			if (chars[start + 1] == 'x' || chars[start + 1] == 'X') {
				int i = start + 2;
				if (i == sz) {
					// str == "0x"
					return false;
				}
				// checking hex (it can't be anything else)
				for (; i < chars.length; i++) {
					boolean n = (chars[i] < '0' || chars[i] > '9') && (chars[i] < 'a' || chars[i] > 'f') && (chars[i] < 'A' || chars[i] > 'F');
					if (n) {
						return false;
					}
				}
				return true;
			} else if (Character.isDigit(chars[start + 1])) {
				// leading 0, but not hex, must be octal
				int i = start + 1;
				for (; i < chars.length; i++) {
					if (chars[i] < '0' || chars[i] > '7') {
						return false;
					}
				}
				return true;
			}
		}
		// don't want to loop to the last char, check it afterwords
		// for type qualifiers
		sz--;
		int i = start;
		// loop to the next to last char or to the last char if we need another digit to
		// make a valid number (e.g. chars[0..5] = "1234E")
		while (i < sz || i < sz + 1 && allowSigns && !foundDigit) {
			if (chars[i] >= '0' && chars[i] <= '9') {
				foundDigit = true;
				allowSigns = false;

			} else if (chars[i] == '.') {
				if (hasDecPoint || hasExp) {
					// two decimal points or dec in exponent
					return false;
				}
				hasDecPoint = true;
			} else if (chars[i] == 'e' || chars[i] == 'E') {
				// we've already taken care of hex.
				if (hasExp) {
					// two E's
					return false;
				}
				if (!foundDigit) {
					return false;
				}
				hasExp = true;
				allowSigns = true;
			} else if (chars[i] == '+' || chars[i] == '-') {
				if (!allowSigns) {
					return false;
				}
				allowSigns = false;
				foundDigit = false; // we need a digit after the E
			} else {
				return false;
			}
			i++;
		}
		if (i < chars.length) {
			if (chars[i] >= '0' && chars[i] <= '9') {
				// no type qualifier, OK
				return true;
			}
			if (chars[i] == 'e' || chars[i] == 'E') {
				// can't have an E at the last byte
				return false;
			}
			if (chars[i] == '.') {
				if (hasDecPoint || hasExp) {
					// two decimal points or dec in exponent
					return false;
				}
				// single trailing decimal point after non-exponent is ok
				return foundDigit;
			}
			boolean fd = (chars[i] == 'd' || chars[i] == 'D' || chars[i] == 'f' || chars[i] == 'F');
			if (!allowSigns && fd) {
				return foundDigit;
			}
			if (chars[i] == 'l' || chars[i] == 'L') {
				// not allowing L with an exponent or decimal point
				return foundDigit && !hasExp && !hasDecPoint;
			}
			// last character is illegal
			return false;
		}
		// allowSigns is true iff the val ends in 'E'
		// found digit it to make sure weird stuff like '.' and '1E-' doesn't pass
		return !allowSigns && foundDigit;
	}

	/**
	 * 
	 * 是否代码中可表示小数<br>
	 * isJavaDigit("")=false<br>
	 * 
	 * isJavaDigit("1")=false<br>
	 * isJavaDigit("1l")=false<br>
	 * isJavaDigit("1L")=false<br>
	 * isJavaDigit("1f")=false<br>
	 * isJavaDigit("1F")=false<br>
	 * isJavaDigit("1d")=false<br>
	 * isJavaDigit("1D")=false<br>
	 * isJavaDigit("1E")=false<br>
	 * isJavaDigit("1a")=false<br>
	 * 
	 * isJavaDigit("-1")=false<br>
	 * isJavaDigit("-1l")=false<br>
	 * isJavaDigit("-1L")=false<br>
	 * isJavaDigit("-1f")=false<br>
	 * isJavaDigit("-1F")=false<br>
	 * isJavaDigit("-1d")=false<br>
	 * isJavaDigit("-1D")=false<br>
	 * isJavaDigit("-1E")=false<br>
	 * isJavaDigit("-1a")=false<br>
	 * 
	 * isJavaDigit("+1")=false<br>
	 * isJavaDigit("+1l")=false<br>
	 * isJavaDigit("+1L")=false<br>
	 * isJavaDigit("+1f")=false<br>
	 * isJavaDigit("+1F")=false<br>
	 * isJavaDigit("+1d")=false<br>
	 * isJavaDigit("+1D")=false<br>
	 * isJavaDigit("+1E")=false<br>
	 * isJavaDigit("+1a")=false<br>
	 * 
	 * isJavaDigit("000")=false<br>
	 * isJavaDigit("001")=false<br>
	 * 
	 * isJavaDigit("0")=false<br>
	 * isJavaDigit("0.0")=true<br>
	 * isJavaDigit("0.0l")=false<br>
	 * isJavaDigit("0.0L")=false<br>
	 * isJavaDigit("0.0f")=true<br>
	 * isJavaDigit("0.0F")=true<br>
	 * isJavaDigit("0.0d")=true<br>
	 * isJavaDigit("0.0D")=true<br>
	 * isJavaDigit("0.0E")=false<br>
	 * isJavaDigit("0.0a")=false<br>
	 * 
	 * isJavaDigit("-0")=false<br>
	 * isJavaDigit("-0.0")=true<br>
	 * isJavaDigit("-0.0l")=false<br>
	 * isJavaDigit("-0.0L")=false<br>
	 * isJavaDigit("-0.0f")=true<br>
	 * isJavaDigit("-0.0F")=true<br>
	 * isJavaDigit("-0.0d")=true<br>
	 * isJavaDigit("-0.0D")=true<br>
	 * isJavaDigit("-0.0E")=false<br>
	 * isJavaDigit("-0.0a")=false<br>
	 * 
	 * isJavaDigit("+0")=false<br>
	 * isJavaDigit("+0.0")=true<br>
	 * isJavaDigit("+0.0l")=false<br>
	 * isJavaDigit("+0.0L")=false<br>
	 * isJavaDigit("+0.0f")=true<br>
	 * isJavaDigit("+0.0F")=true<br>
	 * isJavaDigit("+0.0d")=true<br>
	 * isJavaDigit("+0.0D")=true<br>
	 * isJavaDigit("+0.0E")=false<br>
	 * isJavaDigit("+0.0a")=false<br>
	 * 
	 * isJavaDigit("0.12921")=true<br>
	 * isJavaDigit("0.12921l")=false<br>
	 * isJavaDigit("0.12921L")=false<br>
	 * isJavaDigit("0.12921f")=true<br>
	 * isJavaDigit("0.12921F")=true<br>
	 * isJavaDigit("0.12921d")=true<br>
	 * isJavaDigit("0.12921D")=true<br>
	 * isJavaDigit("0.12921E")=false<br>
	 * isJavaDigit("0.12921a")=false<br>
	 * 
	 * isJavaDigit("-0.12921")=true<br>
	 * isJavaDigit("-0.12921l")=false<br>
	 * isJavaDigit("-0.12921L")=false<br>
	 * isJavaDigit("-0.12921f")=true<br>
	 * isJavaDigit("-0.12921F")=true<br>
	 * isJavaDigit("-0.12921d")=true<br>
	 * isJavaDigit("-0.12921D")=true<br>
	 * isJavaDigit("-0.12921E")=false<br>
	 * isJavaDigit("-0.12921a")=false<br>
	 * 
	 * isJavaDigit("+0.12921")=true<br>
	 * isJavaDigit("+0.12921l")=false<br>
	 * isJavaDigit("+0.12921L")=false<br>
	 * isJavaDigit("+0.12921f")=true<br>
	 * isJavaDigit("+0.12921F")=true<br>
	 * isJavaDigit("+0.12921d")=true<br>
	 * isJavaDigit("+0.12921D")=true<br>
	 * isJavaDigit("+0.12921E")=false<br>
	 * isJavaDigit("+0.12921a")=false<br>
	 * 
	 * isJavaDigit("F")=false<br>
	 * 
	 * isJavaDigit("1,100,000")=false<br>
	 * isJavaDigit("1,100,000l")=false<br>
	 * isJavaDigit("1,100,000L")=false<br>
	 * isJavaDigit("1,100,000f")=false<br>
	 * isJavaDigit("1.7976931348623157e+308")=false<br>
	 * isJavaDigit("999999999999999999999999999999999999999999999999999999999999999999999999")=false<br>
	 * isJavaDigit("-2147483648")=false<br>
	 * isJavaDigit("2147483647")=false<br>
	 * isJavaDigit("-2147483659")=false<br>
	 * isJavaDigit("2147483648")=false<br>
	 * isJavaDigit("-2147483647")=false<br>
	 * isJavaDigit("2147483646")=false<br>
	 * isJavaDigit("-9223372036854775808")=false<br>
	 * isJavaDigit("9223372036854775807")=false<br>
	 * isJavaDigit("-9223372036854775809")=false<br>
	 * isJavaDigit("9223372036854775808")=false<br>
	 * 
	 * @date 2022-06-01 11:00:51<br>
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isJavaDigit(String value) {
		if (StringUtil.isBlank(value)) {
			return false;
		}
		String regex = "^(\\-|\\+)?\\d+(\\.\\d+)(f|F|d|D)?$";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(value).matches();
	}

	/**
	 * 是否int类型（不含long，BigInteger）<br>
	 * isJustJavaInteger("")=false<br>
	 * 
	 * isJustJavaInteger("1")=true<br>
	 * isJustJavaInteger("1l")=false<br>
	 * isJustJavaInteger("1L")=false<br>
	 * isJustJavaInteger("1f")=false<br>
	 * isJustJavaInteger("1F")=false<br>
	 * isJustJavaInteger("1d")=false<br>
	 * isJustJavaInteger("1D")=false<br>
	 * isJustJavaInteger("1E")=false<br>
	 * isJustJavaInteger("1a")=false<br>
	 * 
	 * isJustJavaInteger("-1")=true<br>
	 * isJustJavaInteger("-1l")=false<br>
	 * isJustJavaInteger("-1L")=false<br>
	 * isJustJavaInteger("-1f")=false<br>
	 * isJustJavaInteger("-1F")=false<br>
	 * isJustJavaInteger("-1d")=false<br>
	 * isJustJavaInteger("-1D")=false<br>
	 * isJustJavaInteger("-1E")=false<br>
	 * isJustJavaInteger("-1a")=false<br>
	 * 
	 * isJustJavaInteger("+1")=true<br>
	 * isJustJavaInteger("+1l")=false<br>
	 * isJustJavaInteger("+1L")=false<br>
	 * isJustJavaInteger("+1f")=false<br>
	 * isJustJavaInteger("+1F")=false<br>
	 * isJustJavaInteger("+1d")=false<br>
	 * isJustJavaInteger("+1D")=false<br>
	 * isJustJavaInteger("+1E")=false<br>
	 * isJustJavaInteger("+1a")=false<br>
	 * 
	 * isJustJavaInteger("000")=true<br>
	 * isJustJavaInteger("001")=true<br>
	 * 
	 * isJustJavaInteger("0")=true<br>
	 * isJustJavaInteger("0.0")=false<br>
	 * isJustJavaInteger("0.0l")=false<br>
	 * isJustJavaInteger("0.0L")=false<br>
	 * isJustJavaInteger("0.0f")=false<br>
	 * isJustJavaInteger("0.0F")=false<br>
	 * isJustJavaInteger("0.0d")=false<br>
	 * isJustJavaInteger("0.0D")=false<br>
	 * isJustJavaInteger("0.0E")=false<br>
	 * isJustJavaInteger("0.0a")=false<br>
	 * 
	 * isJustJavaInteger("-0")=true<br>
	 * isJustJavaInteger("-0.0")=false<br>
	 * isJustJavaInteger("-0.0l")=false<br>
	 * isJustJavaInteger("-0.0L")=false<br>
	 * isJustJavaInteger("-0.0f")=false<br>
	 * isJustJavaInteger("-0.0F")=false<br>
	 * isJustJavaInteger("-0.0d")=false<br>
	 * isJustJavaInteger("-0.0D")=false<br>
	 * isJustJavaInteger("-0.0E")=false<br>
	 * isJustJavaInteger("-0.0a")=false<br>
	 * 
	 * isJustJavaInteger("+0")=true<br>
	 * isJustJavaInteger("+0.0")=false<br>
	 * isJustJavaInteger("+0.0l")=false<br>
	 * isJustJavaInteger("+0.0L")=false<br>
	 * isJustJavaInteger("+0.0f")=false<br>
	 * isJustJavaInteger("+0.0F")=false<br>
	 * isJustJavaInteger("+0.0d")=false<br>
	 * isJustJavaInteger("+0.0D")=false<br>
	 * isJustJavaInteger("+0.0E")=false<br>
	 * isJustJavaInteger("+0.0a")=false<br>
	 * 
	 * isJustJavaInteger("0.12921")=false<br>
	 * isJustJavaInteger("0.12921l")=false<br>
	 * isJustJavaInteger("0.12921L")=false<br>
	 * isJustJavaInteger("0.12921f")=false<br>
	 * isJustJavaInteger("0.12921F")=false<br>
	 * isJustJavaInteger("0.12921d")=false<br>
	 * isJustJavaInteger("0.12921D")=false<br>
	 * isJustJavaInteger("0.12921E")=false<br>
	 * isJustJavaInteger("0.12921a")=false<br>
	 * 
	 * isJustJavaInteger("-0.12921")=false<br>
	 * isJustJavaInteger("-0.12921l")=false<br>
	 * isJustJavaInteger("-0.12921L")=false<br>
	 * isJustJavaInteger("-0.12921f")=false<br>
	 * isJustJavaInteger("-0.12921F")=false<br>
	 * isJustJavaInteger("-0.12921d")=false<br>
	 * isJustJavaInteger("-0.12921D")=false<br>
	 * isJustJavaInteger("-0.12921E")=false<br>
	 * isJustJavaInteger("-0.12921a")=false<br>
	 * 
	 * isJustJavaInteger("+0.12921")=false<br>
	 * isJustJavaInteger("+0.12921l")=false<br>
	 * isJustJavaInteger("+0.12921L")=false<br>
	 * isJustJavaInteger("+0.12921f")=false<br>
	 * isJustJavaInteger("+0.12921F")=false<br>
	 * isJustJavaInteger("+0.12921d")=false<br>
	 * isJustJavaInteger("+0.12921D")=false<br>
	 * isJustJavaInteger("+0.12921E")=false<br>
	 * isJustJavaInteger("+0.12921a")=false<br>
	 * 
	 * isJustJavaInteger("F")=false<br>
	 * 
	 * isJustJavaInteger("1,100,000")=false<br>
	 * isJustJavaInteger("1,100,000l")=false<br>
	 * isJustJavaInteger("1,100,000L")=false<br>
	 * isJustJavaInteger("1,100,000f")=false<br>
	 * isJustJavaInteger("1.7976931348623157e+308")=false<br>
	 * isJustJavaInteger("999999999999999999999999999999999999999999999999999999999999999999999999")=false<br>
	 * isJustJavaInteger("-2147483648")=true<br>
	 * isJustJavaInteger("2147483647")=true<br>
	 * isJustJavaInteger("-2147483659")=false<br>
	 * isJustJavaInteger("2147483648")=false<br>
	 * isJustJavaInteger("-2147483647")=true<br>
	 * isJustJavaInteger("2147483646")=true<br>
	 * isJustJavaInteger("-9223372036854775808")=false<br>
	 * isJustJavaInteger("9223372036854775807")=false<br>
	 * isJustJavaInteger("-9223372036854775809")=false<br>
	 * isJustJavaInteger("9223372036854775808")=false<br>
	 * 
	 * @param value
	 * @return boolean
	 */
	public static boolean isJustJavaInteger(String value) {
		if (isIntegral(value)) {
			if (value.length() > 11) {
				return false;
			}
			BigInteger compare = new BigInteger(value);
			int compareToMax = compare.compareTo(MAX_INTEGER_BIG);
			int compareToMin = compare.compareTo(MIN_INTEGER_BIG);
			return compareToMax <= 0 && compareToMin >= 0;
		}
		return false;
	}

	/**
	 * 
	 * 是否long类型（大于int，小于BigInteger）<br>
	 * isJustJavaLong("")=false<br>
	 * 
	 * isJustJavaLong("1")=false<br>
	 * isJustJavaLong("1l")=false<br>
	 * isJustJavaLong("1L")=false<br>
	 * isJustJavaLong("1f")=false<br>
	 * isJustJavaLong("1F")=false<br>
	 * isJustJavaLong("1d")=false<br>
	 * isJustJavaLong("1D")=false<br>
	 * isJustJavaLong("1E")=false<br>
	 * isJustJavaLong("1a")=false<br>
	 * 
	 * isJustJavaLong("-1")=false<br>
	 * isJustJavaLong("-1l")=false<br>
	 * isJustJavaLong("-1L")=false<br>
	 * isJustJavaLong("-1f")=false<br>
	 * isJustJavaLong("-1F")=false<br>
	 * isJustJavaLong("-1d")=false<br>
	 * isJustJavaLong("-1D")=false<br>
	 * isJustJavaLong("-1E")=false<br>
	 * isJustJavaLong("-1a")=false<br>
	 * 
	 * isJustJavaLong("+1")=false<br>
	 * isJustJavaLong("+1l")=false<br>
	 * isJustJavaLong("+1L")=false<br>
	 * isJustJavaLong("+1f")=false<br>
	 * isJustJavaLong("+1F")=false<br>
	 * isJustJavaLong("+1d")=false<br>
	 * isJustJavaLong("+1D")=false<br>
	 * isJustJavaLong("+1E")=false<br>
	 * isJustJavaLong("+1a")=false<br>
	 * 
	 * isJustJavaLong("000")=false<br>
	 * isJustJavaLong("001")=false<br>
	 * 
	 * isJustJavaLong("0")=false<br>
	 * isJustJavaLong("0.0")=false<br>
	 * isJustJavaLong("0.0l")=false<br>
	 * isJustJavaLong("0.0L")=false<br>
	 * isJustJavaLong("0.0f")=false<br>
	 * isJustJavaLong("0.0F")=false<br>
	 * isJustJavaLong("0.0d")=false<br>
	 * isJustJavaLong("0.0D")=false<br>
	 * isJustJavaLong("0.0E")=false<br>
	 * isJustJavaLong("0.0a")=false<br>
	 * 
	 * isJustJavaLong("-0")=false<br>
	 * isJustJavaLong("-0.0")=false<br>
	 * isJustJavaLong("-0.0l")=false<br>
	 * isJustJavaLong("-0.0L")=false<br>
	 * isJustJavaLong("-0.0f")=false<br>
	 * isJustJavaLong("-0.0F")=false<br>
	 * isJustJavaLong("-0.0d")=false<br>
	 * isJustJavaLong("-0.0D")=false<br>
	 * isJustJavaLong("-0.0E")=false<br>
	 * isJustJavaLong("-0.0a")=false<br>
	 * 
	 * isJustJavaLong("+0")=false<br>
	 * isJustJavaLong("+0.0")=false<br>
	 * isJustJavaLong("+0.0l")=false<br>
	 * isJustJavaLong("+0.0L")=false<br>
	 * isJustJavaLong("+0.0f")=false<br>
	 * isJustJavaLong("+0.0F")=false<br>
	 * isJustJavaLong("+0.0d")=false<br>
	 * isJustJavaLong("+0.0D")=false<br>
	 * isJustJavaLong("+0.0E")=false<br>
	 * isJustJavaLong("+0.0a")=false<br>
	 * 
	 * isJustJavaLong("0.12921")=false<br>
	 * isJustJavaLong("0.12921l")=false<br>
	 * isJustJavaLong("0.12921L")=false<br>
	 * isJustJavaLong("0.12921f")=false<br>
	 * isJustJavaLong("0.12921F")=false<br>
	 * isJustJavaLong("0.12921d")=false<br>
	 * isJustJavaLong("0.12921D")=false<br>
	 * isJustJavaLong("0.12921E")=false<br>
	 * isJustJavaLong("0.12921a")=false<br>
	 * 
	 * isJustJavaLong("-0.12921")=false<br>
	 * isJustJavaLong("-0.12921l")=false<br>
	 * isJustJavaLong("-0.12921L")=false<br>
	 * isJustJavaLong("-0.12921f")=false<br>
	 * isJustJavaLong("-0.12921F")=false<br>
	 * isJustJavaLong("-0.12921d")=false<br>
	 * isJustJavaLong("-0.12921D")=false<br>
	 * isJustJavaLong("-0.12921E")=false<br>
	 * isJustJavaLong("-0.12921a")=false<br>
	 * 
	 * isJustJavaLong("+0.12921")=false<br>
	 * isJustJavaLong("+0.12921l")=false<br>
	 * isJustJavaLong("+0.12921L")=false<br>
	 * isJustJavaLong("+0.12921f")=false<br>
	 * isJustJavaLong("+0.12921F")=false<br>
	 * isJustJavaLong("+0.12921d")=false<br>
	 * isJustJavaLong("+0.12921D")=false<br>
	 * isJustJavaLong("+0.12921E")=false<br>
	 * isJustJavaLong("+0.12921a")=false<br>
	 * 
	 * isJustJavaLong("F")=false<br>
	 * 
	 * isJustJavaLong("1,100,000")=false<br>
	 * isJustJavaLong("1,100,000l")=false<br>
	 * isJustJavaLong("1,100,000L")=false<br>
	 * isJustJavaLong("1,100,000f")=false<br>
	 * isJustJavaLong("1.7976931348623157e+308")=false<br>
	 * isJustJavaLong("999999999999999999999999999999999999999999999999999999999999999999999999")=false<br>
	 * isJustJavaLong("-2147483648")=false<br>
	 * isJustJavaLong("2147483647")=false<br>
	 * isJustJavaLong("-2147483659")=true<br>
	 * isJustJavaLong("2147483648")=true<br>
	 * isJustJavaLong("-2147483647")=false<br>
	 * isJustJavaLong("2147483646")=false<br>
	 * isJustJavaLong("-9223372036854775808")=true<br>
	 * isJustJavaLong("9223372036854775807")=true<br>
	 * isJustJavaLong("-9223372036854775809")=false<br>
	 * isJustJavaLong("9223372036854775808")=false<br>
	 * 
	 * 
	 * @date 2022-06-01 12:13:36<br>
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isJustJavaLong(String value) {
		if (isIntegral(value) && !isJustJavaInteger(value)) {
			if (value.length() > 20) {
				return false;
			}
			BigInteger compare = new BigInteger(value);
			int compareToMax = compare.compareTo(MAX_LONG_BIG);
			int compareToMin = compare.compareTo(MIN_LONG_BIG);
			return compareToMax <= 0 && compareToMin >= 0;
		}
		return false;
	}

	/**
	 * 
	 * 是否BigInteger类型（大于long）<br>
	 * isJustJavaBigInteger("")=false<br>
	 * 
	 * isJustJavaBigInteger("1")=false<br>
	 * isJustJavaBigInteger("1l")=false<br>
	 * isJustJavaBigInteger("1L")=false<br>
	 * isJustJavaBigInteger("1f")=false<br>
	 * isJustJavaBigInteger("1F")=false<br>
	 * isJustJavaBigInteger("1d")=false<br>
	 * isJustJavaBigInteger("1D")=false<br>
	 * isJustJavaBigInteger("1E")=false<br>
	 * isJustJavaBigInteger("1a")=false<br>
	 * 
	 * isJustJavaBigInteger("-1")=false<br>
	 * isJustJavaBigInteger("-1l")=false<br>
	 * isJustJavaBigInteger("-1L")=false<br>
	 * isJustJavaBigInteger("-1f")=false<br>
	 * isJustJavaBigInteger("-1F")=false<br>
	 * isJustJavaBigInteger("-1d")=false<br>
	 * isJustJavaBigInteger("-1D")=false<br>
	 * isJustJavaBigInteger("-1E")=false<br>
	 * isJustJavaBigInteger("-1a")=false<br>
	 * 
	 * isJustJavaBigInteger("+1")=false<br>
	 * isJustJavaBigInteger("+1l")=false<br>
	 * isJustJavaBigInteger("+1L")=false<br>
	 * isJustJavaBigInteger("+1f")=false<br>
	 * isJustJavaBigInteger("+1F")=false<br>
	 * isJustJavaBigInteger("+1d")=false<br>
	 * isJustJavaBigInteger("+1D")=false<br>
	 * isJustJavaBigInteger("+1E")=false<br>
	 * isJustJavaBigInteger("+1a")=false<br>
	 * 
	 * isJustJavaBigInteger("000")=false<br>
	 * isJustJavaBigInteger("001")=false<br>
	 * 
	 * isJustJavaBigInteger("0")=false<br>
	 * isJustJavaBigInteger("0.0")=false<br>
	 * isJustJavaBigInteger("0.0l")=false<br>
	 * isJustJavaBigInteger("0.0L")=false<br>
	 * isJustJavaBigInteger("0.0f")=false<br>
	 * isJustJavaBigInteger("0.0F")=false<br>
	 * isJustJavaBigInteger("0.0d")=false<br>
	 * isJustJavaBigInteger("0.0D")=false<br>
	 * isJustJavaBigInteger("0.0E")=false<br>
	 * isJustJavaBigInteger("0.0a")=false<br>
	 * 
	 * isJustJavaBigInteger("-0")=false<br>
	 * isJustJavaBigInteger("-0.0")=false<br>
	 * isJustJavaBigInteger("-0.0l")=false<br>
	 * isJustJavaBigInteger("-0.0L")=false<br>
	 * isJustJavaBigInteger("-0.0f")=false<br>
	 * isJustJavaBigInteger("-0.0F")=false<br>
	 * isJustJavaBigInteger("-0.0d")=false<br>
	 * isJustJavaBigInteger("-0.0D")=false<br>
	 * isJustJavaBigInteger("-0.0E")=false<br>
	 * isJustJavaBigInteger("-0.0a")=false<br>
	 * 
	 * isJustJavaBigInteger("+0")=false<br>
	 * isJustJavaBigInteger("+0.0")=false<br>
	 * isJustJavaBigInteger("+0.0l")=false<br>
	 * isJustJavaBigInteger("+0.0L")=false<br>
	 * isJustJavaBigInteger("+0.0f")=false<br>
	 * isJustJavaBigInteger("+0.0F")=false<br>
	 * isJustJavaBigInteger("+0.0d")=false<br>
	 * isJustJavaBigInteger("+0.0D")=false<br>
	 * isJustJavaBigInteger("+0.0E")=false<br>
	 * isJustJavaBigInteger("+0.0a")=false<br>
	 * 
	 * isJustJavaBigInteger("0.12921")=false<br>
	 * isJustJavaBigInteger("0.12921l")=false<br>
	 * isJustJavaBigInteger("0.12921L")=false<br>
	 * isJustJavaBigInteger("0.12921f")=false<br>
	 * isJustJavaBigInteger("0.12921F")=false<br>
	 * isJustJavaBigInteger("0.12921d")=false<br>
	 * isJustJavaBigInteger("0.12921D")=false<br>
	 * isJustJavaBigInteger("0.12921E")=false<br>
	 * isJustJavaBigInteger("0.12921a")=false<br>
	 * 
	 * isJustJavaBigInteger("-0.12921")=false<br>
	 * isJustJavaBigInteger("-0.12921l")=false<br>
	 * isJustJavaBigInteger("-0.12921L")=false<br>
	 * isJustJavaBigInteger("-0.12921f")=false<br>
	 * isJustJavaBigInteger("-0.12921F")=false<br>
	 * isJustJavaBigInteger("-0.12921d")=false<br>
	 * isJustJavaBigInteger("-0.12921D")=false<br>
	 * isJustJavaBigInteger("-0.12921E")=false<br>
	 * isJustJavaBigInteger("-0.12921a")=false<br>
	 * 
	 * isJustJavaBigInteger("+0.12921")=false<br>
	 * isJustJavaBigInteger("+0.12921l")=false<br>
	 * isJustJavaBigInteger("+0.12921L")=false<br>
	 * isJustJavaBigInteger("+0.12921f")=false<br>
	 * isJustJavaBigInteger("+0.12921F")=false<br>
	 * isJustJavaBigInteger("+0.12921d")=false<br>
	 * isJustJavaBigInteger("+0.12921D")=false<br>
	 * isJustJavaBigInteger("+0.12921E")=false<br>
	 * isJustJavaBigInteger("+0.12921a")=false<br>
	 * 
	 * isJustJavaBigInteger("F")=false<br>
	 * 
	 * isJustJavaBigInteger("1,100,000")=false<br>
	 * isJustJavaBigInteger("1,100,000l")=false<br>
	 * isJustJavaBigInteger("1,100,000L")=false<br>
	 * isJustJavaBigInteger("1,100,000f")=false<br>
	 * isJustJavaBigInteger("1.7976931348623157e+308")=false<br>
	 * isJustJavaBigInteger("999999999999999999999999999999999999999999999999999999999999999999999999")=true<br>
	 * isJustJavaBigInteger("-2147483648")=false<br>
	 * isJustJavaBigInteger("2147483647")=false<br>
	 * isJustJavaBigInteger("-2147483659")=false<br>
	 * isJustJavaBigInteger("2147483648")=false<br>
	 * isJustJavaBigInteger("-2147483647")=false<br>
	 * isJustJavaBigInteger("2147483646")=false<br>
	 * isJustJavaBigInteger("-9223372036854775808")=false<br>
	 * isJustJavaBigInteger("9223372036854775807")=false<br>
	 * isJustJavaBigInteger("-9223372036854775809")=true<br>
	 * isJustJavaBigInteger("9223372036854775808")=true<br>
	 * 
	 * 
	 * @date 2022-06-02 11:54:57<br>
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isJustJavaBigInteger(String value) {
		if (isIntegral(value)) {
			if (value.length() > 20) {
				return true;
			}
			BigInteger compare = new BigInteger(value);
			int compareToMax = compare.compareTo(MAX_LONG_BIG);
			int compareToMin = compare.compareTo(MIN_LONG_BIG);
			return compareToMax > 0 || compareToMin < 0;
		}
		return false;
	}

	/**
	 * 
	 * 是否long类型（包含int）<br>
	 * isScopeJavaLong("")=false<br>
	 * 
	 * isScopeJavaLong("1")=true<br>
	 * isScopeJavaLong("1l")=false<br>
	 * isScopeJavaLong("1L")=false<br>
	 * isScopeJavaLong("1f")=false<br>
	 * isScopeJavaLong("1F")=false<br>
	 * isScopeJavaLong("1d")=false<br>
	 * isScopeJavaLong("1D")=false<br>
	 * isScopeJavaLong("1E")=false<br>
	 * isScopeJavaLong("1a")=false<br>
	 * 
	 * isScopeJavaLong("-1")=true<br>
	 * isScopeJavaLong("-1l")=false<br>
	 * isScopeJavaLong("-1L")=false<br>
	 * isScopeJavaLong("-1f")=false<br>
	 * isScopeJavaLong("-1F")=false<br>
	 * isScopeJavaLong("-1d")=false<br>
	 * isScopeJavaLong("-1D")=false<br>
	 * isScopeJavaLong("-1E")=false<br>
	 * isScopeJavaLong("-1a")=false<br>
	 * 
	 * isScopeJavaLong("+1")=true<br>
	 * isScopeJavaLong("+1l")=false<br>
	 * isScopeJavaLong("+1L")=false<br>
	 * isScopeJavaLong("+1f")=false<br>
	 * isScopeJavaLong("+1F")=false<br>
	 * isScopeJavaLong("+1d")=false<br>
	 * isScopeJavaLong("+1D")=false<br>
	 * isScopeJavaLong("+1E")=false<br>
	 * isScopeJavaLong("+1a")=false<br>
	 * 
	 * isScopeJavaLong("000")=true<br>
	 * isScopeJavaLong("001")=true<br>
	 * 
	 * isScopeJavaLong("0")=true<br>
	 * isScopeJavaLong("0.0")=false<br>
	 * isScopeJavaLong("0.0l")=false<br>
	 * isScopeJavaLong("0.0L")=false<br>
	 * isScopeJavaLong("0.0f")=false<br>
	 * isScopeJavaLong("0.0F")=false<br>
	 * isScopeJavaLong("0.0d")=false<br>
	 * isScopeJavaLong("0.0D")=false<br>
	 * isScopeJavaLong("0.0E")=false<br>
	 * isScopeJavaLong("0.0a")=false<br>
	 * 
	 * isScopeJavaLong("-0")=true<br>
	 * isScopeJavaLong("-0.0")=false<br>
	 * isScopeJavaLong("-0.0l")=false<br>
	 * isScopeJavaLong("-0.0L")=false<br>
	 * isScopeJavaLong("-0.0f")=false<br>
	 * isScopeJavaLong("-0.0F")=false<br>
	 * isScopeJavaLong("-0.0d")=false<br>
	 * isScopeJavaLong("-0.0D")=false<br>
	 * isScopeJavaLong("-0.0E")=false<br>
	 * isScopeJavaLong("-0.0a")=false<br>
	 * 
	 * isScopeJavaLong("+0")=true<br>
	 * isScopeJavaLong("+0.0")=false<br>
	 * isScopeJavaLong("+0.0l")=false<br>
	 * isScopeJavaLong("+0.0L")=false<br>
	 * isScopeJavaLong("+0.0f")=false<br>
	 * isScopeJavaLong("+0.0F")=false<br>
	 * isScopeJavaLong("+0.0d")=false<br>
	 * isScopeJavaLong("+0.0D")=false<br>
	 * isScopeJavaLong("+0.0E")=false<br>
	 * isScopeJavaLong("+0.0a")=false<br>
	 * 
	 * isScopeJavaLong("0.12921")=false<br>
	 * isScopeJavaLong("0.12921l")=false<br>
	 * isScopeJavaLong("0.12921L")=false<br>
	 * isScopeJavaLong("0.12921f")=false<br>
	 * isScopeJavaLong("0.12921F")=false<br>
	 * isScopeJavaLong("0.12921d")=false<br>
	 * isScopeJavaLong("0.12921D")=false<br>
	 * isScopeJavaLong("0.12921E")=false<br>
	 * isScopeJavaLong("0.12921a")=false<br>
	 * 
	 * isScopeJavaLong("-0.12921")=false<br>
	 * isScopeJavaLong("-0.12921l")=false<br>
	 * isScopeJavaLong("-0.12921L")=false<br>
	 * isScopeJavaLong("-0.12921f")=false<br>
	 * isScopeJavaLong("-0.12921F")=false<br>
	 * isScopeJavaLong("-0.12921d")=false<br>
	 * isScopeJavaLong("-0.12921D")=false<br>
	 * isScopeJavaLong("-0.12921E")=false<br>
	 * isScopeJavaLong("-0.12921a")=false<br>
	 * 
	 * isScopeJavaLong("+0.12921")=false<br>
	 * isScopeJavaLong("+0.12921l")=false<br>
	 * isScopeJavaLong("+0.12921L")=false<br>
	 * isScopeJavaLong("+0.12921f")=false<br>
	 * isScopeJavaLong("+0.12921F")=false<br>
	 * isScopeJavaLong("+0.12921d")=false<br>
	 * isScopeJavaLong("+0.12921D")=false<br>
	 * isScopeJavaLong("+0.12921E")=false<br>
	 * isScopeJavaLong("+0.12921a")=false<br>
	 * 
	 * isScopeJavaLong("F")=false<br>
	 * 
	 * isScopeJavaLong("1,100,000")=false<br>
	 * isScopeJavaLong("1,100,000l")=false<br>
	 * isScopeJavaLong("1,100,000L")=false<br>
	 * isScopeJavaLong("1,100,000f")=false<br>
	 * isScopeJavaLong("1.7976931348623157e+308")=false<br>
	 * isScopeJavaLong("999999999999999999999999999999999999999999999999999999999999999999999999")=false<br>
	 * isScopeJavaLong("-2147483648")=true<br>
	 * isScopeJavaLong("2147483647")=true<br>
	 * isScopeJavaLong("-2147483659")=true<br>
	 * isScopeJavaLong("2147483648")=true<br>
	 * isScopeJavaLong("-2147483647")=true<br>
	 * isScopeJavaLong("2147483646")=true<br>
	 * isScopeJavaLong("-9223372036854775808")=true<br>
	 * isScopeJavaLong("9223372036854775807")=true<br>
	 * isScopeJavaLong("-9223372036854775809")=false<br>
	 * isScopeJavaLong("9223372036854775808")=false<br>
	 * 
	 * @date 2022-06-02 11:55:27<br>
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isScopeJavaLong(String value) {
		if (isIntegral(value)) {
			if (value.length() > 20) {
				return false;
			}
			BigInteger compare = new BigInteger(value);
			int compareToMax = compare.compareTo(MAX_LONG_BIG);
			int compareToMin = compare.compareTo(MIN_LONG_BIG);
			return compareToMax <= 0 && compareToMin >= 0;
		}
		return false;
	}

	/**
	 * 
	 * 是否BigInteger类型（包含int、long）<br>
	 * isScopeJavaBigInteger("")=false<br>
	 * 
	 * isScopeJavaBigInteger("1")=true<br>
	 * isScopeJavaBigInteger("1l")=false<br>
	 * isScopeJavaBigInteger("1L")=false<br>
	 * isScopeJavaBigInteger("1f")=false<br>
	 * isScopeJavaBigInteger("1F")=false<br>
	 * isScopeJavaBigInteger("1d")=false<br>
	 * isScopeJavaBigInteger("1D")=false<br>
	 * isScopeJavaBigInteger("1E")=false<br>
	 * isScopeJavaBigInteger("1a")=false<br>
	 * 
	 * isScopeJavaBigInteger("-1")=true<br>
	 * isScopeJavaBigInteger("-1l")=false<br>
	 * isScopeJavaBigInteger("-1L")=false<br>
	 * isScopeJavaBigInteger("-1f")=false<br>
	 * isScopeJavaBigInteger("-1F")=false<br>
	 * isScopeJavaBigInteger("-1d")=false<br>
	 * isScopeJavaBigInteger("-1D")=false<br>
	 * isScopeJavaBigInteger("-1E")=false<br>
	 * isScopeJavaBigInteger("-1a")=false<br>
	 * 
	 * isScopeJavaBigInteger("+1")=true<br>
	 * isScopeJavaBigInteger("+1l")=false<br>
	 * isScopeJavaBigInteger("+1L")=false<br>
	 * isScopeJavaBigInteger("+1f")=false<br>
	 * isScopeJavaBigInteger("+1F")=false<br>
	 * isScopeJavaBigInteger("+1d")=false<br>
	 * isScopeJavaBigInteger("+1D")=false<br>
	 * isScopeJavaBigInteger("+1E")=false<br>
	 * isScopeJavaBigInteger("+1a")=false<br>
	 * 
	 * isScopeJavaBigInteger("000")=true<br>
	 * isScopeJavaBigInteger("001")=true<br>
	 * 
	 * isScopeJavaBigInteger("0")=true<br>
	 * isScopeJavaBigInteger("0.0")=false<br>
	 * isScopeJavaBigInteger("0.0l")=false<br>
	 * isScopeJavaBigInteger("0.0L")=false<br>
	 * isScopeJavaBigInteger("0.0f")=false<br>
	 * isScopeJavaBigInteger("0.0F")=false<br>
	 * isScopeJavaBigInteger("0.0d")=false<br>
	 * isScopeJavaBigInteger("0.0D")=false<br>
	 * isScopeJavaBigInteger("0.0E")=false<br>
	 * isScopeJavaBigInteger("0.0a")=false<br>
	 * 
	 * isScopeJavaBigInteger("-0")=true<br>
	 * isScopeJavaBigInteger("-0.0")=false<br>
	 * isScopeJavaBigInteger("-0.0l")=false<br>
	 * isScopeJavaBigInteger("-0.0L")=false<br>
	 * isScopeJavaBigInteger("-0.0f")=false<br>
	 * isScopeJavaBigInteger("-0.0F")=false<br>
	 * isScopeJavaBigInteger("-0.0d")=false<br>
	 * isScopeJavaBigInteger("-0.0D")=false<br>
	 * isScopeJavaBigInteger("-0.0E")=false<br>
	 * isScopeJavaBigInteger("-0.0a")=false<br>
	 * 
	 * isScopeJavaBigInteger("+0")=true<br>
	 * isScopeJavaBigInteger("+0.0")=false<br>
	 * isScopeJavaBigInteger("+0.0l")=false<br>
	 * isScopeJavaBigInteger("+0.0L")=false<br>
	 * isScopeJavaBigInteger("+0.0f")=false<br>
	 * isScopeJavaBigInteger("+0.0F")=false<br>
	 * isScopeJavaBigInteger("+0.0d")=false<br>
	 * isScopeJavaBigInteger("+0.0D")=false<br>
	 * isScopeJavaBigInteger("+0.0E")=false<br>
	 * isScopeJavaBigInteger("+0.0a")=false<br>
	 * 
	 * isScopeJavaBigInteger("0.12921")=false<br>
	 * isScopeJavaBigInteger("0.12921l")=false<br>
	 * isScopeJavaBigInteger("0.12921L")=false<br>
	 * isScopeJavaBigInteger("0.12921f")=false<br>
	 * isScopeJavaBigInteger("0.12921F")=false<br>
	 * isScopeJavaBigInteger("0.12921d")=false<br>
	 * isScopeJavaBigInteger("0.12921D")=false<br>
	 * isScopeJavaBigInteger("0.12921E")=false<br>
	 * isScopeJavaBigInteger("0.12921a")=false<br>
	 * 
	 * isScopeJavaBigInteger("-0.12921")=false<br>
	 * isScopeJavaBigInteger("-0.12921l")=false<br>
	 * isScopeJavaBigInteger("-0.12921L")=false<br>
	 * isScopeJavaBigInteger("-0.12921f")=false<br>
	 * isScopeJavaBigInteger("-0.12921F")=false<br>
	 * isScopeJavaBigInteger("-0.12921d")=false<br>
	 * isScopeJavaBigInteger("-0.12921D")=false<br>
	 * isScopeJavaBigInteger("-0.12921E")=false<br>
	 * isScopeJavaBigInteger("-0.12921a")=false<br>
	 * 
	 * isScopeJavaBigInteger("+0.12921")=false<br>
	 * isScopeJavaBigInteger("+0.12921l")=false<br>
	 * isScopeJavaBigInteger("+0.12921L")=false<br>
	 * isScopeJavaBigInteger("+0.12921f")=false<br>
	 * isScopeJavaBigInteger("+0.12921F")=false<br>
	 * isScopeJavaBigInteger("+0.12921d")=false<br>
	 * isScopeJavaBigInteger("+0.12921D")=false<br>
	 * isScopeJavaBigInteger("+0.12921E")=false<br>
	 * isScopeJavaBigInteger("+0.12921a")=false<br>
	 * 
	 * isScopeJavaBigInteger("F")=false<br>
	 * 
	 * isScopeJavaBigInteger("1,100,000")=false<br>
	 * isScopeJavaBigInteger("1,100,000l")=false<br>
	 * isScopeJavaBigInteger("1,100,000L")=false<br>
	 * isScopeJavaBigInteger("1,100,000f")=false<br>
	 * isScopeJavaBigInteger("1.7976931348623157e+308")=false<br>
	 * isScopeJavaBigInteger("999999999999999999999999999999999999999999999999999999999999999999999999")=true<br>
	 * isScopeJavaBigInteger("-2147483648")=true<br>
	 * isScopeJavaBigInteger("2147483647")=true<br>
	 * isScopeJavaBigInteger("-2147483659")=true<br>
	 * isScopeJavaBigInteger("2147483648")=true<br>
	 * isScopeJavaBigInteger("-2147483647")=true<br>
	 * isScopeJavaBigInteger("2147483646")=true<br>
	 * isScopeJavaBigInteger("-9223372036854775808")=true<br>
	 * isScopeJavaBigInteger("9223372036854775807")=true<br>
	 * isScopeJavaBigInteger("-9223372036854775809")=true<br>
	 * isScopeJavaBigInteger("9223372036854775808")=true<br>
	 * 
	 * @date 2022-06-02 11:55:45<br>
	 * @param value
	 * @return
	 * @since 1.0.0
	 */
	public static boolean isScopeJavaBigInteger(String value) {
		return isIntegral(value);
	}
}
