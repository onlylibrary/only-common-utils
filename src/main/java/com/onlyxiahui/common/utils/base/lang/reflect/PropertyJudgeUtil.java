package com.onlyxiahui.common.utils.base.lang.reflect;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Date 2019-01-11 14:14:40<br>
 * Description
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class PropertyJudgeUtil {

	/****************** primitive start ******************************/
	public static boolean isInteger(Class<?> classType) {
		return Integer.class == classType
				|| Integer.class.isAssignableFrom(classType)
				|| int.class == classType
				|| int.class.isAssignableFrom(classType);
	}

	public static boolean isLong(Class<?> classType) {
		return Long.class == classType
				|| Long.class.isAssignableFrom(classType)
				|| long.class == classType
				|| long.class.isAssignableFrom(classType);
	}

	public static boolean isFloat(Class<?> classType) {
		return Float.class == classType
				|| Float.class.isAssignableFrom(classType)
				|| (float.class == classType)
				|| float.class.isAssignableFrom(classType);
	}

	public static boolean isDouble(Class<?> classType) {
		return Double.class == classType
				|| Double.class.isAssignableFrom(classType)
				|| double.class == classType
				|| double.class.isAssignableFrom(classType);
	}

	public static boolean isByte(Class<?> classType) {
		return Byte.class == classType
				|| Byte.class.isAssignableFrom(classType)
				|| byte.class == classType
				|| byte.class.isAssignableFrom(classType);
	}

	public static boolean isShort(Class<?> classType) {
		return Short.class == classType
				|| Short.class.isAssignableFrom(classType)
				|| short.class == classType
				|| short.class.isAssignableFrom(classType);
	}

	public static boolean isCharacter(Class<?> classType) {
		return Character.class == classType
				|| Character.class.isAssignableFrom(classType)
				|| char.class == classType
				|| char.class.isAssignableFrom(classType);
	}

	public static boolean isBoolean(Class<?> classType) {
		return Boolean.class == classType
				|| Boolean.class.isAssignableFrom(classType)
				|| boolean.class == classType
				|| boolean.class.isAssignableFrom(classType);
	}

	public static boolean isPrimitive(Class<?> classType) {
		return null != classType && (classType.isPrimitive()
				|| isInteger(classType)
				|| isLong(classType)
				|| isFloat(classType)
				|| isDouble(classType)
				|| isByte(classType)
				|| isShort(classType)
				|| isCharacter(classType)
				|| isBoolean(classType));
	}

	/****************** primitive end ******************************/

	public static boolean isAtomicInteger(Class<?> classType) {
		return AtomicInteger.class.isAssignableFrom(classType);
	}

	public static boolean isBigInteger(Class<?> classType) {
		return BigInteger.class.isAssignableFrom(classType);
	}

	public static boolean isBigDecimal(Class<?> classType) {
		return BigDecimal.class.isAssignableFrom(classType);
	}

	public static boolean isNumber(Class<?> classType) {
		return Number.class.isAssignableFrom(classType);
	}

	public static boolean isBaseProperty(Class<?> classType) {
		return Comparable.class.isAssignableFrom(classType);
	}

	public static boolean isComparable(Class<?> classType) {
		return Comparable.class.isAssignableFrom(classType);
	}

	public static boolean isArray(Class<?> classType) {
		return null != classType && classType.isArray();
	}

	public static boolean isString(Class<?> classType) {
		return String.class.isAssignableFrom(classType);
	}

	public static boolean isCharSequence(Class<?> classType) {
		return CharSequence.class.isAssignableFrom(classType);
	}

	public static boolean isCollection(Class<?> classType) {
		return Collection.class.isAssignableFrom(classType);
	}

	public static boolean isMap(Class<?> classType) {
		return Map.class.isAssignableFrom(classType);
	}

	public static boolean isJdkMap(Class<?> classType) {
		boolean is = false;
		if (null != classType && Map.class.isAssignableFrom(classType)) {
			String[] ps = { "java.", "javax.", "jdk.", "sun.", "com.sun." };
			for (String p : ps) {
				if (classType.getPackage().getName().startsWith(p)) {
					is = true;
					break;
				}
			}

		}
		return is;
	}

	/******************************/

	public static boolean isNull(Object o) {
		return (null == o);
	}

	public static boolean isNotNull(Object o) {
		return !isNull(o);
	}

	public static boolean isEmpty(Object o) {
		return (null == o || "".equals(o));
	}

	public static boolean isNotEmpty(Object o) {
		return !(isEmpty(o));
	}

	public static boolean isInteger(Object o) {
		return (o instanceof Integer);
	}

	public static boolean isLong(Object o) {
		return (o instanceof Long);
	}

	public static boolean isFloat(Object o) {
		return (o instanceof Float);
	}

	public static boolean isDouble(Object o) {
		return (o instanceof Double);
	}

	public static boolean isByte(Object o) {
		return (o instanceof Byte);
	}

	public static boolean isCharacter(Object o) {
		return (o instanceof Character);
	}

	public static boolean isShort(Object o) {
		return (o instanceof Short);
	}

	public static boolean isBoolean(Object o) {
		return (o instanceof Boolean);
	}

	/**
	 * 判断对象是否为8种基本类型的对象。
	 *
	 * @param o
	 * @return boolean
	 */
	public static boolean isPrimitive(Object o) {
		return (isInteger(o))
				|| isLong(o)
				|| isFloat(o)
				|| isDouble(o)
				|| isByte(o)
				|| isCharacter(o)
				|| isShort(o)
				|| isBoolean(o)
				|| (null != o && isPrimitive(o.getClass()));
	}

	/**
	 * Description 是否为字符串类型 <br>
	 * Date 2019-03-30 17:31:10<br>
	 *
	 * @param o
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isString(Object o) {
		return (o instanceof String);
	}

	public static boolean isCharSequence(Object o) {
		return (o instanceof CharSequence);
	}
}
