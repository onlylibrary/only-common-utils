package com.onlyxiahui.common.utils.base.lang.primitive;

import java.math.BigDecimal;

import com.onlyxiahui.common.utils.base.lang.number.NumberArithmeticUtil;

/**
 * <br>
 *
 * @date 2022-05-24 11:12:53<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */

public class DoubleArithmeticUtil {

	/**
	 * 两个Double数相加
	 *
	 * @param v1
	 * @param v2
	 * @return double
	 */
	public static double add(double v1, double v2) {
		return NumberArithmeticUtil.add(v1, v2).doubleValue();
	}

	/**
	 * 两个Double数相减
	 *
	 * @param v1
	 * @param v2
	 * @return double
	 */
	public static double subtract(double v1, double v2) {
		return NumberArithmeticUtil.subtract(v1, v2).doubleValue();
	}

	/**
	 * 两个Double数相乘
	 *
	 * @param v1
	 * @param v2
	 * @return Double
	 */
	public static double multiply(double v1, double v2) {
		return NumberArithmeticUtil.multiply(v1, v2).doubleValue();
	}

	/**
	 * 两个Double数相除
	 *
	 * @param v1
	 * @param v2
	 * @return Double
	 */
	public static double divide(double v1, double v2) {
		return NumberArithmeticUtil.divide(v1, v2).doubleValue();

	}

	/**
	 * 两个Double数相除，并保留scale位小数
	 *
	 * @param v1
	 * @param v2
	 * @param scale
	 * @return double
	 */
	public static double divide(double v1, double v2, int scale) {
		return NumberArithmeticUtil.divide(v1, v2, scale).doubleValue();
	}

	/**
	 * 返回两个数里比较大的值
	 *
	 * @param v1
	 * @param v2
	 * @return double
	 */
	public static double max(double v1, double v2) {
		return NumberArithmeticUtil.max(v1, v2).doubleValue();
	}

	/**
	 * 返回两个数里比较小的值
	 *
	 * @param v1
	 * @param v2
	 * @return double
	 */
	public static double min(double v1, double v2) {
		return NumberArithmeticUtil.min(v1, v2).doubleValue();
	}

	/**
	 * 对double数据取精度，例输入1.666666，3，返回1.667
	 *
	 * @param v
	 * @param scale
	 * @return double
	 */
	public static double round(double v, int scale) {
		String sv = Double.toString(v);
		BigDecimal b = new BigDecimal(sv);
		b = b.setScale(scale, BigDecimal.ROUND_HALF_UP);
		return b.doubleValue();
	}

	/**
	 * 取余数，输入55.5，40，2，返回15.5
	 *
	 * @param v1
	 * @param v2
	 * @param scale
	 * @return double
	 */
	public static double remainder(double v1, double v2, int scale) {
		String sv1 = Double.toString(v1);
		String sv2 = Double.toString(v2);
		BigDecimal b1 = new BigDecimal(sv1);
		BigDecimal b2 = new BigDecimal(sv2);
		return b1.remainder(b2).setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
}
