package com.onlyxiahui.common.utils.base.util.time;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * 
 * Description LocalDateUtil<br>
 * Date 2019-03-30 17:47:42<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class LocalDateUtil {
	/**
	 * 日期+时间+毫秒，格式（yyyy-MM-dd HH:mm:ss.SSS）
	 */
	public static final String FORMAT_DATE_TIME_MILLISECOND = "yyyy-MM-dd HH:mm:ss.SSS";
	/**
	 * 日期+时间，格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 日期，格式（yyyy-MM-dd 不含时分秒）
	 */
	public static final String FORMAT_DATE = "yyyy-MM-dd";
	/**
	 * 时间，格式（HH:mm:ss 不含日期）
	 */
	public static final String FORMAT_TIME = "HH:mm:ss";

	/**
	 * 年，格式（yyyy）
	 */
	public static final String FORMAT_YEAR = "yyyy";
	/**
	 * 月，格式（MM）
	 */
	public static final String FORMAT_MONTH = "MM";
	/**
	 * 日，格式（dd）
	 */
	public static final String FORMAT_DAY = "dd";
	/**
	 * 时，格式（HH）
	 */
	public static final String FORMAT_HOUR = "HH";

	/**
	 * 
	 * Description 获取当前日期+时间+毫秒<br>
	 * Format yyyy-MM-dd HH:mm:ss.SSS<br>
	 * Sample 2020-10-10 10:10:10.999<br>
	 * Date 2019-03-30 17:47:57<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDateTimeMillisecond() {
		return DateTimeFormatter.ofPattern(FORMAT_DATE_TIME_MILLISECOND).format(LocalDateTime.now());
	}

	/**
	 * 
	 * Description 获取当前日期+时间<br>
	 * Format yyyy-MM-dd HH:mm:ss<br>
	 * Sample 2020-10-10 10:10:10<br>
	 * Date 2019-03-30 17:48:17<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDateTime() {
		return DateTimeFormatter.ofPattern(FORMAT_DATE_TIME).format(LocalDateTime.now());
	}

	/**
	 * 
	 * Description 获取当前日期（年-月-日）<br>
	 * Format yyyy-MM-dd<br>
	 * Sample 2020-10-10<br>
	 * Date 2019-03-30 17:48:50<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentDate() {
		return DateTimeFormatter.ofPattern(FORMAT_DATE).format(LocalDate.now());
	}

	/**
	 * 
	 * Description 获取当前时间（时:分:秒）<br>
	 * Format HH:mm:ss<br>
	 * Sample 17:49:38<br>
	 * Date 2019-03-30 17:49:38<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentTime() {
		return DateTimeFormatter.ofPattern(FORMAT_TIME).format(LocalTime.now());
	}

	/**
	 * 
	 * Description 获取当前年份<br>
	 * Format yyyy<br>
	 * Sample 2020<br>
	 * 
	 * Date 2020-11-10 10:02:25<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public static String getCurrentYear() {
		return DateTimeFormatter.ofPattern(FORMAT_YEAR).format(LocalTime.now());
	}

	/**
	 * 
	 * Description 获取当前月<br>
	 * Format MM<br>
	 * Sample 08<br>
	 * 
	 * Date 2020-11-10 10:02:36<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public static String getCurrentMonth() {
		return DateTimeFormatter.ofPattern(FORMAT_MONTH).format(LocalTime.now());
	}

	/**
	 * 
	 * Description 获取当前天<br>
	 * Format dd<br>
	 * Sample 28<br>
	 * <br>
	 * Date 2020-11-10 10:02:43<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public static String getCurrentDay() {
		return DateTimeFormatter.ofPattern(FORMAT_DAY).format(LocalTime.now());
	}

	/**
	 * 
	 * Description 获取当前小时<br>
	 * Format HH<br>
	 * Sample 23<br>
	 * <br>
	 * Date 2020-11-10 10:02:50<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrentHour() {
		return DateTimeFormatter.ofPattern(FORMAT_HOUR).format(LocalTime.now());
	}

	/**
	 * 
	 * Description 获取当前自定义格式时间<br>
	 * Date 2019-03-30 17:49:54<br>
	 * 
	 * @param format :自定义时间格式
	 * @return String
	 * @since 1.0.0
	 */
	public static String getCurrent(String format) {
		return DateTimeFormatter.ofPattern(format).format(LocalDateTime.now());
	}

	/**
	 * 
	 * Description 将毫秒转为（HH:mm:ss）多少小时：多少分钟：多少秒<br>
	 * Date 2019-03-30 17:50:10<br>
	 * 
	 * @param time
	 * @return String
	 * @since 1.0.0
	 */
	public static String millisecondToTime(long millisecond) {
		StringBuilder sb = new StringBuilder();
		millisecond = millisecond / 1000L;

		long hour = millisecond / 3600L;
		long minute = (millisecond - hour * 3600L) / 60L;
		long second = millisecond - hour * 3600L - minute * 60L;

		sb.append((hour > 9 ? hour : ("0" + hour)));
		sb.append(":");
		sb.append((minute > 9 ? minute : ("0" + minute)));
		sb.append(":");
		sb.append((second > 9 ? second : ("0" + second)));
		return sb.toString();
	}

	/**
	 * 
	 * Description 字符串日期转LocalDate<br>
	 * Date 2019-03-30 17:50:29<br>
	 * 
	 * @param time
	 * @param format
	 * @return LocalDate
	 * @since 1.0.0
	 */
	public static LocalDate stringToLocalDate(String time, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		LocalDate date = LocalDate.from(formatter.parse(time));
		return date;
	}

	/**
	 * 
	 * Description 字符串日期+时间转LocalDateTime<br>
	 * Date 2019-03-30 17:50:42<br>
	 * 
	 * @param time
	 * @param format
	 * @return LocalDateTime
	 * @since 1.0.0
	 */
	public static LocalDateTime stringToLocalDateTime(String time, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		LocalDateTime dateTime = LocalDateTime.from(formatter.parse(time));
		return dateTime;
	}

	/**
	 * 
	 * Description 格式化日期时间<br>
	 * Date 2019-03-30 17:50:54<br>
	 * 
	 * @param dateTime
	 * @param format
	 * @return String
	 * @since 1.0.0
	 */
	public static String format(LocalDateTime dateTime, String format) {
		return DateTimeFormatter.ofPattern(format).format(dateTime);
	}

	/**
	 * 
	 * Description 格式化日期<br>
	 * Date 2019-03-30 17:51:03<br>
	 * 
	 * @param date
	 * @param format
	 * @return String
	 * @since 1.0.0
	 */
	public static String format(LocalDate date, String format) {
		return DateTimeFormatter.ofPattern(format).format(date);
	}

	/**
	 * 
	 * Description 获取指定天数后的时间<br>
	 * Date 2019-03-30 17:51:18<br>
	 * 
	 * @param days
	 * @return LocalDateTime
	 * @since 1.0.0
	 */
	public static LocalDateTime getAddDays(long days) {
		LocalDateTime dateTime = LocalDateTime.now().plusDays(days);
		return dateTime;
	}

	/**
	 * 
	 * Description 毫秒时间戳转日期+时间<br>
	 * Date 2019-03-30 18:15:12<br>
	 * 
	 * @param timestamp
	 * @return LocalDateTime
	 * @since 1.0.0
	 */
	public static LocalDateTime millisecondTimestampToLocalDateTime(long timestamp) {
		LocalDateTime dateTime = Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
		return dateTime;
	}

	/**
	 * 
	 * Description 毫秒时间戳转日期<br>
	 * Date 2020-11-10 09:54:57<br>
	 * 
	 * @param timestamp
	 * @return
	 * @since 1.0.0
	 */
	public static LocalDate millisecondTimestampToLocalDate(long timestamp) {
		LocalDate date = Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDate();
		return date;
	}

	/**
	 * 
	 * Description 大部分时间格式转yyyy-MM-dd HH:mm:ss<br>
	 * Date 2019-03-30 17:58:19<br>
	 * 
	 * @param text
	 * @return String
	 * @since 1.0.0
	 */
	public static String allToDateTime(String text) {
		if (null != text) {
			// 格式全部转化成-间隔
			text = text.replaceAll("[/\\.]", "-");
			String matches = "\\d{4}";
			if (text.matches(matches)) {
				// 只有年份，补全月日时分秒
				text += "-01-01 00:00:00";
			} else if (text.matches("\\d{4}-\\d{1,2}")) {
				// 只有年月，补全日时分秒
				text += "-01 00:00:00";
			} else if (text.matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {
				// 只有年月日，补全时分秒
				text += " 00:00:00";
			} else if (text.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}")) {
				// 只有年月日时，补全分秒
				text += ":00:00";
			} else if (text.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}")) {
				// 只有年月日时分，补全秒
				text += ":00";
			}
		}
		return text;
	}

	/**
	 * 
	 * Description 大部分时间格式转yyyy-MM-dd HH:mm:ss.SSS<br>
	 * Date 2019-03-30 17:58:19<br>
	 * 
	 * @param text
	 * @return String
	 * @since 1.0.0
	 */
	public static String allToDateTimeMillisecond(String text) {
		if (null != text) {
			// 格式全部转化成-间隔
			text = text.replaceAll("([/\\\\])", "-");
			String matches = "\\d{4}";
			if (text.matches(matches)) {
				// 只有年份，补全月日时分秒
				text += "-01-01 00:00:00.000";
			} else if (text.matches("\\d{4}-\\d{1,2}")) {
				// 只有年月，补全日时分秒
				text += "-01 00:00:00.000";
			} else if (text.matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {
				// 只有年月日，补全时分秒
				text += " 00:00:00.000";
			} else if (text.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}")) {
				// 只有年月日时，补全分秒
				text += ":00:00.000";
			} else if (text.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}")) {
				// 只有年月日时分，补全秒
				text += ":00.000";
			} else if (text.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}")) {
				// 只有年月日时分秒，补全毫秒秒
				text += ".000";
			}
		}
		return text;
	}
}
