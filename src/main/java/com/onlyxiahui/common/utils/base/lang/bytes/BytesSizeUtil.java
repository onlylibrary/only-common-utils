package com.onlyxiahui.common.utils.base.lang.bytes;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Description <br>
 * Date 2020-10-12 09:25:04<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class BytesSizeUtil {

	public static BigInteger SCALE = new BigInteger("1024");
	public static BigInteger KB = SCALE;
	public static BigInteger MB = BytesSizeUtil.KB.multiply(BytesSizeUtil.SCALE);
	public static BigInteger GB = BytesSizeUtil.MB.multiply(BytesSizeUtil.SCALE);
	public static BigInteger TB = BytesSizeUtil.GB.multiply(BytesSizeUtil.SCALE);
	public static BigInteger PB = BytesSizeUtil.TB.multiply(BytesSizeUtil.SCALE);
	public static BigInteger EB = BytesSizeUtil.PB.multiply(BytesSizeUtil.SCALE);
	public static BigInteger ZB = BytesSizeUtil.EB.multiply(BytesSizeUtil.SCALE);
	public static BigInteger YB = BytesSizeUtil.ZB.multiply(BytesSizeUtil.SCALE);
	public static BigInteger BB = BytesSizeUtil.YB.multiply(BytesSizeUtil.SCALE);

	public static String KB_UNIT = "KB";
	public static String MB_UNIT = "MB";
	public static String GB_UNIT = "GB";
	public static String TB_UNIT = "TB";
	public static String PB_UNIT = "PB";
	public static String EB_UNIT = "EB";
	public static String ZB_UNIT = "ZB";
	public static String YB_UNIT = "YB";
	public static String BB_UNIT = "BB";

	public static String getSizeUnit(Long size) {
		return getSizeUnit(new BigInteger(Long.toString(size)), 2);
	}

	public static String getSizeUnit(Long size, Integer scale) {
		return getSizeUnit(new BigInteger(Long.toString(size)), scale);
	}

	public static String getSizeUnit(BigInteger size, Integer scale) {

		String text = "0B";

		if (null == size) {
			return text;
		}
		BigDecimal bigSize = new BigDecimal(size);

		scale = (null == scale) ? 2 : (scale <= 0 ? 2 : scale);
		// 设置保留位数
		// DecimalFormat df = new DecimalFormat("0.00");

		if (size.compareTo(BytesSizeUtil.KB) < 0) {
			text = bigSize + "B";
		} else if (BytesSizeUtil.KB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.MB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.KB), scale, BigDecimal.ROUND_HALF_UP)) + "KB";
		} else if (BytesSizeUtil.MB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.GB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.MB), scale, BigDecimal.ROUND_HALF_UP)) + "MB";
		} else if (BytesSizeUtil.GB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.TB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.GB), scale, BigDecimal.ROUND_HALF_UP)) + "GB";
		} else if (BytesSizeUtil.TB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.PB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.TB), scale, BigDecimal.ROUND_HALF_UP)) + "TB";
		} else if (BytesSizeUtil.PB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.EB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.PB), scale, BigDecimal.ROUND_HALF_UP)) + "PB";
		} else if (BytesSizeUtil.EB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.ZB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.EB), scale, BigDecimal.ROUND_HALF_UP)) + "EB";
		} else if (BytesSizeUtil.ZB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.YB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.ZB), scale, BigDecimal.ROUND_HALF_UP)) + "ZB";
		} else if (BytesSizeUtil.YB.compareTo(size) <= 0 && size.compareTo(BytesSizeUtil.BB) < 0) {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.YB), scale, BigDecimal.ROUND_HALF_UP)) + "YB";
		} else {
			text = (bigSize.divide(new BigDecimal(BytesSizeUtil.BB), scale, BigDecimal.ROUND_HALF_UP)) + "BB";
		}
		return text;
	}
}
