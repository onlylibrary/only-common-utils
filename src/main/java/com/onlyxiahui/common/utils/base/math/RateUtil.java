package com.onlyxiahui.common.utils.base.math;

import java.math.BigDecimal;

/**
 * Description <br>
 * Date 2020-11-11 11:39:27<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class RateUtil {

	public static Integer toPercentageIntegerRate(Double percentage) {
		Integer rate = 0;
		if (null != percentage) {
			rate = percentage.intValue();
		}
		return rate;
	}

	public static Integer getPercentageIntegerRate(Long total, Long part) {
		Double percentage = RateUtil.getPercentageDecimalsRate(total, part);
		Integer value = RateUtil.toPercentageIntegerRate(percentage);
		return value;
	}

	public static Double getPercentageDecimalsRate(Long total, Long part) {
		Double percentage = RateUtil.getLessDecimalsRate(total, part);
		Double rate = (null != percentage) ? percentage * 100 : 0D;
		return rate;
	}

	/**
	 * 
	 * Description 获取小数比例<br>
	 * Date 2020-11-11 11:35:07<br>
	 * 
	 * @param total
	 * @param part
	 * @return
	 * @since 1.0.0
	 */
	public static Double getLessDecimalsRate(Long total, Long part) {
		return getLessDecimalsRate(total, part, 2);
	}

	public static Double getLessDecimalsRate(Long total, Long part, Integer scale) {
		// (null != total && null != part) ? ((part > 0 && total > 0) ? (part / total)
		// :0D) : 0D;
		Double percentage = 0.0D;
		if ((null != total && total > 0 && null != part)) {
			scale = (null == scale) ? 2 : (scale <= 0 ? 2 : scale);
			BigDecimal bigTotal = new BigDecimal(Long.toString(total));
			BigDecimal bigPart = new BigDecimal(Long.toString(part));
			BigDecimal rate = bigPart.divide(bigTotal, scale, BigDecimal.ROUND_HALF_UP);
			percentage = rate.doubleValue();
		}
		return percentage;
	}
}
