package com.onlyxiahui.common.utils.base.util.thread;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * 
 * Description 固定线程池 <br>
 * Date 2020-11-09 17:46:41<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class ThreadFixedPool {
	/**
	 * 线程池执行者
	 */
	private final ExecutorService executor;
	private BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>();

	public ThreadFixedPool() {
		this(
				// 核心线程数量
				ThreadPoolBuilder.CORE_POOL_SIZE,
				// 最大线程数量，线程池所容纳最大线程数(workQueue队列满了之后才开启)
				ThreadPoolBuilder.MAXIMUM_POOL_SIZE);
	}

	public ThreadFixedPool(int corePoolSize, int maximumPoolSize) {
		Integer count = ThreadPoolBuilder.getCount();
		String nameFormat = "build-" + count + "-fixed-pool-%d";
		executor = ThreadPoolBuilder.build(
				// 核心线程数量
				corePoolSize,
				// 最大线程数量，线程池所容纳最大线程数(workQueue队列满了之后才开启)
				maximumPoolSize,
				// 当非核心线程闲置空闲时，保持活跃的时间
				ThreadPoolBuilder.KEEP_ALIVE,
				// keepAliveTime的单位 ，毫秒级
				TimeUnit.MILLISECONDS,
				// 线程任务等待队列，存储还未执行的任务
				workQueue,
				// 创建线程的工厂
				new ThreadFactoryBuilder().setNameFormat(nameFormat).build(),
				// 由于达到线程边界或队列容量而阻塞时的处理程序
				new ThreadPoolExecutor.AbortPolicy());
	}

	/**
	 * 获取创建的线程池对象
	 *
	 * @return 线程池对象
	 */
	public ExecutorService getExecutor() {
		return executor;
	}

	/**
	 * 
	 * 获取线程队列的大小<br>
	 * Date 2019-10-24 15:34:27<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	public int getQueueSize() {
		return workQueue.size();
	}

	/**
	 * 在未来某个时间执行给定的命令
	 * <p>
	 * 该命令可能在新的线程、已入池的线程或者正调用的线程中执行，这由 Executor 实现决定。
	 * </p>
	 *
	 * @param command 命令
	 */
	public void execute(final Runnable command) {
		executor.execute(command);
	}

	/**
	 * 在未来某个时间执行给定的命令链表
	 * <p>
	 * 该命令可能在新的线程、已入池的线程或者正调用的线程中执行，这由 Executor 实现决定。
	 * </p>
	 *
	 * @param commands 命令链表
	 */
	public void execute(final List<Runnable> commands) {
		for (Runnable command : commands) {
			executor.execute(command);
		}
	}

	/**
	 * 待以前提交的任务执行完毕后关闭线程池
	 * <p>
	 * 启动一次顺序关闭，执行以前提交的任务，但不接受新任务。如果已经关闭，则调用没有作用。
	 * </p>
	 */
	public void shutDown() {
		executor.shutdown();
	}

	/**
	 * 试图停止所有正在执行的活动任务
	 * <p>
	 * 试图停止所有正在执行的活动任务，暂停处理正在等待的任务，并返回等待执行的任务列表。
	 * </p>
	 * <p>
	 * 无法保证能够停止正在处理的活动执行任务，但是会尽力尝试。
	 * </p>
	 *
	 * @return 等待执行的任务的列表
	 */
	public List<Runnable> shutDownNow() {
		return executor.shutdownNow();
	}

	/**
	 * 判断线程池是否已关闭
	 *
	 * @return {@code true}: 是<br>
	 *         {@code false}: 否
	 */
	public boolean isShutDown() {
		return executor.isShutdown();
	}

	/**
	 * 关闭线程池后判断所有任务是否都已完成
	 * <p>
	 * 注意，除非首先调用 shutdown 或 shutdownNow，否则 isTerminated 永不为 true。
	 * </p>
	 *
	 * @return {@code true}: 是<br>
	 *         {@code false}: 否
	 */
	public boolean isTerminated() {
		return executor.isTerminated();
	}

	/**
	 * 请求关闭、发生超时或者当前线程中断
	 * <p>
	 * 无论哪一个首先发生之后，都将导致阻塞，直到所有任务完成执行。
	 * </p>
	 *
	 * @param timeout 最长等待时间
	 * @param unit    时间单位
	 * @return {@code true}: 请求成功<br>
	 *         {@code false}: 请求超时
	 * @throws InterruptedException 终端异常
	 */
	public boolean awaitTermination(final long timeout, final TimeUnit unit) throws InterruptedException {
		return executor.awaitTermination(timeout, unit);
	}

	/**
	 * 提交一个Callable任务用于执行
	 * <p>
	 * 如果想立即阻塞任务的等待，则可以使用{@code result = exec.submit(aCallable).get();}形式的构造。
	 * </p>
	 *
	 * @param task 任务
	 * @param <T>  泛型
	 * @return 表示任务等待完成的Future, 该Future的{@code get}方法在成功完成时将会返回该任务的结果。
	 */
	public <T> Future<T> submit(final Callable<T> task) {
		return executor.submit(task);
	}

	/**
	 * 提交一个Runnable任务用于执行
	 *
	 * @param task   任务
	 * @param result 返回的结果
	 * @param <T>    泛型
	 * @return 表示任务等待完成的Future, 该Future的{@code get}方法在成功完成时将会返回该任务的结果。
	 */
	public <T> Future<T> submit(final Runnable task, final T result) {
		return executor.submit(task, result);
	}

	/**
	 * 提交一个Runnable任务用于执行
	 *
	 * @param task 任务
	 * @return 表示任务等待完成的Future, 该Future的{@code get}方法在成功完成时将会返回null结果。
	 */
	public Future<?> submit(final Runnable task) {
		return executor.submit(task);
	}

	/**
	 * 执行给定的任务
	 * <p>
	 * 当所有任务完成时，返回保持任务状态和结果的Future列表。 返回列表的所有元素的{@link Future#isDone}为{@code true}。
	 * 注意，可以正常地或通过抛出异常来终止已完成任务。 如果正在进行此操作时修改了给定的 collection，则此方法的结果是不确定的。
	 * </p>
	 *
	 * @param tasks 任务集合
	 * @param <T>   泛型
	 * @return 表示任务的 Future 列表，列表顺序与给定任务列表的迭代器所生成的顺序相同，每个任务都已完成。
	 * @throws InterruptedException 如果等待时发生中断，在这种情况下取消尚未完成的任务。
	 */
	public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> tasks) throws InterruptedException {
		return executor.invokeAll(tasks);
	}

	/**
	 * 执行给定的任务
	 * <p>
	 * 当所有任务完成或超时期满时(无论哪个首先发生)，返回保持任务状态和结果的Future列表。
	 * 返回列表的所有元素的{@link Future#isDone}为{@code true}。 一旦返回后，即取消尚未完成的任务。
	 * 注意，可以正常地或通过抛出异常来终止已完成任务。 如果此操作正在进行时修改了给定的 collection，则此方法的结果是不确定的。
	 * </p>
	 *
	 * @param tasks   任务集合
	 * @param timeout 最长等待时间
	 * @param unit    时间单位
	 * @param <T>     泛型
	 * @return 表示任务的 Future
	 *         列表，列表顺序与给定任务列表的迭代器所生成的顺序相同。如果操作未超时，则已完成所有任务。如果确实超时了，则某些任务尚未完成。
	 * @throws InterruptedException 如果等待时发生中断，在这种情况下取消尚未完成的任务
	 */
	public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> tasks, final long timeout, final TimeUnit unit) throws InterruptedException {
		return executor.invokeAll(tasks, timeout, unit);
	}

	/**
	 * 执行给定的任务
	 * <p>
	 * 如果某个任务已成功完成（也就是未抛出异常），则返回其结果。 一旦正常或异常返回后，则取消尚未完成的任务。
	 * 如果此操作正在进行时修改了给定的collection，则此方法的结果是不确定的。
	 * </p>
	 *
	 * @param tasks 任务集合
	 * @param <T>   泛型
	 * @return 某个任务返回的结果
	 * @throws InterruptedException 如果等待时发生中断
	 * @throws ExecutionException   如果没有任务成功完成
	 */
	public <T> T invokeAny(final Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
		return executor.invokeAny(tasks);
	}

	/**
	 * 执行给定的任务
	 * <p>
	 * 如果在给定的超时期满前某个任务已成功完成（也就是未抛出异常），则返回其结果。 一旦正常或异常返回后，则取消尚未完成的任务。
	 * 如果此操作正在进行时修改了给定的collection，则此方法的结果是不确定的。
	 * </p>
	 *
	 * @param tasks   任务集合
	 * @param timeout 最长等待时间
	 * @param unit    时间单位
	 * @param <T>     泛型
	 * @return 某个任务返回的结果
	 * @throws InterruptedException 如果等待时发生中断
	 * @throws ExecutionException   如果没有任务成功完成
	 * @throws TimeoutException     如果在所有任务成功完成之前给定的超时期满
	 */
	public <T> T invokeAny(final Collection<? extends Callable<T>> tasks, final long timeout, final TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		return executor.invokeAny(tasks, timeout, unit);
	}
}
