package com.onlyxiahui.common.utils.base.lang.primitive;

import com.onlyxiahui.common.utils.base.lang.reflect.PropertyJudgeUtil;

/**
 * Date 2019-01-11 14:14:40<br>
 * Description
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class PrimitiveValueUtil {

	@SuppressWarnings("unchecked")
	public static <T> T getDefaultValue(Class<T> clazz) {
		Object object = null;
		if (PropertyJudgeUtil.isInteger(clazz)) {
			object = 0;
		} else if (PropertyJudgeUtil.isLong(clazz)) {
			object = 0L;
		} else if (PropertyJudgeUtil.isFloat(clazz)) {
			object = 0.0F;
		} else if (PropertyJudgeUtil.isDouble(clazz)) {
			object = 0.00D;
		} else if (PropertyJudgeUtil.isByte(clazz)) {
			object = (byte) 0;
		} else if (PropertyJudgeUtil.isCharacter(clazz)) {
			object = '\u0000';
		} else if (PropertyJudgeUtil.isShort(clazz)) {
			object = (short) 0;
		} else if (PropertyJudgeUtil.isBoolean(clazz)) {
			object = false;
		}
		return ((T) object);
	}
}
