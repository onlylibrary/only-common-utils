package com.onlyxiahui.common.utils.base.net;

import java.math.BigDecimal;

import com.onlyxiahui.common.utils.base.lang.bytes.BytesSizeUtil;

/**
 * Description <br>
 * Date 2020-11-11 11:40:10<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class NetSpeedUtil {

	public static String getSpeedTextOfSecondByMillisecond(Long size, Long millisecond) {
		String speed = "0MB/S";
		if (null != size && null != millisecond) {

			BigDecimal bigSecond = new BigDecimal("1000");
			BigDecimal bigSize = new BigDecimal(size);
			BigDecimal bigMillisecond = new BigDecimal(millisecond);
			BigDecimal rate = bigMillisecond.divide(bigSecond, 2, BigDecimal.ROUND_HALF_UP);
			BigDecimal loaded = bigSize.divide(rate, 2, BigDecimal.ROUND_HALF_UP);
			speed = BytesSizeUtil.getSizeUnit(loaded.toBigInteger(), 2) + "/S";
		}
		return speed;
	}
}
