package com.onlyxiahui.common.utils.base.compress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * 
 * Description Zip压缩相关<br>
 * Date 2020-11-09 19:19:05<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class ZipUtil {

	/**
	 * 
	 * Description 字节zip压缩 <br>
	 * Date 2019-03-30 17:28:33<br>
	 * 
	 * @param bytes
	 * @return byte[]
	 * @since 1.0.0
	 */
	public static byte[] zip(byte[] bytes) {
		byte[] outBytes = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ZipOutputStream zip = new ZipOutputStream(bos);
			ZipEntry entry = new ZipEntry("zip");
			entry.setSize(bytes.length);
			zip.putNextEntry(entry);
			zip.write(bytes);
			zip.closeEntry();
			zip.close();
			outBytes = bos.toByteArray();
			bos.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return outBytes;
	}

	/**
	 * 
	 * Description 字节zip解压 <br>
	 * Date 2019-03-30 17:28:10<br>
	 * 
	 * @param bytes
	 * @return byte[]
	 * @since 1.0.0
	 */
	public static byte[] unZip(byte[] bytes) {
		byte[] outBytes = null;
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			ZipInputStream zip = new ZipInputStream(bis);
			while (zip.getNextEntry() != null) {
				byte[] buf = new byte[1024];
				int num = -1;
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				while ((num = zip.read(buf, 0, buf.length)) != -1) {
					baos.write(buf, 0, num);
				}
				outBytes = baos.toByteArray();
				baos.flush();
				baos.close();
			}
			zip.close();
			bis.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return outBytes;
	}
}