
package com.onlyxiahui.common.utils.base.lang.util.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * Description <br>
 * Date 2021-03-16 15:48:37<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ListConvertUtil {

	public interface Converter<S, T> {

		/**
		 * 
		 * <br>
		 *
		 * @date 2022-05-24 12:52:31<br>
		 * @param source
		 * @return
		 * @since 1.0.0
		 */
		T convert(S source);
	}

	public interface GetKey<V, K> {

		/**
		 * F <br>
		 *
		 * @date 2022-05-24 12:52:40<br>
		 * @param value
		 * @return
		 * @since 1.0.0
		 */
		K getKey(V value);
	}

	public interface GetValue<K, V> {

		/**
		 * 
		 * <br>
		 *
		 * @date 2022-05-24 12:52:43<br>
		 * @param key
		 * @return
		 * @since 1.0.0
		 */
		V getByKey(K key);
	}

	public interface ValueBack<T> {

		/**
		 * 
		 * <br>
		 *
		 * @date 2022-05-24 12:52:47<br>
		 * @param value
		 * @since 1.0.0
		 */
		void back(T value);
	}

	/**
	 * 
	 * 列表转换<br>
	 *
	 * @date 2022-05-24 14:30:23<br>
	 * @param <S>
	 * @param <T>
	 * @param list
	 * @param valueConvert
	 * @return
	 * @since 1.0.0
	 */
	public static <S, T> List<T> convertList(List<S> list, Converter<S, T> valueConvert) {
		List<T> values = new ArrayList<>();
		if (null != list && null != valueConvert) {
			list.forEach((s) -> {
				T t = valueConvert.convert(s);
				values.add(t);
			});
		}
		return values;
	}

	public static <K, S, T> List<T> convertListByKey(
			List<S> list,
			Converter<S, K> valueConvertKey,
			GetValue<K, T> getValue) {
		List<K> keys = convertList(list, valueConvertKey);
		List<T> values = new ArrayList<>();
		if (null != keys) {
			keys.forEach((key) -> {
				T value = getValue.getByKey(key);
				values.add(value);
			});
		}
		return (values);
	}

	public static <K, S, T> List<T> convertListByKeys(
			List<S> list,
			Converter<S, K> valueConvertKey,
			GetValue<List<K>, List<T>> getValue) {
		List<K> keys = convertList(list, valueConvertKey);
		List<T> values = getValue.getByKey(keys);
		return (values);
	}

	public static <S, T> void convertListBack(
			List<S> list,
			Converter<S, T> valueConvert,
			ValueBack<List<T>> back) {
		List<T> values = convertList(list, valueConvert);
		back.back(values);
	}

	public static <K, V, T> void convertListBackByKey(
			List<V> list,
			Converter<V, K> valueConvertKey,
			GetValue<K, T> getValue,
			ValueBack<List<T>> back) {
		List<T> values = convertListByKey(
				list,
				valueConvertKey,
				getValue);
		back.back(values);
	}

	public static <K, V, T> void convertListBackByKeys(
			List<V> list,
			Converter<V, K> valueConvertKey,
			GetValue<List<K>, List<T>> getValue,
			ValueBack<List<T>> back) {
		List<K> keys = convertList(list, valueConvertKey);
		List<T> values = getValue.getByKey(keys);
		back.back(values);
	}
}
